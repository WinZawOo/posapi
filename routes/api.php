<?php

use App\Balance;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



    //User API
    Route::post('register','API\RegisterController@register');
    Route::post('login/salt/{salt}','API\RegisterController@login');

Route::middleware('auth:api', 'throttle:500,60')->group(function(){

    //Imei Api
    Route::get('imei/get/{s}/simid/{simid}','API\ImeiController@imeis_get');
    Route::post('imei','API\ImeiController@imeis_post');

    //Balance API
    Route::get('balance/action/log/limit/{limit?}/offset/{offset?}','API\BalanceController@balance_get');
    Route::post('balance/action/log/limit/{limit?}/offset/{offset?}','API\BalanceController@balance_post');

    //Category API
    Route::get('category/action/log/limit/{limit?}/offset/{offset?}','API\CategoryController@cats_get');
    Route::post('category/action/log/limit/{limit?}/offset/{offset?}','API\CategoryController@cats_post');

    //Convert API
    Route::get('convert/action/log/limit/{limit?}/offset/{offset?}', 'API\ConvertController@converts_get');
    Route::post('convert/action/log/limit/{limit?}/offset/{offset?}', 'API\ConvertController@converts_post');

    //Coupon API
    Route::get('coupon/action/log/limit/{limit?}/offset/{offset?}', 'API\CouponController@coupons_get');
    Route::post('coupon/action/log/limit/{limit?}/offset/{offset?}', 'API\CouponController@coupons_post');

    //Customer API
    Route::get('customers/toupdates','API\CustomerController@toupdates_get');
    Route::get('customers/confirmupdates/{confirms}','API\CustomerController@confirmupdates_get');
    Route::get('customers/action/log/limit/{limit?}/offset/{offset?}', 'API\CustomerController@customers_get');
    Route::post('customers/action/log/limit/{limit?}/offset/{offset?}', 'API\CustomerController@customers_post');
    Route::get('customers/groups/action/log/limit/{limit?}/offset/{offset?}', 'API\CustomerController@groups_get');
    Route::post('customers/groups/action/log/limit/{limit?}/offset/{offset?}', 'API\CustomerController@groups_post');

    //Feedback API
    Route::get('feedback/salt/{salt}','API\FeedbackController@feedbacks_get');
    Route::post('feedback','API\FeedbackController@feedbacks_post');

    //IncomeExpense API
    Route::get('incomeexpense/action/log/limit/{limit?}/offset/{offset?}','API\IncomeExpenseController@incomeexpenses_get');
    Route::post('incomeexpense/action/log/limit/{limit?}/offset/{offset?}','API\IncomeExpenseController@incomeexpenses_post');

    //Invoice API
    Route::get('invoice/salesorders','API\InvoiceController@salesorders_get');
    Route::post('invoice/salesorders','API\InvoiceController@salesorders_post');
    Route::get('invoice/salesorderdetails','API\InvoiceController@salesorderdetails_get');
    Route::get('invoice/salesorderopens','API\InvoiceController@salesorderopens_get');
    Route::get('invoice/maxids/{did}','API\InvoiceController@maxids_get');
    Route::get('invoice/doccloses','API\InvoiceController@doccloses_get');
    Route::get('invoice/salesorderupdates','API\InvoiceController@salesorderupdates_get');
    Route::get('invoice/lastinvoices/did/{did}','API\InvoiceController@lastinvoices_get');
    Route::get('invoice/action/log/limit/{limit?}/offset/{offset?}/oversales/{oversales?}','API\InvoiceController@invoices_get');
    Route::post('invoice/action/log/limit/{limit?}/offset/{offset?}','API\InvoiceController@invoices_post');
    Route::post('invoice/openqtys','API\InvoiceController@openqtys_post');

    //ShopStock API
    Route::get('shopstock/action/log/limit/{limit?}/offset/{offset?}','API\ShopStockController@shopstocks_get');
    Route::post('shopstock/action/log/limit/{limit?}/offset/{offset?}','API\ShopStockController@shopstocks_post');

    //StockAdjust API
    Route::get('stockadjust/action/log/limit/{limit?}/offset/{offset?}','API\StockAdjustController@stockadjusts_get');
    Route::post('stockadjust/action/log/limit/{limit?}/offset/{offset?}','API\StockAdjustController@stockadjusts_post');

    //StockTransfer API
    Route::get('stocktransfer/action/log/limit/{limit?}/offset/{offset?}','API\StockTransferController@stocktransfers_get');
    Route::post('stocktransfer/action/log/limit/{limit?}/offset/{offset?}','API\StockTransferController@stocktransfers_post');

    //Supplier API
    Route::get('supplier/action/log/limit/{limit?}/offset/{offset?}','API\SupplierController@suppliers_get');
    Route::post('supplier/action/log/limit/{limit?}/offset/{offset?}','API\SupplierController@suppliers_post');

    //Item API
    Route::get('item/action/log/limit/{limit?}/offset/{offset?}', 'API\ItemController@items_get');
    Route::post('item/action/log/limit/{limit?}/offset/{offset?}', 'API\ItemController@items_post');

    //Main Stock API
    Route::get('stock/action/log/limit/{limit?}/offset/{offset?}', 'API\MainStockController@stocks_get');
    Route::post('stock/action/log/limit/{limit?}/offset/{offset?}', 'API\MainStockController@stocks_post');

    //Odwd API
    Route::get('odwd/action/log/limit/{limit?}/offset/{offset?}', 'API\OdwdController@odwds_get');
    Route::post('odwd/action/log/limit/{limit?}/offset/{offset?}', 'API\OdwdController@odwds_post');

    //Omad API
    Route::get('omad/action/log/limit/{limit?}/offset/{offset?}', 'API\OmadController@omads_get');
    Route::post('omad/action/log/limit/{limit?}/offset/{offset?}', 'API\OmadController@omads_post');

    //Package API
    Route::get('package/action/log/limit/{limit?}/offset/{offset?}','API\PackageController@packages_get');
    Route::post('package/action/log/limit/{limit?}/offset/{offset?}','API\PackageController@packages_post');

    //PriceList API
    Route::get('pricelist/action/log/limit/{limit?}/offset/{offset?}','API\PricelistController@pricelists_get');
    Route::post('pricelist/action/log/limit/{limit?}/offset/{offset?}','API\PricelistController@pricelists_post');

    //PriceListName API
    Route::get('pricelistname/action/log/limit/{limit?}/offset/{offset?}','API\PricelistnameController@pricelistnames_get');
    Route::post('pricelistname/action/log/limit/{limit?}/offset/{offset?}','API\PricelistnameController@pricelistnames_post');

    //Purchase API
    Route::get('purchase/action/log/limit/{limit?}/offset/{offset?}','API\PurchaseController@purchases_get');
    Route::post('purchase/action/log/limit/{limit?}/offset/{offset?}','API\PurchaseController@purchases_post');

    //PurchaseDetail API
    Route::get('purchasedetails/{salt?}', 'API\PurchasedetailController@purchasedetails_get');
    Route::post('purchasedetails', 'API\PurchasedetailController@purchasedetails_post');

    //Sale API
    Route::get('salesorders', 'API\SaleController@salesorders_get');
    Route::get('salesorderdetails', 'API\SaleController@salesorderdetails_get');
    Route::get('salesorderopens', 'API\SaleController@salesorderopens_get');
    Route::get('maxids/did/{did?}', 'API\SaleController@maxids_get');
    Route::get('doccloses', 'API\SaleController@doccloses_get');
    Route::get('salesorderupdates', 'API\SaleController@salesorderupdates_get');
    Route::get('lastsales/did/{did?}', 'API\SaleController@lastsales_get');
    Route::get('sale/action/log/limit/{limit?}/offset/{offset?}','API\SaleController@sales_get');
    Route::post('salesorders', 'SaleController@salesorders_post');
    Route::post('sale/action/log/limit/{limit?}/offset/{offset?}','API\SaleController@sales_post');
    Route::post('openqtys', 'API\SaleController@openqtys_post');

    //Saleperson API
    Route::get('saleperson/action/log/limit/{limit?}/offset/{offset?}','API\SalepersonController@salepersons_get');
    Route::post('saleperson/action/log/limit/{limit?}/offset/{offset?}','API\SalepersonController@salepersons_post');

    //Series API
    Route::get('series/action/log/limit/{limit?}/offset/{offset?}','API\SeriesController@series_get');
    Route::post('series/action/log/limit/{limit?}/offset/{offset?}','API\SeriesController@series_post');

    //Master API
    Route::post('masters', 'API\MasterController@masters_post');
    Route::post('uploads', 'API\MasterController@uploads_post');

    //Unauth API
    Route::post('unauths', 'API\UnauthController@unauths_post');

    //Version API
    Route::get('versions/s/{salt?}', 'API\VersionController@versions_get');

    //Warehouse API
    Route::get('warehouse/action/log/limit/{limit?}/offset/{offset?}','API\WarehouseController@warehouses_get');
    Route::post('warehouse/action/log/limit/{limit?}/offset/{offset?}','API\WarehouseController@warehouses_post');


});

