<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbSalesdetailCopyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_salesdetail_copy', function(Blueprint $table)
		{
			$table->integer('id')->unsigned()->primary();
			$table->smallInteger('kid')->unsigned()->default(1);
			$table->smallInteger('did')->unsigned()->default(1)->comment('did');
			$table->integer('_id')->unsigned()->comment('_id');
			$table->integer('sal')->unsigned()->comment('sales');
			$table->integer('iid')->unsigned()->comment('itemid');
			$table->string('itc', 50)->comment('item code');
			$table->smallInteger('sid')->unsigned()->comment('shopid');
			$table->float('qty', 10, 0)->unsigned()->default(0)->comment('qty');
			$table->integer('prc')->default(0)->comment('price');
			$table->string('rmk', 200)->nullable()->comment('remark');
			$table->timestamp('udt')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('updatetime');
			$table->boolean('syc')->comment('sync');
			$table->boolean('version')->default(1)->comment('version');
			$table->string('foc', 10)->default('0')->comment('gif');
			$table->string('srl', 50)->comment('serial no');
			$table->string('bth', 50)->comment('batch no');
			$table->integer('dlr')->default(0)->comment('to deliver');
			$table->smallInteger('duid')->unsigned()->default(1)->comment('delivery user id');
			$table->string('dtm', 19)->default('')->comment('delivery date');
			$table->string('drv', 30)->default('')->comment('driver');
			$table->string('vhl', 30)->default('')->comment('vehicle');
			$table->string('brd', 20)->default('')->comment('brand');
			$table->integer('dis')->default(0)->comment('discount');
			$table->string('car', 30)->default('')->comment('carrier');
			$table->string('cod', 30)->default('')->comment('doc code');
			$table->string('whc', 30)->default('')->comment('wearhous code');
			$table->string('prj', 30)->default('')->comment('project');
			$table->smallInteger('lin')->default(-1)->comment('line num');
			$table->boolean('sta')->default(1)->comment('state');
			$table->boolean('upd')->default(0)->comment('to-update 1=yes, 0=no');
			$table->integer('autoid', true);
			$table->bigInteger('log')->unsigned()->unique('log');
			$table->string('but', 30)->default('')->comment('business unit');
			$table->string('grp', 30)->default('')->comment('groups');
			$table->string('csf', 30)->default('')->comment('cashfund');
			$table->index(['sal','kid','_id','did','iid'], 'Index3');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_salesdetail_copy');
	}

}
