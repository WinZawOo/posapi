<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbPurchaseTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_purchase', function(Blueprint $table)
		{
			$table->integer('id')->unsigned()->primary();
			$table->smallInteger('kid')->unsigned()->default(1);
			$table->smallInteger('did')->unsigned()->default(1);
			$table->integer('_id')->unsigned();
			$table->smallInteger('stockid')->unsigned();
			$table->integer('userid')->unsigned()->default(1001);
			$table->float('qty', 10, 0)->default(0);
			$table->integer('cost')->default(0);
			$table->boolean('state')->default(0);
			$table->string('remark', 200);
			$table->timestamp('updatetime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('version')->default(1);
			$table->bigInteger('log')->unsigned()->default(0);
			$table->integer('autoid', true);
			$table->timestamp('createtime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->unique(['stockid','_id','did','userid','updatetime'], 'Index3');
			$table->index(['_id','did'], 'Index1');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_purchase');
	}

}
