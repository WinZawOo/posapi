<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbSupplierTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_supplier', function(Blueprint $table)
		{
			$table->integer('id')->unsigned()->primary();
			$table->smallInteger('kid')->unsigned()->default(1);
			$table->smallInteger('did')->unsigned()->default(1);
			$table->integer('_id')->unsigned();
			$table->string('name')->index('Index4');
			$table->string('address');
			$table->string('email');
			$table->string('phone')->unique('Index2');
			$table->string('contact');
			$table->string('cphone');
			$table->integer('debt')->unsigned()->default(0);
			$table->string('remark', 200);
			$table->timestamp('updatetime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('createtime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('sync')->default(1);
			$table->integer('version')->unsigned()->default(1);
			$table->boolean('encrypt')->default(0);
			$table->boolean('state')->default(1);
			$table->string('apiuser', 10);
			$table->string('apikey', 50);
			$table->bigInteger('log')->unsigned()->default(0);
			$table->integer('autoid', true);
			$table->index(['_id','kid','did'], 'Index3');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_supplier');
	}

}
