<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbBalanceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_balance', function(Blueprint $table)
		{
			$table->bigInteger('id')->unsigned()->primary();
			$table->smallInteger('kid')->unsigned()->default(1);
			$table->smallInteger('did')->unsigned()->default(1);
			$table->integer('_id')->unsigned();
			$table->integer('sid')->unsigned();
			$table->integer('uid')->unsigned();
			$table->boolean('tid')->default(0)->comment('1=purchase, 2=invoice, 3=income expense');
			$table->integer('fid')->unsigned()->default(0)->comment('from id');
			$table->boolean('io')->default(0)->comment('1=in, 2=out');
			$table->bigInteger('amt')->default(0)->comment('amount');
			$table->bigInteger('bal')->default(0)->comment('balance');
			$table->string('rmk', 200);
			$table->timestamp('udt')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('cdt')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('syc')->default(1);
			$table->boolean('ste')->default(1);
			$table->bigInteger('log')->unsigned()->default(0);
			$table->integer('version')->default(0);
			$table->integer('autoid', true);
			$table->index(['tid','kid','did','_id','sid','uid','fid','udt'], 'Index2');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_balance');
	}

}
