<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbFeedbackTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_feedback', function(Blueprint $table)
		{
			$table->increments('id');
			$table->smallInteger('kid')->unsigned()->default(1);
			$table->smallInteger('did')->unsigned()->default(1);
			$table->string('username', 25);
			$table->string('role', 20)->nullable();
			$table->timestamp('updatetime')->default(DB::raw('CURRENT_TIMESTAMP'))->index('Index2');
			$table->timestamp('createtime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('feedback')->nullable();
			$table->string('reply')->nullable();
			$table->boolean('state')->nullable()->default(0);
			$table->index(['did','kid','updatetime'], 'Index3');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_feedback');
	}

}
