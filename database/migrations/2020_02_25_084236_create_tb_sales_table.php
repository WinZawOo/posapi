<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbSalesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_sales', function(Blueprint $table)
		{
			$table->integer('id')->unsigned()->primary();
			$table->string('cod', 30)->index('Index4');
			$table->smallInteger('kid')->unsigned()->default(1);
			$table->smallInteger('did')->unsigned()->default(1)->comment('did');
			$table->integer('_id')->unsigned()->comment('_id');
			$table->smallInteger('sid')->unsigned()->comment('shopid');
			$table->bigInteger('cid')->comment('customer id');
			$table->string('ccd', 30)->comment('customer code');
			$table->string('ccn', 30)->comment('customer card name');
			$table->smallInteger('uid')->unsigned()->comment('userid');
			$table->integer('amt')->unsigned()->default(0)->comment('amount');
			$table->float('dis', 10, 0)->unsigned()->default(0)->comment('discount');
			$table->integer('dks')->unsigned()->default(0)->comment('discountks');
			$table->boolean('tax')->default(0)->comment('tax');
			$table->integer('ttl')->unsigned()->default(0)->comment('total');
			$table->integer('pid')->unsigned()->default(0)->comment('paid');
			$table->integer('chg')->unsigned()->default(0)->comment('change');
			$table->integer('rmn')->unsigned()->default(0)->comment('remain');
			$table->float('qty', 10, 0)->default(0)->comment('qty');
			$table->boolean('sta')->default(0)->comment('state');
			$table->string('rmk', 512)->comment('remark');
			$table->timestamp('udt')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('updatetime');
			$table->boolean('syc')->comment('sync');
			$table->boolean('version')->default(1)->comment('version');
			$table->boolean('prt')->default(0)->comment('printed');
			$table->string('eve', 30)->comment('event');
			$table->string('cpn', 30)->comment('coupon');
			$table->integer('dlr')->default(0)->comment('to deliver');
			$table->integer('dle')->default(0)->comment('delivered');
			$table->string('drm')->comment('deliver remark');
			$table->bigInteger('log')->unsigned()->default(0)->comment('log');
			$table->string('slt', 30)->comment('sales type');
			$table->string('dpt', 30)->comment('department');
			$table->string('shp', 30)->comment('shipping addr');
			$table->string('brh', 30)->comment('branch');
			$table->string('dtr', 30)->comment('distribution');
			$table->string('prj', 30)->comment('prject');
			$table->string('srs', 30)->comment('sap series');
			$table->string('srn', 30)->comment('series name');
			$table->string('det', 30)->comment('doc entry');
			$table->string('dnb', 30)->comment('doc number');
			$table->timestamp('cdt')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('create date time');
			$table->string('caa', 30)->comment('carrier');
			$table->string('adr', 50)->comment('show room address');
			$table->timestamp('dld')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('delevery data');
			$table->string('slp', 30)->comment('sales person');
			$table->string('gnb', 5)->comment('group num');
			$table->string('cnm', 40)->comment('customer name');
			$table->string('ucd', 30)->comment('user code');
			$table->boolean('upd')->default(0)->comment('is update');
			$table->integer('autoid', true);
			$table->boolean('pty')->default(1)->comment('pay type, 1cash, 2bank transfer, 3card, 4other');
			$table->string('err', 512)->comment('error message');
			$table->unique(['_id','did'], 'Index1');
			$table->index(['ccd','kid','uid','_id','sid','did'], 'Index3');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_sales');
	}

}
