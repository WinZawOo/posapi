<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbPricelistnameTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_pricelistname', function(Blueprint $table)
		{
			$table->increments('ListNum');
			$table->string('ListName', 30)->default('');
			$table->integer('BaseNum')->unsigned()->default(0);
			$table->string('PrimCurr', 5)->default('');
			$table->float('Factor', 10, 0)->unsigned()->default(1);
			$table->timestamp('updatetime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('createtime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('version')->unsigned()->default(1);
			$table->boolean('state')->default(1);
			$table->bigInteger('log')->unsigned()->default(0);
			$table->unique(['ListName','BaseNum'], 'code');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_pricelistname');
	}

}
