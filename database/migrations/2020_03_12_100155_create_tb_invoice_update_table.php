<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbInvoiceUpdateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_invoice_update', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('iid')->unsigned()->comment('itemid');
            $table->smallInteger('did')->unsigned()->default(1)->comment('did');
            $table->string('act');
            $table->boolean('sta')->default(1)->comment('state');
            $table->timestamp('cdt')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('create date time');
            $table->timestamp('udt')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('updatetime');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_invoice_update');
    }
}
