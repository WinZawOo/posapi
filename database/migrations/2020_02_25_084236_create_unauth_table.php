<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUnauthTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('unauth', function(Blueprint $table)
		{
			$table->increments('id');
			$table->smallInteger('kid')->unsigned();
			$table->string('key', 50);
			$table->string('simid', 50)->unique('Index4');
			$table->string('imei', 50);
			$table->string('sn', 32);
			$table->string('phone', 30)->nullable();
			$table->string('name', 30)->nullable();
			$table->timestamp('updatetime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('sync')->default(0);
			$table->index(['kid','key'], 'Index3');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('unauth');
	}

}
