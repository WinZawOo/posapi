<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVersionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('versions', function(Blueprint $table)
		{
			$table->bigInteger('ca')->unsigned()->default(0)->comment('cat');
			$table->bigInteger('it')->unsigned()->default(0)->comment('item');
			$table->bigInteger('su')->unsigned()->default(0)->comment('supplier');
			$table->bigInteger('cu')->unsigned()->default(0)->comment('customer');
			$table->bigInteger('sh')->unsigned()->default(0)->comment('shopstock');
			$table->bigInteger('ma')->unsigned()->default(0)->comment('mainstock');
			$table->bigInteger('in')->unsigned()->default(0)->comment('invoice');
			$table->bigInteger('ind')->unsigned()->default(0)->comment('invoicedetail');
			$table->bigInteger('pu')->unsigned()->default(0)->comment('purchase');
			$table->bigInteger('pud')->unsigned()->default(0)->comment('purchasedetail');
			$table->bigInteger('st')->unsigned()->default(0)->comment('stocktransfer');
			$table->bigInteger('sa')->unsigned()->default(0)->comment('stockadjust ');
			$table->bigInteger('ie')->unsigned()->default(0)->comment('incomeexpense');
			$table->bigInteger('ba')->unsigned()->default(0)->comment('balance');
			$table->bigInteger('ac')->unsigned()->default(0)->comment('account');
			$table->bigInteger('br')->unsigned()->default(0)->comment('branch');
			$table->bigInteger('bl')->unsigned()->default(0)->comment('branch loc');
			$table->bigInteger('bd')->unsigned()->default(0)->comment('brand');
			$table->bigInteger('cr')->unsigned()->default(0)->comment('carrier');
			$table->bigInteger('cg')->unsigned()->default(0)->comment('customer group');
			$table->bigInteger('dp')->unsigned()->default(0)->comment('department');
			$table->bigInteger('ds')->unsigned()->default(0)->comment('distribution');
			$table->bigInteger('dv')->unsigned()->default(0)->comment('division');
			$table->bigInteger('ig')->unsigned()->default(0)->comment('item group');
			$table->bigInteger('pc')->unsigned()->default(0)->comment('product cat');
			$table->bigInteger('pl')->unsigned()->default(0)->comment('product line');
			$table->bigInteger('sl')->unsigned()->default(0)->comment('sales type');
			$table->bigInteger('sn')->unsigned()->default(0)->comment('sales person');
			$table->bigInteger('sp')->unsigned()->default(0)->comment('shipping');
			$table->bigInteger('sr')->unsigned()->default(0)->comment('showroom addr');
			$table->bigInteger('tp')->unsigned()->default(0)->comment('township');
			$table->bigInteger('wh')->unsigned()->default(0)->comment('warehouse');
			$table->bigInteger('cp')->unsigned()->default(0)->comment('coupon');
			$table->bigInteger('pj')->unsigned()->default(0)->comment('project');
			$table->bigInteger('cv')->unsigned()->default(0)->comment('convert');
			$table->bigInteger('cvd')->unsigned()->default(0)->comment('convertdetail');
			$table->bigInteger('ss')->unsigned()->default(0)->comment('series');
			$table->bigInteger('gn')->unsigned()->default(0)->comment('groupnum payment term');
			$table->bigInteger('pk')->unsigned()->default(0)->comment('package');
			$table->bigInteger('us')->unsigned()->default(0)->comment('user');
			$table->bigInteger('od')->unsigned()->comment('odwd');
			$table->bigInteger('pr')->unsigned()->default(0)->comment('price list');
			$table->bigInteger('se')->unsigned()->default(0)->comment('sales');
			$table->bigInteger('fr')->unsigned()->default(0)->comment('freight');
			$table->bigInteger('pn')->unsigned()->default(0)->comment('pricelist name');
			$table->bigInteger('om')->default(0)->comment('omad');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('versions');
	}

}
