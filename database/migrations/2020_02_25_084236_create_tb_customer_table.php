<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbCustomerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_customer', function(Blueprint $table)
		{
			$table->integer('id')->unsigned()->primary();
			$table->smallInteger('kid')->unsigned()->default(1);
			$table->smallInteger('did')->unsigned()->default(1);
			$table->integer('_id')->unsigned();
			$table->string('code', 30)->unique('Index2');
			$table->string('name', 50)->index('Index4');
			$table->string('serial', 30)->nullable()->default('');
			$table->integer('balance')->default(0);
			$table->string('division', 35)->nullable()->default('')->comment('division');
			$table->string('township', 20)->nullable()->default('')->comment('township');
			$table->string('address')->nullable()->default('');
			$table->string('email')->nullable()->default('');
			$table->string('rank', 10)->nullable()->default('');
			$table->float('discount', 10, 0)->default(0);
			$table->boolean('price')->nullable()->default(1);
			$table->string('phone')->nullable()->default('');
			$table->integer('owe')->nullable()->default(0);
			$table->string('remark', 200)->nullable()->default('');
			$table->timestamp('updatetime')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('update time');
			$table->boolean('sync')->nullable()->default(1);
			$table->integer('version')->unsigned()->default(1);
			$table->boolean('encrypt')->default(0);
			$table->boolean('state')->default(1);
			$table->string('apiuser', 10)->nullable()->default('');
			$table->string('apikey', 50)->nullable()->default('');
			$table->bigInteger('log')->unsigned()->default(0);
			$table->integer('groupid')->unsigned()->default(0);
			$table->string('groupname', 50)->nullable()->default('');
			$table->string('contact', 50)->nullable()->default('');
			$table->string('contactphone', 50)->nullable()->default('');
			$table->string('branch', 50)->nullable()->default('');
			$table->string('groupnum', 5)->nullable()->default('')->comment('payment term');
			$table->string('pymntgroup', 20)->nullable()->default('')->comment('payment term name');
			$table->boolean('toupdate')->default(0);
			$table->increments('autoid');
			$table->timestamp('createtime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->smallInteger('pricelist')->default(0);
			$table->index(['_id','kid','did'], 'Index3');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_customer');
	}

}
