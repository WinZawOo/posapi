<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbStockadjustTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_stockadjust', function(Blueprint $table)
		{
			$table->integer('id')->unsigned()->primary();
			$table->smallInteger('kid')->unsigned()->default(1);
			$table->smallInteger('did')->unsigned()->default(1);
			$table->integer('_id')->unsigned();
			$table->smallInteger('stockid')->unsigned();
			$table->integer('itemid')->unsigned();
			$table->float('qty', 10, 0)->default(0);
			$table->float('qtynew', 10, 0)->default(0);
			$table->float('differ', 10, 0)->default(0);
			$table->string('remark', 200);
			$table->timestamp('updatetime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('createtime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->bigInteger('log')->unsigned()->default(0);
			$table->integer('autoid', true);
			$table->boolean('state')->nullable()->default(1);
			$table->integer('version')->default(0);
			$table->unique(['did','_id'], 'Index3');
			$table->index(['kid','did','_id','stockid','itemid','qtynew','updatetime'], 'Index2');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_stockadjust');
	}

}
