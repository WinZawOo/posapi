<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbCatTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_cat', function(Blueprint $table)
		{
			$table->integer('id')->unsigned()->primary();
			$table->smallInteger('did')->unsigned()->default(1);
			$table->integer('_id')->unsigned();
			$table->smallInteger('parentid')->unsigned()->default(0)->comment('parentid');
			$table->string('code', 20)->unique('Index2');
			$table->string('name', 100);
			$table->timestamp('updatetime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('state')->default(1);
			$table->timestamp('createtime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->bigInteger('log')->unsigned()->default(0);
			$table->integer('autoid', true);
			$table->integer('version')->unsigned()->default(0);
			$table->index(['_id','did'], 'Index3');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_cat');
	}

}
