<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbImeiTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_imei', function(Blueprint $table)
		{
			$table->increments('id');
			$table->smallInteger('kid')->unsigned();
			$table->string('key', 50);
			$table->string('simid', 50)->unique('Index4');
			$table->string('imei', 50);
			$table->string('sn', 32);
			$table->string('name', 30)->default('');
			$table->string('phone', 30);
			$table->timestamp('updatetime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('sync')->default(0);
			$table->string('printheader', 300)->nullable();
			$table->string('printfooter', 512)->nullable();
			$table->string('tax', 10)->nullable()->default('0');
			$table->char('haslogo', 1)->default(0);
			$table->boolean('tlnumber')->default(10);
			$table->char('webupdate', 1)->default(0);
			$table->char('webmessage', 1)->default(0);
			$table->string('s', 50);
			$table->string('sliplang', 10)->default('English')->comment('Slip Language');
			$table->string('apkname', 50)->nullable();
			$table->smallInteger('apkversioncode')->unsigned()->default(1);
			$table->string('apkmessage')->default('Version');
			$table->string('spec1', 50)->default('Spec1');
			$table->string('spec2', 50)->default('Spec2');
			$table->string('spec3', 50)->default('Spec3');
			$table->boolean('autosync')->default(1);
			$table->boolean('autosyncbeforeadd')->default(0);
			$table->smallInteger('countover')->unsigned()->default(888)->comment('limit entries');
			$table->smallInteger('timeover')->unsigned()->default(15)->comment('limit days');
			$table->smallInteger('maxunuploadsize')->unsigned()->default(100)->comment('need to upload');
			$table->smallInteger('autouploadsize')->unsigned()->default(0)->comment('auto upload if ununload size if greater than');
			$table->boolean('tlfontsize')->default(18)->comment('tableListFontSzieDip');
			$table->boolean('tlfontsizeheader')->default(20)->comment('tableListHeaderFontSzieDip');
			$table->integer('version')->unsigned()->default(1);
			$table->boolean('taxmin')->default(1)->comment('Min 50ks');
			$table->boolean('printlogo')->default(1);
			$table->boolean('slipitemeng')->default(1)->comment('Slip item only in Eng');
			$table->boolean('tlshowsize')->default(15);
			$table->boolean('tlheadersize')->default(18);
			$table->boolean('slipfooterspace')->default(1);
			$table->boolean('payfontsize')->default(18);
			$table->string('onfocus', 10)->default('code');
			$table->boolean('encrypt')->default(0);
			$table->boolean('dontsyncdata')->default(0);
			$table->string('custcode', 20)->default('1');
			$table->boolean('opendrawer')->default(0);
			$table->boolean('slipwidth')->default(58);
			$table->boolean('printernum')->default(1);
			$table->boolean('printunit')->default(0);
			$table->boolean('printinfo')->default(1);
			$table->boolean('editprice')->default(1);
			$table->boolean('editinvoice')->default(1);
			$table->boolean('printed')->default(0);
			$table->boolean('autobgsync')->default(1);
			$table->smallInteger('autobgsynctime')->unsigned()->default(15);
			$table->boolean('relogin')->default(0);
			$table->string('showcost', 12)->nullable()->default('Manager');
			$table->string('custprefix', 30)->nullable();
			$table->bigInteger('custstart')->default(10000000);
			$table->bigInteger('custend')->default(99999999);
			$table->string('invprefix', 30)->default('18YGNHC-SO');
			$table->bigInteger('invstart')->default(10000000);
			$table->bigInteger('invend')->default(99999999);
			$table->integer('luckydraw')->unsigned()->default(0);
			$table->boolean('showsap')->default(1)->comment('is sap');
			$table->boolean('qtydigits')->default(0)->comment('qty point digits');
			$table->boolean('devicedigits')->default(4)->comment('device id digits');
			$table->string('didlen', 5)->default('%04d')->comment('divice num length');
			$table->boolean('showspec')->default(0)->comment('show spec');
			$table->boolean('oversales')->default(0);
			$table->index(['kid','key'], 'Index3');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_imei');
	}

}
