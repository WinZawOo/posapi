<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbAddressTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_address', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('code', 30)->unique('Index2');
			$table->string('name', 100);
			$table->timestamp('updatetime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('version')->unsigned()->nullable()->default(1);
			$table->boolean('state')->default(1);
			$table->bigInteger('log')->unsigned()->default(0);
			$table->string('company', 30);
			$table->string('address', 256);
			$table->string('faddress', 256);
			$table->string('tship', 30);
			$table->string('town', 30);
			$table->string('phone1', 30);
			$table->string('phone2', 30);
			$table->string('fax', 30);
			$table->string('email', 100);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_address');
	}

}
