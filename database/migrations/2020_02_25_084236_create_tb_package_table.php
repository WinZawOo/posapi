<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbPackageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_package', function(Blueprint $table)
		{
			$table->integer('id')->unsigned()->primary();
			$table->integer('_id');
			$table->integer('did');
			$table->string('project', 50)->default('');
			$table->string('name', 50)->default('');
			$table->integer('iid')->unsigned();
			$table->string('icode', 30)->default('');
			$table->string('iname', 100);
			$table->float('qty', 10, 0);
			$table->integer('price')->default(0);
			$table->timestamp('updatetime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('createtime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('state')->default(1);
			$table->bigInteger('log')->unsigned()->default(0);
			$table->integer('autoid', true);
			$table->string('warehouse', 30)->default('');
			$table->integer('version')->unsigned()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_package');
	}

}
