<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_user', function(Blueprint $table)
		{
			$table->increments('id');
			$table->smallInteger('kid')->unsigned()->default(1);
			$table->smallInteger('did')->unsigned()->default(1);
			$table->integer('_id')->unsigned();
			$table->string('code', 20)->default('');
			$table->string('username', 20)->default('')->unique('Index4');
			$table->string('password', 20)->nullable();
			$table->string('name', 30)->default('')->index('Index2');
			$table->string('role', 20)->nullable();
			$table->timestamp('updatetime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('createtime')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('sync')->nullable()->default(1);
			$table->integer('version')->unsigned()->default(1);
			$table->boolean('state')->default(1);
			$table->string('rights')->default('');
			$table->string('branch', 20)->default('');
			$table->string('dept', 20)->default('');
			$table->string('shipping', 20)->default('');
			$table->string('dist', 20)->default('');
			$table->string('telephone', 30)->default('');
			$table->string('mobile', 30)->default('');
			$table->string('fax', 30)->default('');
			$table->string('email', 50)->default('');
			$table->string('printing', 30)->default('');
			$table->string('salestype', 30)->default('');
			$table->bigInteger('log')->default(0);
			$table->index(['did','kid','updatetime'], 'Index3');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_user');
	}

}
