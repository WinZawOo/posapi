<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_item', function(Blueprint $table)
		{
			$table->integer('id')->unsigned()->primary();
			$table->smallInteger('kid')->unsigned()->default(1);
			$table->smallInteger('did')->unsigned()->default(1);
			$table->integer('_id')->unsigned();
			$table->smallInteger('catid')->unsigned()->default(0);
			$table->string('catcode', 50)->nullable()->default('');
			$table->integer('supplierid')->unsigned()->default(1);
			$table->string('name', 150)->nullable()->default('')->index('Index4')->comment('item desc');
			$table->string('name2', 200)->nullable()->default('');
			$table->string('unit', 15)->nullable()->default('');
			$table->string('unit2', 15)->nullable()->default('');
			$table->string('image', 100)->nullable()->default('');
			$table->string('code', 100)->nullable()->default('')->index('Index5');
			$table->string('barcode', 100)->nullable()->default('');
			$table->string('model', 120)->nullable()->default('')->comment('item no');
			$table->string('spec1', 25)->nullable()->default('');
			$table->string('spec2', 50)->nullable()->default('');
			$table->string('spec3', 50)->nullable()->default('');
			$table->integer('cost2')->default(0);
			$table->integer('cost')->default(0);
			$table->integer('price')->default(0);
			$table->integer('price2')->default(0);
			$table->integer('price3')->default(0);
			$table->string('remark')->nullable()->default('');
			$table->timestamp('updatetime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('createtime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('sync')->nullable()->default(1);
			$table->integer('version')->unsigned()->nullable()->default(1);
			$table->boolean('encrypt')->default(0);
			$table->boolean('state')->default(1);
			$table->boolean('lockprice')->default(1);
			$table->boolean('currency')->default(1)->comment('1=Ks, 2=US, 3=CN, 4=Thai, 5=india');
			$table->integer('fcost')->unsigned()->default(0);
			$table->integer('parent')->unsigned()->default(0);
			$table->integer('qty')->unsigned()->default(0);
			$table->string('brand', 20)->nullable()->default('');
			$table->string('uom', 50)->nullable()->default('');
			$table->string('old_code', 50)->nullable()->default('');
			$table->bigInteger('log')->unsigned()->default(0);
			$table->integer('autoid', true);
			$table->boolean('hassn')->default(0)->comment('has serail number');
			$table->boolean('nonstock')->default(0);
			$table->index(['_id','kid','did'], 'Index3');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_item');
	}

}
