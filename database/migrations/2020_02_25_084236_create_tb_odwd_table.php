<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbOdwdTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_odwd', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('code', 30)->unique('Index2');
			$table->string('name', 100);
			$table->string('itemgroup', 20)->default('');
			$table->string('whs', 20)->default('');
			$table->string('groups', 20)->default('');
			$table->string('unit', 20)->default('');
			$table->string('project', 20)->default('');
			$table->timestamp('updatetime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('createtime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('version')->unsigned()->nullable()->default(1);
			$table->boolean('state')->default(1);
			$table->bigInteger('log')->unsigned()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_odwd');
	}

}
