<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbShopstockTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_shopstock', function(Blueprint $table)
		{
			$table->increments('id');
			$table->smallInteger('kid')->unsigned()->default(1);
			$table->smallInteger('did')->unsigned()->default(1);
			$table->integer('sid')->unsigned()->default(1)->comment('shopid');
			$table->string('whscode', 50)->index('Index3');
			$table->integer('iid')->unsigned()->default(0)->comment('itemid');
			$table->string('itemcode', 50)->index('Index4');
			$table->float('qty', 10, 0)->default(0);
			$table->integer('tti')->default(0)->comment('totalin');
			$table->integer('alt')->unsigned()->default(0);
			$table->string('rmk', 200)->nullable()->default('');
			$table->timestamp('udt')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('cdt')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('syc')->nullable()->default(1);
			$table->integer('version')->unsigned()->default(1);
			$table->bigInteger('log')->unsigned()->default(0);
			$table->integer('onhand')->default(0);
			$table->unique(['itemcode','whscode'], 'Index2');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_shopstock');
	}

}
