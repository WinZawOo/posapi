<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbItemGroupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_item_group', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('code', 30)->unique('Index2');
			$table->string('name', 100);
			$table->timestamp('updatetime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('version')->unsigned()->nullable()->default(1);
			$table->boolean('state')->default(1);
			$table->bigInteger('log')->unsigned()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_item_group');
	}

}
