<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbIncomeexpenseTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_incomeexpense', function(Blueprint $table)
		{
			$table->bigInteger('id')->unsigned()->primary();
			$table->smallInteger('kid')->unsigned()->default(1);
			$table->smallInteger('did')->unsigned()->default(1);
			$table->integer('_id')->unsigned();
			$table->smallInteger('shopid')->unsigned();
			$table->integer('userid')->unsigned();
			$table->string('title', 200)->index('Index1');
			$table->boolean('inout')->default(0);
			$table->bigInteger('amount')->default(0);
			$table->string('remark', 200);
			$table->timestamp('updatetime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('createtime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('sync')->nullable()->default(1);
			$table->boolean('state')->nullable()->default(1);
			$table->bigInteger('log')->unsigned()->default(0);
			$table->integer('autoid', true);
			$table->integer('version')->default(0);
			$table->index(['kid','did','_id','shopid','userid','inout','updatetime'], 'Index2');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_incomeexpense');
	}

}
