<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbStocktransferTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_stocktransfer', function(Blueprint $table)
		{
			$table->integer('id')->unsigned()->primary();
			$table->smallInteger('kid')->unsigned();
			$table->smallInteger('did')->unsigned();
			$table->integer('_id')->unsigned();
			$table->smallInteger('fromid')->unsigned();
			$table->smallInteger('toid')->unsigned();
			$table->integer('itemid')->unsigned();
			$table->float('qty', 10, 0)->default(0);
			$table->string('remark', 200);
			$table->timestamp('updatetime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('createtime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->bigInteger('log')->unsigned()->default(0);
			$table->integer('autoid', true);
			$table->boolean('state')->nullable()->default(1);
			$table->integer('version')->default(0);
			$table->index(['did','_id'], 'Index1');
			$table->unique(['kid','_id','itemid','fromid','toid','did','updatetime'], 'Index3');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_stocktransfer');
	}

}
