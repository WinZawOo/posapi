<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbCouponTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_coupon', function(Blueprint $table)
		{
			$table->integer('id')->unsigned()->primary();
			$table->integer('_id')->unsigned();
			$table->boolean('did')->default(1);
			$table->string('event', 20);
			$table->string('code', 32)->default('');
			$table->integer('discount')->unsigned()->default(0);
			$table->integer('price')->unsigned()->default(0);
			$table->timestamp('createtime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updatetime')->default(DB::raw('CURRENT_TIMESTAMP'))->index('Index3');
			$table->boolean('used')->default(0);
			$table->integer('version')->unsigned()->default(1);
			$table->boolean('state')->default(1);
			$table->bigInteger('log')->unsigned()->default(0);
			$table->integer('autoid', true);
			$table->unique(['code','event'], 'Index2');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_coupon');
	}

}
