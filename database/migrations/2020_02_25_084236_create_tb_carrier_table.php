<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbCarrierTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_carrier', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('code', 50)->unique('Index2');
			$table->string('name', 100)->index('Index4');
			$table->string('cnamef');
			$table->string('cname', 100);
			$table->string('phone1', 30);
			$table->string('phone2', 30);
			$table->string('phone3', 30);
			$table->string('address');
			$table->string('addressf');
			$table->string('township', 50);
			$table->string('town', 20);
			$table->string('statename', 30);
			$table->string('country', 30);
			$table->string('num', 30);
			$table->string('street');
			$table->string('blockno');
			$table->string('remark', 200);
			$table->timestamp('updatetime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('createtime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('version')->unsigned()->default(1);
			$table->boolean('state')->default(1);
			$table->bigInteger('log')->unsigned()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_carrier');
	}

}
