<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbFreightTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_freight', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('code', 30)->unique('code');
			$table->string('name', 50)->default('');
			$table->timestamp('updatetime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('createtime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('version')->unsigned()->default(1);
			$table->boolean('state')->default(1);
			$table->bigInteger('log')->unsigned()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_freight');
	}

}
