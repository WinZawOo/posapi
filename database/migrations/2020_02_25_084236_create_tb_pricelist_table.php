<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbPricelistTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_pricelist', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('code', 30);
			$table->smallInteger('plist')->unsigned()->default(0);
			$table->string('currency', 5)->default('');
			$table->float('price', 10, 0)->unsigned()->default(0);
			$table->char('overwritten', 1)->default('N');
			$table->timestamp('updatetime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('createtime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('version')->unsigned()->default(1);
			$table->boolean('state')->default(1);
			$table->bigInteger('log')->unsigned()->default(0);
			$table->unique(['code','plist'], 'code');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_pricelist');
	}

}
