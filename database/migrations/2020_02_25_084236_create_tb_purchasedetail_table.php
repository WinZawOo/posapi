<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTbPurchasedetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tb_purchasedetail', function(Blueprint $table)
		{
			$table->integer('id')->unsigned()->primary();
			$table->smallInteger('kid')->unsigned();
			$table->smallInteger('did')->unsigned();
			$table->integer('_id')->unsigned();
			$table->integer('purchaseid')->unsigned();
			$table->integer('stockid')->unsigned();
			$table->integer('itemid')->unsigned();
			$table->float('qty', 10, 0)->default(0);
			$table->integer('cost')->default(0);
			$table->string('remark', 200);
			$table->timestamp('updatetime')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->boolean('version')->default(1);
			$table->boolean('state')->default(1);
			$table->bigInteger('log')->unsigned()->default(0);
			$table->integer('autoid', true);
			$table->unique(['purchaseid','did','_id','itemid','updatetime','stockid'], 'Index3');
			$table->index(['did','_id','purchaseid'], 'Index1');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tb_purchasedetail');
	}

}
