<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Uploads extends Model
{
    public function scopeGetCount($query, $table)
    {
    	$table = strtolower($table);
    	$query = DB::select('select * from ' . $table);

    	return count($query);
    }

    public function scopeInsert_batch($query, $table, $pricelist)
    {
    	$table = strtolower($table);
    	return DB::insert('insert into '. $table .' value'. $pricelist);
    }

    public function scopeUpdateInsert($query, $table, $arr, $where)
    {
    	$table = strtolower($table);

    	return DB::table($table)->updateOrInsert([$where],[$arr]);
    }

    public function scopeInsert($table, $data)
    {
        DB::table($table)->insert($data);
    }
}
