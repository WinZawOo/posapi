<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use phpDocumentor\Reflection\Types\Self_;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $table = 'tb_user';

    const CREATED_AT = 'createtime';
    const UPDATED_AT = 'updatetime';


//    protected $guarded = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','_id','code','name', 'email', 'password','salestype',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public  function scopeGetRowsByColumns($query,$where = null, $limit = null, $offset = null){
        if($where == null){
            $user = $query->orderBy('id','desc')->get()->toArray();
        }else{
            $user = $query->where($where)->offset($offset)->limit($limit)->get()->toArray();
        }
        return $user;
    }

    public static function updateData($data){
        $data['password'] = bcrypt($data['password']);
        if(self::where('id',$data['id'])->update($data)) {
            return $data['id'];
        }else{
            return false;
        }
    }

    public static function insertData($data){
        $data['id'] = null;
        $data['password'] = bcrypt($data['password']);
        if($user = self::create($data)) {
            $data['id'] = $user->id;
            $data['_id'] = $user->id;
            self::where('id', $data['id'])->update($data);
            $success['id'] = $user->id;
            $success['token'] = $user->createToken('MyApp')->accessToken;
            return $success;
        }
        else
            return false;
    }
}
