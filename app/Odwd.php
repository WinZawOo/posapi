<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Odwd extends Model
{
	protected $fillable = ['id','code','name','itemgroup','whs','unit','groups','project','updatetime',
	'createtime','version','state','log'];

	protected $table = 'tb_odwd';

	const UPDATED_AT = 'updatetime';
  const CREADED_AT = 'createtime';

    public function setUpdatedAt($value)
   	{     
    	return $this;
   	}
   	public function setCreatedAt($value)
   	{
    	return $this;
   	}  

    public function scopeGetAll($query, $col, $limit, $offset)
    {
      if($offset == 0 && $limit == 0)
      {
          return $query->select($col)->orderBy('log','ASC')->get()->toArray();
      }
      elseif($limit == 0 && $offset != 0)
      {
        return $query->select($col)->where('log', '>', $offset)->orderBy('log','ASC')->get()->toArray();
      }
      else
      {
          return $query->select($col)->where('log', '>', $offset)->orderBy('log','ASC')->take($limit)->get()->toArray();
      }
    }

    public function scopeUpdateInsert($query, $data)
    {
      return $query->updateOrCreate(['id' => $data['id']], $data);

    }  

}

