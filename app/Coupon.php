<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Coupon extends Model
{
    protected $table = 'tb_coupon';
    protected $fillable = ['id','_id','did','event','code','discount','price','used','log','state'];

    const  CREATED_AT = 'createtime';
    const  UPDATED_AT = 'updatetime';

    /**
     * @param Builder $query
     * @param $col
     * @param $limit
     * @param $offset
     * @return
     */
    public function scopeGetData($query, $col, $limit, $offset){
         if($limit == 0 && $offset == 0){
            $data = $query->select($col)->orderBy('log','desc')->get()->toArray();
        }elseif ($limit == 0 && $offset != null){
            $data = $query->select($col)->where('log','>',$offset)->orderby('log','desc')->get()->toArray();
        }else{
            $data = $query->select($col)->where('log','>',$offset)->limit($limit)->orderby('log','desc')->get()->toArray();
        }
        return $data;
    }
}
