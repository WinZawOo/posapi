<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\SalesDetail;
use App\Series;
use App\ShopStock;

class Sale extends Model
{
    protected $guarded = [];

    protected $table = 'tb_sales';

    const UPDATED_AT = 'udt';
    //const CREADED_AT = 'cdt';


    public function salesdetails()
    {
    	return $this->hasMany('App\SalesDetail','sal','id');
    }

    public function scopegetAllWhere($query, $col1)
    {
        return $query->where('upd', '=', 1)->select($col1)->get()->toArray();
    }

    public function scopegetMax_id($query, $did)
    {
        return $query->where('did', '=', $did)->get('id')->max();
    }

    public function scopeGetAllDetail($query, $col1, $col2, $limit = NULL, $offset = NULL)
    {
        if($limit == 0 && $offset == 0)
        {
            $sales = $this->with(['salesdetails' => function($query)use($col2){
                $query->select($col2);
            }])->select($col1)->get()->toArray();
        }
        elseif($limit == 0 && $offset !=0)
        {
            $sales = $this->with(['salesdetails' => function($query)use($col2, $offset){
                $query->where('log', '>', $offset)->select($col2);
            }])->select($col1)->where('log', '>', $offset)->get()->toArray();
        }
        else
        {
            $sales = $this->with(['salesdetails' => function($query)use($col2, $limit, $offset){
                $query->where('log', '>', $offset)->take($limit)->select($col2);
            }])->select($col1)->where('log', '>', $offset)->take($limit)->get()->toArray();
        }

        return $sales;
    }

    public function scopeGetLastAllDetail($query, $col1, $col2, $did)
    {
        $lasts = $this->with(['salesdetails' => function($query)use($col2){
            $query->select($col2);
        }])->select($col1)->orderby('autoid', 'desc')->take(1)->get()->toArray();
        return $lasts;
    }

    public function scopeUpdateCode($query, $data)
    {

        $query = Series::where('code', '=', $data['series'])->get()->toArray();

        foreach ($query as $row) {
            $name = $row['name'];
        }

        Sale::where('id', '=', $data['id'])->update(['cod' =>$name.'-'.$data['docnum'],
            'det' => $data['docentry'], 'dnb' => $data['docnum'], 'srs' => $data['series'], 'upd' =>0]);

        SalesDetail::where('sale_id', '=', $data['id'])->where('qty','=', 0)->update(['upd'=>0, 'sta'=>0]);
    }

    public function scopeUpdateClosed($query, $close)
    {
        Sale::where('id', '=', $close['id'])->update(['upd'=>0, 'sta'=>2]);

        SalesDetail::where('sale_id', '=', $close['id'])->update(['upd'=>0, 'sta'=>2]);
    }

    public function scopeUpdateCanceled($query, $cancel)
    {
        $rmk = Sale::where('id', '=', $cancel['id'])->get('rmk')->toArray();

        foreach ($rmk as $row) {
            $rmks = $row['rmk'];
        }

        Sale::where('id', '=', $cancel['id'])->update(['upd'=>0, 'sta'=>0, 'rmk' => 'SAP CANCELLED!'. $rmks]);

        SalesDetail::where('sale_id', '=', $cancel['id'])->update(['upd'=>0, 'sta'=>0]);
    }

    public function scopeUpdateError($query, $errors)
    {
        Sale::where('id', '=', $errors['id'])->update(['upd'=>0, 'sta'=>2, 'err' => $errors['err']]);

        SalesDetail::where('sale_id', '=', $errors['id'])->update(['upd' => 0]);
    }

    public function scopeDel($query, $data)
    {
        if($query->where('id', '=', $data['id']))
        {
            $query->where('id', '=', $data['id'])->update(['sta' => 0, 'rmk' => 'rmk'. $data['rmk']]);

            $details = SalesDetail::where('sale_id', '=', $data['id'])->get();


            if($details)
            {
                foreach($details as $detail)
                {
                    $data = ['qty' => DB::raw('qty - '. (int)$detail['qty'])];
                    ShopStock::where([
                        ['sid', '=', $detail['sid'] ],
                        ['iid', '=', $detail['iid'] ],
                    ])->update($data);
                }
            }
        }
        else
        {
            return false;
        }
    }

    public function scopeUpdateSalesOpenQty($query, $arr)
    {
        $data = $query->where('det', '=', $data['docentry'])->get()->toArray();

        if (count($data) > 0)
        {
            $openqty = 0;

            $q1 = SalesDetail::where('sal', '=', $data['id'] )->get()->toArray();
            foreach($q1 as $row1)
            {
                if($data['freetxt'] == $row1['id']){
                    if($data['openqty'] == 0)
                        SalesDetail::where('id', '=', $data['freetxt'])->update(['dlr' => $data['openqty'], 'sta' => 2]);
                    else
                        SalesDetail::where('id', '=', $data['freetxt'])->update(['dlr' => $data['openqty']]);
                    $openqty += $data['openqty'];
                }
                else{
                    $openqty += $row1['dlr'];
                }

            }

            $update_data = [
                'dle' => DB::raw('qty - '. (int)$openqty),
                'dlr' => $openqty,
            ];

            $query->where('det', '=', $data['docentry'])->update($update_data);

        }
        else
        {
            return 0;
        }
    }



}

