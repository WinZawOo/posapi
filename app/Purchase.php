<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    //
   	protected $fillable = ['id','kid','did','_id','stockid','userid','qty','cost','state','remark','updatetime','version','log','autoid','createtime'];

    protected $table = 'tb_purchase';

    const UPDATED_AT = 'updatetime';
    const CREATED_AT = 'createtime';    

    public function purchasedetails()
    {
    	return $this->hasMany('App\PurchaseDetail');
    }

    public function scopeGetAllDetail($query, $col1, $col2, $limit = NULL, $offset = NULL)
    {
    	if($limit == 0 && $offset == 0)
    	{
    		$purchasedetails = $this->with(['purchasedetails' => function($query) use ($col2){ $query->select($col2); }])->get()->toArray();
    	}
        elseif($limit == 0 && $offset !=0)
        {
            $purchasedetails = $this->with(['purchasedetails' => function($query) use ($col2, $offset){ $query->where('log', '>', $offset)->select($col2); }])->where('log', '>', $offset)->get()->toArray();
        }
    	else
    	{
    		$purchasedetails = $this->with(['purchasedetails' => function($query) use ($col2, $limit, $offset){ $query->where('log', '>', $offset)
    			->take($limit)->select($col2);}])->where('log', '>', $offset)->take($limit)->get()->toArray();
		}

        return $purchasedetails; 
    }

    //$purchasedetails = Purchase::with('purchasedetails')->select('*')->get()->toarray();
    public function scopeUpdateInsert($query, $data)
    {
      return $query->updateOrCreate(['id' => $data['id']], $data);

    }  
}
