<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
	protected $guarded = [];

   protected $table = 'tb_warehouse';

   const UPDATED_AT = 'updatetime';
   const CREATED_AT = 'createtime';

   public function scopeGetAll($query, $col, $limit, $offset)
   {
      if($offset == 0 && $limit == 0)
      {
          return $query->select($col)->orderBy('log','ASC')->get()->toArray();
      }
      elseif($limit == 0 && $offset != 0)
      {
        return $query->select($col)->where('log', '>', $offset)->orderBy('log','ASC')->get()->toArray();
      }
      else
      {
          return $query->select($col)->where('log', '>', $offset)->orderBy('log','ASC')->take($limit)->get()->toArray();
      }

   }

   public function scopeUpdateInsert($query, $data)
   {
   		return $query->updateOrCreate(['id' => $data['id']], $data);
   }
}
