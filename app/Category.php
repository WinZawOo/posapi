<?php

namespace App;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = 'tb_cat';
    protected $fillable = ['id','_id','did','code','name','state'];

    const CREATED_AT = 'createtime';
    const UPDATED_AT = 'updatetime';

    /**
     * Scope a query to only include users of a given type.
     *
     * @param Builder $query
     * @param $col
     * @param null $limit
     * @param null $offset
     * @return Builder
     */
     public function scopeGetData($query, $col, $limit, $offset){
        if($limit == 0 && $offset == 0){
            $data = $query->select($col)->orderBy('log','desc')->get()->toArray();
        }elseif ($limit == 0 && $offset != null){
            $data = $query->select($col)->where('log','>',$offset)->orderby('log','desc')->get()->toArray();
        }else{
            $data = $query->select($col)->where('log','>',$offset)->limit($limit)->orderby('log','desc')->get()->toArray();
        }
        return $data;
    }
}
