<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
    //
    protected $table = "tb_balance";
    protected $fillable = ['id','_id','did','sid','uid','tid','fid','io','amt','bal','rmk','udt','log','ste'];
    const CREATED_AT = 'cdt';
    const UPDATED_AT = 'udt';

    public function scopeGetData($query, $col, $limit, $offset){
        if($limit == 0 && $offset == 0){
            $data = $query->select($col)->orderBy('log','desc')->get()->toArray();
        }elseif ($limit == 0 && $offset != null){
            $data = $query->select($col)->where('log','>',$offset)->orderby('log','desc')->get()->toArray();
        }else{
            $data = $query->select($col)->where('log','>',$offset)->limit($limit)->orderby('log','desc')->get()->toArray();
        }
        return $data;
    }
}
