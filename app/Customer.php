<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected  $table = 'tb_customer';
    protected $fillable = [
               'id','kid','_id','did','code','name','address','division','township','email','rank','discount','phone','owe','remark',
                'updatetime','ercrypt','price','log','toupdate'
        ];

    const CREATED_AT = 'createtime';
    const UPDATED_AT = 'updatetime';

    public function scopeGetData($query, $col, $limit, $offset){
        if($limit == 0 && $offset == 0){
            return $query->select($col)->orderby('log','desc')->get();
        }elseif ($limit == 0 && $offset != null){
            return $query->select($col)->where('log','>',$offset)->orderby('log','desc')->get();
        }else{
           return $query->select($col)->where('log','>',$offset)->limit($limit)->orderby('log','desc')->get();
        }
    }

}
