<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncomeExpense extends Model
{
    //
    protected $table = 'tb_incomeexpense';
    protected $fillable = ['kid','did','_id','remark','id','shopid','title','inout','amount','state'];
    const UPDATED_AT = 'updatetime';
    const CREATED_AT = 'createtime';

    public function scopeGetData($query, $col, $limit, $offset){
        if($limit == 0 && $offset == 0){
            return $query->select($col)->orderby('log','desc')->get();
        }elseif ($limit == 0 && $offset != null){
            return $query->select($col)->where('log','>',$offset)->orderby('log','desc')->get();
        }else{
            return $query->select($col)->where('log','>',$offset)->limit($limit)->orderby('log','desc')->get();
        }
    }
}
