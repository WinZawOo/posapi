<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MainStock extends Model
{
    protected $fillable = ['id','did','sid','iid','qty','alt','rmk','udt','cdt'];

    protected $table = 'tb_mainstock';

    const UPDATED_AT = 'udt';
    const CREADED_AT = 'cdt';

    public function setUpdatedAt($value)
   	{     
    	return $this;
   	}
   	public function setCreatedAt($value)
   	{
    	return $this;
   	}

    public function scopeGetAll($query, $col, $limit, $offset)
    {
      if($offset == 0 && $limit == 0)
      {
          return $query->select($col)->orderBy('log','ASC')->get()->toArray();
      }
      elseif($limit == 0 && $offset != 0)
      {
        return $query->select($col)->where('log', '>', $offset)->orderBy('log','ASC')->get()->toArray();
      }
      else
      {
          return $query->select($col)->where('log', '>', $offset)->orderBy('log','ASC')->take($limit)->get()->toArray();
      }
    }

    public function scopeUpdateInsert($query, $data)
    {
      return $query->updateOrCreate(['id' => $data['id']], $data);

    }
}
