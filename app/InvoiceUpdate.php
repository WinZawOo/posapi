<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use phpseclib\Math\BigInteger;

class InvoiceUpdate extends Model
{
    protected $table = 'tb_invoice_update';
    protected $guarded = [];
    protected $casts = [
        'lin' => 'double',
    ];

    const CREATED_AT =  'cdt';
    const UPDATED_AT =  'udt';
}
