<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unauth extends Model
{
    protected $fillable = ['id', 'kid', 'key','simid','imei','sn','phone','name','createtime','updatetime','sync'];

    protected $table = 'unauth';

    const CREATED_AT ='createtime'; 
    const UPDATED_AT ='updatetime';

    public function scopeGetCountByColumn($query, $data)
    {
    	return $query->where($data)->get();
    }

    public function scopegetRowsByColumns($query, $data)
    {
    	return $query->where($data)->orderBy('id', 'DESC')->get();
    } 



    public function scopeInsert($query, $data)
	{  	
			$arr = [
	   		'id'    	=> null,	// did
			'kid'    	=> $data['kid'],
			'key'    	=> $data['key'],
			'simid'    => $data['simid'],
			'imei'    	=> $data['imei'],
			'sn'    	=> $data['sn'],
			'updatetime'   => $data['updatetime'],
			'sync'    	=> 1,
	        's'     => $data['s'],
	    ];

			$query->create($arr);

			return $query->get()->last()->id;
	}

}
