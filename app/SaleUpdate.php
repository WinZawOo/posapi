<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleUpdate extends Model
{
    protected $guarded = [];

    protected $table = 'tb_sales_update';
    const UPDATED_AT = 'udt';
    const CREADED_AT = 'cdt';

    public function scopeGetAllWhere($query, $col)
    {
      return $query->select($col)->where('sta', '=', 0)->get()->toArray();
    }
}
