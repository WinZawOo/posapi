<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseDetail extends Model
{
    //
    protected $fillable = ['id','kid','did','_id','purchase_id','stockid', 'qty','cost','itemid','remark','updatetime','version','state','log','autoid'];

    protected $table = 'tb_purchasedetail';

    const UPDATED_AT = 'updatetime';
    const CREATED_AT = 'createtime';


    public function purchase()
    {
    	return $this->belongsTo('App\Purchase');
    }

    public function scopeGetPurchaseDetailsByYearMonth($query, $request)
    {
    	$newday = $request->day;
        $newmonth = $request->month;
        $newyear = $request->year;
  
    	if($newday == NULL)
        {
        	$message = $query->whereYear('updatetime', '=', $newyear)->whereMonth('updatetime', '=', 
        		$newmonth)->get();

        }
        else
        {
        	$message = $query->whereYear('updatetime', '=', $newyear)->whereMonth('updatetime', '=', $newmonth)->whereDay('updatetime', '=', $newday)->get();
        } 

        return $message;
    }

    public function scopeGetIdGreaterThan($query, $request)
    {
        $maxid = $request->maxid;

        $message = $query->where('id', '>', $maxid)->get();

        return $message;
    }

    public function scopeGetWhereInIds($query, $did, $_id)
    {
        $message = $query->where('did', '=', $did)->where('_id', '=', $_id)->get();
        return $message;
    }

    public function scopeGetWhereInIdsAlls($query, $id)
    {
        $message = $query->where('id', '=', $id)->get();
        return $message;
    }

    public function scopeGetWhereBetween($query, $maxid, $id)
    {
        $message = $query->where('id', '>', $id)->where('id', '<', $maxid)->get();
        return $message;
        
    }

    public function scopeInsertUpdate($query, $data)
    {
        $query->updateOrCreate(['id' => $data['id']], $data);

        $qty = 'qty+';

        $stocks = ShopStock::where('iid', '=', $data['itemid'])->where('sid', '=', $data['stockid'])->get();

        if(count($stocks) < 1)
        {
            //----------- add to stock if not exist ---------------
            $stock['id']    = null;
            $stock['kid']   = $data['kid'];
            $stock['did']   = $data['did'];
            $stock['sid']   = $data['stockid'];
            $stock['iid']   = $data['itemid'];
            $stock['qty']   = $data['qty'];
            $stock['alt']   = 0;
            $stock['rmk']   = "";
            $stock['udt']   = $data['updatetime'];
            $stock['syc']   = 1;
            $stock['version']   = 1;
            //$stock['log']     = 0;
            
            $rows = Warehouse::where('id', '=', $data['stockid'])->get();

            if(count($rows) > 0)
            {
                $stock['whscode'] = $rows[0]['code'];
            }

            $rows =TbItem::where('id', '=', $data['itemid'])->get();

            if(count($rows) > 0){
                $stock['itemcode'] = $rows[0]['code'];
            }

            
            $stock['tti'] = $data['qty'];

            ShopStock::create($stock);                           
        
        }
        else
        {
        //     print_r($data['qty']);

        //     die();
            $update_data = [
                'qty' => DB::raw('qty + '. (int)$data['qty']), 
                'tti' => DB::raw('tti + '. (int)$data['qty']), 
                'version' => DB::raw('version + 1'), 
                'udt' => $data['updatetime']
            ];

            // print_r($update_data);
            // die();
            ShopStock::where('iid', '=', $data['itemid'])->where('sid', '=', $data['stockid'])->update($update_data);
            
        }
    }
}
