<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Saleperson;
use App\Imei;

class SalepersonController extends BaseController
{
    //Sale Person GET Method
    public function salepersons_get(Request $request, $limit= NULL, $offset = NULL)
    {
        $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        $count = Saleperson::all()->count();
        $max_log_id = Saleperson::get('log')->max();

        if($count == null) $count = 0;
        if($max_log_id == null) $max_log_id = 0;

        $col = '*';

        $salepersons = Saleperson::getAll($col, $limit, $offset);

        if($salepersons)
        {
            array_push($salepersons, array('max_log_id' => $max_log_id, 'count' => $count, 'status' => 1,
                'limit'=>$limit, 'offset'=>$offset));
            return $this->sendResponse($salepersons, 'Saleperson Retrieved Successfully.');   //OK
        }
        else
        {
             return $this->sendResponse(array(array('max_log_id' => $max_log_id, 'count' => $count, 'status'=>0, 'error' => '0 row' )));
        }
    }

    //Sale Person PUT Method

	public function salepersons_post(Request $request, $limit= NULL, $offset = NULL)
	{
	    $salepersons = new Saleperson();

	    $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
	    if($count< 1)
	    {
	        $message = array( 'status' => 0, 'error' => 'No Rights!' );
	        return $this->sendError($message);
	    }

	    if( $request->updatetime != NULL )
	    {
	        if (strpos($request->updatetime, '|') !== false)
	        {
	        	$ids = explode('|',$request->id);
	            $codes = explode('|',$request->code);
	            $names = explode('|',$request->name);
	            $updatetimes = explode('|',$request->updatetime);
	            $states = explode('|',$request->state);

	            for($i = 0; $i < count($updatetime); $i++)
	            {
	            	$datas[] = array(
					'id' => $ids[$i],
					'code' => $codes[$i],
                    'name' => $names[$i],
					'updatetime' => $updatetimes[$i],
                    'state' => $version[$i],
                    'log' => 0
					);

	            }
	        }

	        else    // updatetime does not have | char
	        {
	        	if($request->id != '')
	        	{
	                $datas[] = array(
	                    'id' => $request->id,
	                    'code' => $request->code,
	                    'name' => $request->name,
	                    'updatetime' => $request->updatetime,
	                    'state' => $request->state,
	                    'log' => 0
                	);
            	}

	        }

	        foreach($datas as $data)
	        {
	        	Saleperson::updateInsert($data);
	        }

	        return $this->salepersons_get($request, $limit, $offset);
	     }
	     else
	     {
	        $message = array( 'status' => 0, 'error' => 'No Rights!' );
	        return $this->sendError($message);

	     }
	}
}
