<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Package;
use App\Imei;
use App\Resources\Package as PackageResource;
use Validator;

class PackageController extends BaseController
{
    //Package Get Method
    public function packages_get(Request $request, $limit= NULL, $offset = NULL)
    {
        $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        $count = Package::all()->count();
        $max_log_id = Package::get('log')->max();

        if($count == null) $count = 0;
        if($max_log_id == null) $max_log_id = 0;

        $col = [
        'id AS id',
        '_id AS _id',
        'did AS did',
        'name AS name',
        'project AS project',
        'iid AS iid',
        'iname AS iname',
        'icode AS icode',
        'qty AS qty',
        'updatetime AS updatetime',
        'state AS state',
        'warehouse AS warehouse',
        'price AS price',
        'log AS log',
        'autoid AS autoid'];

        $packages = Package::getAll($col, $limit, $offset);

        if($packages)
        {
            array_push($packages, array('max_log_id' => $max_log_id, 'count' => $count, 'status' => 1, 'limit'=>$limit, 'offset'=>$offset));
            return $this->sendResponse($packages, 'Package Retrieved Successfully.');   //OK
        }
        else
        {
            return $this->sendResponse(array(array('max_log_id' => $max_log_id, 'count' => $count, 'status'=>0, 'error' => '0 row' )));
        }
    }

    //Package PUT Method

    public function packages_post(Request $request, $limit= NULL, $offset = NULL)
    {
        $packages = new Package();

        $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        if( $request->updatetime != false )
        {
            if (strpos($request->updatetime, '|') !== false)
            {
            	$ids = explode('|',$request->id);
                $_ids = explode('|',$request->_id);
                $dids = explode('|',$request->did);
                $projects = explode('|',$request->project);
  				$names = explode('|',$request->name);
                $iids = explode('|',$request->iid);
                $icodes = explode('|',$request->icode);
                $inames = explode('|',$request->iname);
                $qtys = explode('|',$request->qty);
                $prices = explode('|',$request->price);
                $updatetimes = explode('|',$request->updatetime);
                $states = explode('|',$request->state);
                $warehouses = explode('|',$request->warehouse);




                for($i = 0; $i < count($updatetime); $i++)
                {
                    $datas[] = array(
                            'id' => $ids[$i],
                            '_id' => $_ids[$i],
                            'did' => $dids[$i],
                            'project' => $projects[$i],
                            'name' => $names[$i],
                            'iid' => $iids[$i],
                            'icode' => $icodes[$i],
                            'iname' => $inames[$i],
                            'qty' => $qtys[$i],
                            'updatetime' => $updatetimes[$i],
                            'log' => 0,
                            'state' => $states[$i],
                            'warehouse' =>  (strpos($request->warehouse, '|') !== false) ? $warehouses[$i] : "",
                            'price' => $prices[$i],
                        );

                }
            }

            else    // qty does not have | char
            {

                $datas[] = array(

                        'id'       => $request->id,
                        '_id'       => $request->_id,
                        'did'       => $request->did,
                        'project'     => $request->project,
                        'name'     => $request->name,
                        'iid'     => $request->iid,
                        'icode' => $request->icode,
                        'iname'    => $request->iname,
                        'qty' => $request->qty,
                        'updatetime' => $request->updatetime,
                        'log' => 0,
                        'state' => $request->state,
                        'warehouse' =>  ($request->warehouse != null) ? $request->warehouse : "",
                        'price' => ($request->price != null) ? $request->price : "",
                    );
            }

            foreach($datas as $data)
            {
                Package::updateInsert($data);
            }

            return $this->packages_get($request, $limit, $offset);
         }
         else
         {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);

         }
    }

}
