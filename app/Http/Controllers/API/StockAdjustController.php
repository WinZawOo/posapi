<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Imei;
use App\StockAdjust;
use Illuminate\Http\Request;

class StockAdjustController extends BaseController
{
    //StockAdjust GET
    public function stockadjusts_get(Request $request, $limit = null, $offset = null)
    {
        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $count = StockAdjust::all()->count();
        $max_log_id = StockAdjust::max('log');
        if($count == null) $count = 0;
        if($max_log_id == null ) $max_log_id = 0;

        $col = array(
                'id',
                '_id',
                'did AS did',
                'stockid AS stockid',
                'itemid AS itemid',
                'qty AS qty',
                'qtynew AS qtynew',
                'differ AS differ',
                'remark AS remark',
                'updatetime AS updatetime',
                'state AS state',
                'log AS log',
//                'wname AS wname',
                'autoid AS autoid'
            );
        $message = StockAdjust::GetData($col, $limit, $offset)->toArray();
        if($message){
            array_push($message, array('max_log_id'=>$max_log_id, 'count'=>$count, 'status'=>1,'limit'=>$limit, 'offset'=>$offset));
            return $this->sendResponse($message);
        }else{
            return $this->sendError(array('max_log_id' => $max_log_id, 'count' => $count, 'status'=>0, 'error' => '0 row' ));
        }
    }

    //Stockadjusts POST
    public function stockadjusts_post(Request $request, $limit = null, $offset = null){

        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $datas = array();
        if( $request->updatetime != null ) {
            if (strpos($request->updatetime, '|') !== false) { // name has | char

                $_ids = explode('|', $request->_id);
                $stockids = explode('|', $request->stockid);
                $itemids = explode('|', $request->itemid);
                $qtys = explode('|', $request->qty);
                $qtynews = explode('|', $request->qtynew);
                $differs = explode('|', $request->differ);
                $remarks = explode('|', $request->remark);
                $updatetimes = explode('|', $request->updatetime);
                $states = explode('|', $request->state);
                $dids = explode('|', $request->did);
                $ids = explode('|', $request->id);
//                $wnames = explode('|', $request->wname);

                for ($i = 0; $i < count($updatetimes); $i++) {
                    $datas[] = array(
                        'kid' => $this->keyId,
                        'did' => $dids[$i],
                        '_id' => $_ids[$i],
                        'id' => $ids[$i],
                        'stockid' => $stockids[$i],
                        'itemid' => $itemids[$i],
                        'qty' => $qtys[$i],
                        'qtynew' => $qtynews[$i],
                        'differ' => $differs[$i],
                        'remark' => $remarks[$i],
                        'updatetime' => $updatetimes[$i],
                        'state' => $states[$i],
//                        'wname' => $wnames[$i],
                        'log' => 0
                    );
                }
            } else    // name does not have | char
            {
                $datas[] = array(
                    'kid' => $this->keyId,
                    'did' => $request->did,
                    'id' => $request->id,
                    '_id' => $request->_id,
                    'stockid' => $request->stockid,
                    'itemid' => $request->itemid,
                    'qty' => $request->qty,
                    'qtynew' => $request->qtynew,
                    'differ' => $request->differ,
                    'remark' => $request->remark,
                    'updatetime' => $request->updatetime,
                    'state' => $request->state,
//                    'wname' => $request->wname,
                    'log' => 0
                );
            }
            foreach ($datas as $data){
                StockAdjust::insertUpdate($data, ['id'=>$data['id']]);
            }
        }
        return $this->stockadjusts_get($request,$limit,$offset);
    }
}
