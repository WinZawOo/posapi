<?php

namespace App\Http\Controllers\API;

use App\Imei;
use App\Invoice;
use App\InvoiceDetail;
use App\InvoiceUpdate;
use App\ShopStock;
use Defuse\Crypto\File;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use Illuminate\Support\Arr;
//use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rules\In;
use Laravie\Parser\Xml\Document;
use Laravie\Parser\Xml\Reader;
use Tightenco\Collect\Support\Collection;
use Madnest\Madzipper\Madzipper;


class InvoiceController extends BaseController
{
    //Salesorders GET
    public function salesorders_get(Request $request)
    {
        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $col1 = array(
            'id AS id',
            '_id AS _id',
            'did AS Did',
            'sid AS Shopid',
            'cod AS Code',
            'cid AS Custid',
            'ccd AS Custcode',
            'ccn AS CustCardname',
            'cnm AS Custname',
            'uid AS Userid',
            'amt AS Amount',
            'dis AS Discount',
            'dks AS Discountks',
            'tax AS Tax',
            'ttl AS Total',
            'pid AS Paid',
            'chg AS Change',
            'rmn AS Remain',
            'qty AS qty',
            'sta AS State',
            'rmk AS Remark',
            'udt AS Updatetime',
            'version AS ver',
            'prt AS prt',
            'eve AS eve',
            'cpn AS cpn',
            'dlr AS dlr',
            'drm AS drm',

            'prj AS Project',
            'brh AS Branch',
            'dtr AS Dist',
            'slt AS Salestype',
            'dpt AS Dept',
            'shp AS Shipping',
            'adr AS Address',
            'dld AS Deldate',
            'slp AS Salesperson',

            'srs AS Series',
            'srn AS Seriesname',
            'det AS Docentry',
            'dnb AS Docnum',
            'cod AS dcc',
            'caa AS Carrier',
            'gnb AS Groupnum',
            'ucd AS Usercode'
        );
        $message = Invoice::select($col1)->where('upd','=', '1')->get()->toArray();
        if ($message) {
            return $this->sendResponse($message);
        } else {
            return $this->sendError(array('status' => 0, 'rows' => 0));
        }
    }

    //SalesordersDetails GET
    public function salesorderdetails_get(Request $request)
    {
        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $message = InvoiceDetail::SaleOrderDetail();
        if ($message) {
            return $this->sendResponse($message);
        } else {
            return $this->sendError(array('status' => 0, 'rows' => 0));
        }
    }

    //SalesorderOpens GET
    public function salesorderopens_get(Request $request)
    {
        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $message = Invoice::select('id','det')->where([['sta','=', 1], ['dnb','<>', ""]])->get()->toArray();
        if ($message) {
            return $this->sendResponse($message);
        } else {
            return $this->sendError(array('status' => 0, 'rows' => 0));
        }
    }

    //Maxids GET
    public function maxids_get(Request $request, $did)
    {
        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $message = Invoice::where('did', $did)->max('_id');
        if ($message) {
            return $this->sendResponse(array('status' => 1, '_id' => $message));
        } else {
            return $this->sendError(array('status' => 0, 'rows' => 0));
        }
    }

    //Doccloses GET
    public function doccloses_get(Request $request)
    {
        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $col = array(
            'id',
            'iid',
            'did',
            'act',
            'sta',
            'cdt',
            'udt'
        );
        $message = InvoiceUpdate::select($col)->where('sta','=', 0)->get()->toArray();
        if ($message) {
            return $this->sendResponse($message);
        } else {
            return $this->sendError(array('status' => 0, 'rows' => 0));
        }
    }

    //SalesorderUpdates GET
    public function salesorderupdates_get(Request $request)
    {
        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $col = array(
            'id',
            'iid',
            'did',
            'act',
            'sta',
            'cdt',
            'udt'
        );
        $message = InvoiceUpdate::select($col)->where('sta', '=', 0)->get()->toArray();
        if ($message) {
            return $this->sendResponse($message);
        } else {
            return $this->sendError(array('status' => 0, 'rows' => 0));
        }
    }

    //LastInvoices GET
    public function lastinvoices_get(Request $request, $did)
    {
        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $count = Invoice::all()->count();
        $max_log_id = Invoice::max('log');

        if ($count == null) $count = 0;
        if ($max_log_id == null) $max_log_id = 0;

        $col1 = array(
            'id	 AS id',
            '_id AS _id',
            'did AS did',
            'sid AS sid',
            'cod AS cod',
            'cid AS cid',
            'ccd AS ccd',
            'uid AS uid',
            'amt AS amt',
            'dis AS dis',
            'dks AS dks',
            'tax AS tax',
            'ttl AS ttl',
            'pid AS pid',
            'chg AS chg',
            'rmn AS rmn',
            'qty AS qty',
            'sta AS sta',
            'rmk AS rmk',
            'udt AS udt',
            'version AS ver',
            'prt AS prt',
            'eve AS eve',
            'cpn AS cpn',
            'dlr AS dlr',
            'dle AS dle',
            'drm AS drm',

            'prj AS prj',
            'brh AS brh',
            'dtr AS dtr',
            'slt AS slt',
            'dpt AS dpt',
            'shp AS shp',

            'srs AS srs',
            'srn AS srn',
            'det AS det',
            'dnb AS dnb',
            'cod AS dcc',
            'ccn AS ccn',
            'caa AS caa',
            'gnb AS gnb',
            'slp AS slp',
            'cnm AS cnm',
            'ucd AS ucd',
            'adr AS adr',
            'dld AS dld',
            'upd AS upd',
//                'aut AS aid',
            'cdt AS cdt',
            'pty AS pty',
            'log AS log',
            'autoid AS autoid'
        );

        $col2 = array(
            'id AS id',
            '_id AS _i',
            'did AS di',
            'inv',
            'iid AS ii',
            'sid AS si',
            'qty AS qt',
            'prc AS pr',
            'rmk AS rm',
            'udt AS ud',
            'version AS ve',
            'foc AS fo',
            'srl AS sr',
            'bth AS bt',
            'dlr AS dl',
            'duid AS du',
            'dtm AS dt',
            'drv AS dr',
            'vhl AS vh',
            'brd AS br',
            'dis AS ds',
            'car AS ca',
            'prj AS pj',
            'upd AS up',
            'sta AS st',
            'log AS lo',
            'autoid AS au'
        );

        $message = Invoice::GetLastAllDetail($col1, $col2, $did);
        if ($message) {
            array_push($message, array('max_log_id' => $max_log_id, 'count' => $count, 'status' => 1,
                'limit' => 1, 'offset' => 0));
            return $this->sendResponse($message);
        } else {
            return $this->sendError(array('status' => 0, 'rows' => 0));
        }
    }

    //Invoices GET
    public function invoices_get(Request $request, $limit = null, $offset = null, $oversales = array())
    {
        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $count = Invoice::all()->count();
        $max_log_id = Invoice::max('log');

        if ($count == null) $count = 0;
        if ($max_log_id == null) $max_log_id = 0;

        $col1 = array(
            'id	 AS id',
            '_id AS _id',
            'did AS did',
            'sid AS sid',
            'cod AS cod',
            'cid AS cid',
            'ccd AS ccd',
            'uid AS uid',
            'amt AS amt',
            'dis AS dis',
            'dks AS dks',
            'tax AS tax',
            'ttl AS ttl',
            'pid AS pid',
            'chg AS chg',
            'rmn AS rmn',
            'qty AS qty',
            'sta AS sta',
            'rmk AS rmk',
            'udt AS udt',
            'version AS ver',
            'prt AS prt',
            'eve AS eve',
            'cpn AS cpn',
            'dlr AS dlr',
            'dle AS dle',
            'drm AS drm',

            'prj AS prj',
            'brh AS brh',
            'dtr AS dtr',
            'slt AS slt',
            'dpt AS dpt',
            'shp AS shp',

            'srs AS srs',
            'srn AS srn',
            'det AS det',
            'dnb AS dnb',
            'cod AS dcc',
            'ccn AS ccn',
            'caa AS caa',
            'gnb AS gnb',
            'slp AS slp',
            'cnm AS cnm',
            'ucd AS ucd',
            'adr AS adr',
            'dld AS dld',
            'upd AS upd',
//                'aut AS aid',
            'cdt AS cdt',
            'pty AS pty',
            'log AS log',
            'autoid AS autoid'
        );

        $col2 = array(
            'id AS id',
            '_id AS _i',
            'did AS di',
            'inv',
            'iid AS ii',
            'sid AS si',
            'qty AS qt',
            'prc AS pr',
            'rmk AS rm',
            'udt AS ud',
            'version AS ve',
            'foc AS fo',
            'srl AS sr',
            'bth AS bt',
            'dlr AS dl',
            'duid AS du',
            'dtm AS dt',
            'drv AS dr',
            'vhl AS vh',
            'brd AS br',
            'dis AS ds',
            'car AS ca',
            'prj AS pj',
            'upd AS up',
            'sta AS st',
            'log AS lo',
            'autoid AS au'
        );

        $message = Invoice::GetAllDetail($col1, $col2, $limit, $offset);
        if ($message) {
            array_push($message, array('max_log_id' => $max_log_id, 'count' => $count, 'status' => 1,
                'limit' => 1, 'offset' => 0));
            return $this->sendResponse($message);
        } else {
            return $this->sendError(array('status' => 0, 'rows' => 0));
        }
    }

    //Salesorders POST
    public function salesorders_post(Request $request)
    {
        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $updated = false;

        if ($request->code != null) {

            $orders = explode('|', $request->code);

            if (strpos($orders[0], '|') !== false) { // name has | char

                $codes = explode('|', $orders[0]);

                for ($i = 0; $i < count($codes); $i++) {
                    $arr = explode(',', $codes[$i]);
                    $datas = array(
                        'id' => $arr[0],
                        'docentry' => $arr[1],      //docentry 60009,94055,180103376,1694
                        'docnum' => $arr[2],
                        'series' => $arr[3],
//                    'seriesname' => $arr[4]
                    );
                }

            } else    // name does not have | char
            {
                $arr = explode(',', $orders[0]);
                if ($arr[0] != "") {
                    $datas = array(
                        'id' => $arr[0],
                        'docentry' => $arr[1],      //docentry
                        'docnum' => $arr[2],
                        'series' => $arr[3],
                        //                    'seriesname' => $arr[4]
                    );
                }

            }

            if ($datas['id'] > 0) {
                Invoice::updateCode('Invoice', $datas);
                $updated = true;
            }

            /*****************************Invoice Detail********************************************/

            if (strpos($orders[1], '|') !== false) { // name has | char

                $codes = explode('|', $orders[1]);
                for ($i = 0; $i < count($codes); $i++) {
                    $arr = explode(',', $codes[$i]);
                    $datasd[] = array(
                        'id' => $arr[0],
                        'linenum' => $arr[1],      //docentry 60009,94055,180103376,1694
                    );
                }
            } else    // name does not have | char
            {
                $arr = explode(',', $orders[1]);
                if ($arr[0] != "") {
                    $datas = array(
                        'id' => $arr[0],
                        'linenum' => $arr[1],      //docentry
                    );
                }
            }
            if ($datas['id'] > 0) {
                Invoice::updateCode('InvoiceDetail', $datas);
                $updated = true;
            }

            //--------------- sap closed ----------

            if (strpos($orders[2], '|') !== false) { // name has | char
                $arrs = explode('|', $orders[2]);
                foreach ($arrs as $data) {
                    $datasc = array('id' => $data);
                }
            } else    // name does not have | char
            {
                $datasc = array(
                    'id' => $orders[2]    //docentry
                );
            }

            if ($datasc['id'] > 0) {
                Invoice::updateClosed($datasc);
                $updated = true;
            }

            //--------------- sap cancel ----------
            if (strpos($orders[3], '|') !== false) { // name has | char
                $arrs = explode('|', $orders[3]);
                foreach ($arrs as $data) {
                    $datasCancel = array('id' => $data);
                }
            } else    // name does not have | char
            {
                $datasCancel = array(
                    'id' => $orders[3]    //docentry
                );
            }

            if ($datasCancel['id'] > 0) {
                Invoice::updateCanceled($datasCancel);
                $updated = true;
            }

        }
        if ($updated) {
            $message = array('status' => 1, 'msg' => 'Updated!');
            return $this->sendResponse($message);
        } else {
            return $this->sendError(array('status' => 0, 'msg' => 'No updated!'));
        }
    }

    //Invoices POST
    public function invoices_post(Request $request, $limit = null, $offset = null)
    {
        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        if ($request->ovr != null) {
            if ($request->ovr == '0') {
                if (strpos($request->ud, '|') !== false) { // updatetime has | char
                    $shopids = explode('|', $request->si, true);
                    $invoiceids = explode('|', $request->in, true);
                    $itemids = explode('|', $request->ii, true);
                    $ids = explode('|', $request->id, true);
                    $prices = explode('|', $request->pr, true);
                    $qtys = explode('|', $request->qt, true);

                    for ($i = 0; $i < count($shopids); $i++) {
                        $tochecks = array(
                            'sid' => $shopids[$i],
                            'inv' => $invoiceids[$i],
                            'iid' => $itemids[$i],
                            'prc' => $prices[$i],
                            'qty' => $qtys[$i],
                            'id' => $ids[$i],

                        );
                    }
                } else    // name does not have | char
                {
                    $tochecks = array(
                        'sid' => $request->si,
                        'inv' => $request->in,
                        'iid' => $request->ii,
                        'prc' => $request->pr,
                        'qty' => $request->qt,
                        'id' => $request->id,
                    );
                }

                if ($tochecks['id'] > 0) {
                    $row = ShopStock ::where(['iid' => $tochecks['iid'], 'sid' => $tochecks['sid']])->get()->toArray();
                    if (count($row) > 0) {
                        if ($tochecks['qty'] > $row[0]['qty']) {
                            $oversales[] = array(
                                'sid' => $tochecks['sid'],
                                'inv' => $tochecks['inv'],
                                'iid' => $tochecks['iid'],
                                'prc' => $tochecks['prc'],
                                'qty' => $tochecks['qty'],
                                'id' => $tochecks['id'],
                                'sqty' => $row[0]['qty']
                            );
                        }
                    }
                }
            }
        }
        if ($request->udt != null) {
            if (strpos($request->udt, '|') !== false) { // name has | char
                $_ids = explode('|', $request->_id, true);
                $shopids = explode('|', $request->sid, true);
                $dids = explode('|', $request->did, true);
                $codes = explode('|', $request->cod, true);
                $customerids = explode('|', $request->cid, true);
                $customercodes = explode('|', $request->ccd, true);
                $userids = explode('|', $request->uid, true);
                $amounts = explode('|', $request->amt, true);
                $discounts = explode('|', $request->dis, true);
                $discountkss = explode('|', $request->dks, true);
                $taxs = explode('|', $request->tax, true);
                $totals = explode('|', $request->ttl, true);
                $paids = explode('|', $request->pid, true);
                $changes = explode('|', $request->chg, true);
                $remains = explode('|', $request->rmn, true);
                $qtys = explode('|', $request->qty, true);
                $states = explode('|', $request->sta, true);
                $remarks = explode('|', $request->rmk, true);
                $updatetimes = explode('|', $request->udt, true);
                $versions = explode('|', $request->ver, true);
                $printeds = explode('|', $request->prt, true);
                $events = explode('|', $request->eve, true);
                $coupons = explode('|', $request->cpn, true);
                $delivers = explode('|', $request->dlr, true);
                $dremarks = explode('|', $request->drm, true);
                $idas = explode('|', $request->ida, true);

                $prjs = explode('|', $request->prj, true);
                $brhs = explode('|', $request->brh, true);
                $dtrs = explode('|', $request->dtr, true);
                $slts = explode('|', $request->slt, true);
                $dpts = explode('|', $request->dpt, true);
                $shps = explode('|', $request->shp, true);
                $caas = explode('|', $request->caa, true);
                $srns = explode('|', $request->srn, true);
                $srss = explode('|', $request->srs, true);
                $gnbs = explode('|', $request->gnb, true);
                $slps = explode('|', $request->slp, true);
                $cnms = explode('|', $request->cnm, true);
                $ucds = explode('|', $request->ucd, true);   // user code

                $adrs = explode('|', $request->adr, true);   // printing address
                $dlds = explode('|', $request->dld, true);   // printing address

                $upds = explode('|', $request->upd, true);   // to update sap, 1 = yes
                $ptys = explode('|', $request->pty, true);   // to update sap, 1 = yes

                for ($i = 0; $i < count($upds); $i++) {
                    $upds[$i] = $upds[$i] == '' ? 1 : $upds[$i];
                    $ptys[$i] = $ptys[$i] == '' ? 1 : $ptys[$i];
                }

                for ($i = 0; $i < count($updatetimes); $i++) {
                    $datas[] = array(
                        //'tb' 		=> 'tb_invoice', // for versions use
                        'kid' => $this->keyId,
                        'did' => $dids[$i],
                        '_id' => $_ids[$i],
                        'cod' => $codes[$i],
                        'sid' => $shopids[$i],
                        'cid' => $customerids[$i],
                        'ccd' => $customercodes[$i],
                        'uid' => $userids[$i],
                        'amt' => $amounts[$i],
                        'dis' => $discounts[$i],
                        'dks' => $discountkss[$i],
                        'tax' => $taxs[$i],
                        'ttl' => $totals[$i],
                        'pid' => $paids[$i],
                        'chg' => $changes[$i],
                        'rmn' => $remains[$i],
                        'qty' => $qtys[$i],
                        'sta' => $states[$i],
                        'rmk' => $remarks[$i],
                        'udt' => $updatetimes[$i],
                        'syc' => 1,
                        'log' => 0,
                        'prt' => $printeds[$i],
                        'eve' => $events[$i],
                        'cpn' => $coupons[$i],
                        'dlr' => $delivers[$i],
                        'drm' => $dremarks[$i],
                        'id' => $idas[$i],

                        'prj' => $prjs[$i],
                        'brh' => $brhs[$i],
                        'dtr' => $dtrs[$i],
                        'slt' => $slts[$i],
                        'dpt' => $dpts[$i],
                        'shp' => $shps[$i],
                        'caa' => $caas[$i],
                        'srn' => $srns[$i],
                        'srs' => $srss[$i],
                        'gnb' => $gnbs[$i],
                        'slp' => $slps[$i],
                        'cnm' => $cnms[$i],
                        'ucd' => $ucds[$i],
                        'adr' => $adrs[$i],
                        'dld' => $dlds[$i],
                        'upd' => $upds[$i],
                        'pty' => $ptys[$i]
                    );
                }
            } else {
                $datas[] = array(
                    //'tb' 		=> 'tb_invoice', // for versions use
                    'kid' => $this->keyId,
                    'did' => $request->did,
                    '_id' => $request->_id,
                    'cod' => $request->cod,
                    'sid' => $request->sid,
                    'cid' => $request->cid,
                    'ccd' => $request->ccd,
                    'ccn' => $request->ccn,
                    'uid' => $request->uid,
                    'amt' => $request->amt,
                    'dis' => $request->dis,
                    'dks' => $request->dks,
                    'tax' => $request->tax,
                    'ttl' => $request->ttl,
                    'pid' => $request->pid,
                    'chg' => $request->chg,
                    'rmn' => $request->rmn,
                    'qty' => $request->qty,
                    'sta' => $request->sta,
                    'rmk' => $request->rmk,
                    'udt' => $request->udt,
                    'syc' => 1,
                    'log' => 0,
                    'prt' => $request->prt,
                    'eve' => $request->eve,
                    'cpn' => $request->cpn,
                    'dlr' => $request->dlr,
                    'drm' => $request->drm,
                    'id' => $request->ida,

                    'prj' => $request->prj,
                    'brh' => $request->brh,
                    'dtr' => $request->dtr,
                    'slt' => $request->slt,
                    'dpt' => $request->dpt,
                    'shp' => $request->shp,
                    'caa' => $request->caa,
                    'srn' => $request->srn,
                    'srs' => $request->srs,
                    'gnb' => $request->gnb,
                    'slp' => $request->slp,
                    'cnm' => $request->cnm,
                    'ucd' => $request->ucd,
                    'adr' => $request->adr,
                    'dld' => $request->dld,
                    'upd' => $request->upd == '' ? 1 : $request->upd,
                    'pty' => $request->pty == '' ? 1 : $request->pty,
                );

            }
            foreach($datas as $data)
            {
                if($data['id'] > 0){
                    $found = false;
                    foreach ($oversales as $oversale) {
                        if($oversale['inv'] == $data['id']){
                            $found = true;
                            continue;
                        }
                    }
                    if(!$found)
                       Invoice::insertUpdate("Invoice",$data,['id'=>$data['id']]);
                }
            }

            //---------------------------------- invoice detail ---------------------------------------------------------

            if (strpos($request->ud, '|') !== false) { // updatetime has | char
                $_ids = explode('|', $request->_i, true);
                $dids = explode('|', $request->di, true);
                $shopids = explode('|', $request->si, true);
                $invoiceids = explode('|', $request->in, true);
                $itemids = explode('|', $request->ii, true);
                $ids = explode('|', $request->ids, true);
                $prices = explode('|', $request->pr, true);
                $qtys = explode('|', $request->qt, true);
                $remarks = explode('|', $request->rm, true);
                $updatetimes = explode('|', $request->ud, true);
                $ids = explode('|', $request->id, true);
                $focs = explode('|', $request->fo, true);
                $bths = explode('|', $request->bt, true);
                $srls = explode('|', $request->sr, true);
                $dlrs = explode('|', $request->dl, true);
                $duids = explode('|', $request->du, true);
                $dtms = explode('|', $request->dt, true);
                $drvs = explode('|', $request->dr, true);
                $vhls = explode('|', $request->vh, true);

                $brds = explode('|', $request->br, true);
                $diss = explode('|', $request->ds, true);
                $cars = explode('|', $request->ca, true);
                $prjs = explode('|', $request->pj, true);
                $stas = explode('|', $request->st, true);
                $upds = explode('|', $request->up, true);


                for ($i = 0; $i < count($upds); $i++) {
                    $upds[$i] = $upds[$i] == '' ? 1 : $upds[$i];
                }

                for ($i = 0; $i < count($updatetimes); $i++) {
                    $datas2[] = array(
                        //'tb' 		=> 'tb_invoicedetail', // for versions use
                        'kid' => $this->rest->id,
                        'did' => $dids[$i],
                        '_id' => $_ids[$i],
                        'sid' => $shopids[$i],
                        'inv' => $invoiceids[$i],
                        'iid' => $itemids[$i],
                        'prc' => $prices[$i],
                        'qty' => $qtys[$i],
                        'rmk' => $remarks[$i],
                        'udt' => $updatetimes[$i],
                        'syc' => 1,
                        'id' => $ids[$i],
                        'foc' => $focs[$i],
                        'bth' => $bths[$i],
                        'srl' => $srls[$i],
                        'dlr' => $dlrs[$i],
                        'duid' => $duids[$i],
                        'dtm' => $dtms[$i] . ' 00:00:00',
                        'drv' => $drvs[$i],
                        'vhl' => $vhls[$i],

                        'dis' => $diss[$i],
                        'brd' => $brds[$i],
                        'car' => $cars[$i],
                        'prj' => $prjs[$i],
                        'upd' => $upds[$i],
                        'sta' => $stas[$i]

                    );
                }
            } else    // name does not have | char
            {
                $datas2[] = array(
                    //'tb' 		=> 'tb_invoicedetail', // for versions use
                    'kid' => $this->keyId,
                    'did' => $request->di,
                    '_id' => $request->_i,
                    'sid' => $request->si,
                    'inv' => $request->in,
                    'iid' => $request->ii,
                    'prc' => $request->pr,
                    'qty' => $request->qt,
                    'rmk' => $request->rm,
                    'udt' => $request->ud,
                    'syc' => 1,
                    'id' => $request->ids,
                    'foc' => $request->fo,
                    'bth' => $request->bt,
                    'srl' => $request->sr,

                    'dlr' => $request->dl,

                    'duid' => $request->du,
                    'dtm' => $request->dt . ' 00:00:00',
                    'drv' => $request->dr,
                    'vhl' => $request->vh,

                    'dis' => $request->ds,
                    'brd' => $request->br,
                    'car' => $request->ca,
                    'prj' => $request->pj,
                    'upd' => $request->up == '' ? 1 : $this->input->post('up', true),
                    'sta' => $request->st

                );
            }
            foreach ($datas2 as $data) {
                if ($data['id'] > 0) {
                    foreach ($oversales as $oversale) {
                        $found = false;
                        if ($oversale['id'] == $data['id']) {
                            $found = true;
                            continue;
                        }
                    }
                    if (!$found)
                        Invoice::insertUpdate("InvoiceDetail", $data, ['id' => $data['id']]);
                }
            }
        }
        if ($request->del_id != null){
            $data = array(
                'id' => $request->del_id,
                'rmk' => $request->rmk,
                'sta' => 0,
                'upd' => 1,
            );
            Invoice::del($data);
        }
        if ($request->remain_id != null){
            $data = array(
                'id' => $request->remain_id,
                'rmn' => $request->rmn,
                'rmk' => $request->rmk,
                'pid' => $request->pid,
                'log' => 0
            );
            Invoice::where('id',$data['id'])->update($data);
        }
        if($request->change_id != null){
            $data = array(
                'inv_id' => $request->change_id,
                'iid' => $request->iid,
                'qty' => $request->qty,
                'det_id' => $request->det_id,
                'from' => $request->from,
                'to' => $request->to
            );
            Invoice::updateInvoiceChangeStock($data);
        }
        return $this->invoices_get($request,$limit,$offset);
    }

    //Openqtys POST
    public function openqtys_post(Request $request){
        $request->validate([
            'zip_file' => 'required|mimetypes:application/zip,application/x-zip-compressed,multipart/x-zip,application/x-compressed',
        ]);

        $filename = $request->file('zip_file')->getClientOriginalName();
        $name = explode('.',$filename);

        if($path = $request->file('zip_file')->move(public_path('xml'),$filename)){
           (new Madzipper)->zip($path)->extractTo(public_path('xml'));
           unlink($path);
           $this->sendResponse(array('msg'=>'Your .zip file was uploaded and unpacked.'));
        }else{
            $this->sendError(array('msg' => 'There was a problem with the upload. Please try again.'));
        }
        $xmlpath = public_path("xml/".$name[0].'.xml');
        if(file_exists($xmlpath)){
            $reader = new \XMLReader();
            if(!$reader->open($xmlpath)){
                $this->sendError(array('status'=>0));
            }else{
                $strReturn = '';
                while ($reader->read()){
                    if($reader->nodeType == \XMLReader::ELEMENT){
                        $arr = array();
                        if($reader->localName == 'ivn'){
                            $reader->read();
                            $arr['inv'] = $reader->value;
                            $strReturn .= $reader->value.',';
                        }
                        if($reader->localName == 'openqty'){
                            $reader->read();
                            $arr['openqty'] = $reader->value;
                            $strReturn .= $reader->value.',';
                        }
                        if($reader->localName == 'u_pos'){
                            $reader->read();
                            $arr['u_pos'] = $reader->value;
                            $strReturn .= $reader->value.' - ';
                            Invoice::updateOpenQty($arr);
                        }
                    }
                }
                $reader->close();
                $this->sendResponse(array('status'=>1, 'msg'=>'Success!','updated' => $strReturn));
            }
        }else{
            $this->sendError(array('status'=>0, 'msg'=>'Error Uploading'));
        }
    }

}
