<?php

namespace App\Http\Controllers\API;

use App\Imei;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\BaseController;
use App\Http\Resources\Category as CategoryResource;

class CategoryController extends BaseController
{

    //Category GET
    public function cats_get(Request $request, $limit , $offset ){

        if ( Imei::where([['s', '=', $request->header('s')], ['simid', '=', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $count = Category::all()->count();
        $max_log_id = Category::max('log');

        if($count == null) $count = 0;
        if($max_log_id == null) $max_log_id = 0;

        $col  = array(
                'id',
                '_id',
                'did',
                'code',
                'name',
                'updatetime',
                'state',
                'log',
                'autoid'
            );

        $message = Category::GetData($col, $limit, $offset);

        if($message){
            array_push($message, array('max_log_id'=>$max_log_id, 'count' => $count, 'status'=>1, 'limit'=>$limit, 'offset' => $offset));
            return $this->sendResponse(CategoryResource::collection($message));
        }else {
            return $this->sendError(array('max_log_id' => $max_log_id, 'count' => $count, 'status' => 0, 'error' => '0 row'));
        }

    }

    //Category POST
    public function cats_post(Request $request, $limit = NULL, $offset = NULL){

        if ( Imei::where([['s', '=', $request->header('s')], ['simid', '=', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $datas = array();

        if(strpos($request->updatetime, '|') !== false){
            $ids = explode('|',$request->id);
            $_ids = explode('|', $request->_id);
            $dids = explode('|', $request->did);
            $codes = explode('|', $request->code);
            $names = explode('|',$request->name);
            $updatetimes = explode('|', $request->updatetime);
            $states = explode('|', $request->state);

            for($i = 0 ; $i < count($updatetimes); $i++){
                $datas[] = array(
                    'id' => $ids[$i],
                    '_id' => $_ids[$i],
                    'did' => $dids[$i],
                    'code' => $codes[$i],
                    'name' => $names[$i],
                    'updatetime' => $updatetimes[$i],
                    'stat' => $states[$i],
                );
            }
        }else {
            if($request->id != ''){
                    $datas[] = array(
                        'id' => $request->id,
                        '_id'=> $request->_id,
                        'did'=> $request->did,
                        'code'=>$request->code,
                        'name' =>$request->name,
                        'updatetime' => $request->updatetime,
                        'state' => $request->state,
                    );
            }
        }
        foreach ($datas as $data) {
            Category::updateOrCreate(['id' => $data['id']], $data);
        }
       return $this->cats_get($request, $limit, $offset);
    }
}
