<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Series;
use App\Imei;

class SeriesController extends BaseController
{
    //Sale Person GET Method
    public function series_get(Request $request, $limit= NULL, $offset = NULL)
    {
        $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        $count = Series::all()->count();
        $max_log_id = Series::get('log')->max();

        if($count == null) $count = 0;
        if($max_log_id == null) $max_log_id = 0;

        $col = '*';

        $series = Series::getAll($col, $limit, $offset);

        if($series)
        {
            array_push($series, array('max_log_id' => $max_log_id, 'count' => $count, 'status' => 1,
                'limit'=>$limit, 'offset'=>$offset));
            return $this->sendResponse($series, 'Series Retrieved Successfully.');   //OK
        }
        else
        {
             return $this->sendResponse(array(array('max_log_id' => $max_log_id, 'count' => $count, 'status'=>0, 'error' => '0 row' )));
        }
    }

    //Sale Person PUT Method

	public function series_post(Request $request, $limit= NULL, $offset = NULL)
	{
	    $series = new Series();

	    $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
	    if($count< 1)
	    {
	        $message = array( 'status' => 0, 'error' => 'No Rights!' );
	        return $this->sendError($message);
	    }

	    if( $request->updatetime != NULL )
	    {
	        if (strpos($request->updatetime, '|') !== false)
	        {
	        	$ids = explode('|',$request->id);
	        	$objectcodes = explode('|', $request->objectcode);
	            $codes = explode('|',$request->code);
	            $names = explode('|',$request->name);
	            $updatetimes = explode('|',$request->updatetime);
	            $states = explode('|',$request->state);

	            for($i = 0; $i < count($updatetime); $i++)
	            {
	            	$datas[] = array(
						'id' => $ids[$i],
						'objectcode' => $objectcodes[$i],
						'code' => $codes[$i],
	                    'name' => $names[$i],
						'updatetime' => $updatetimes[$i],
	                    'state' => $version[$i],
	                    'log' => 0
					);

	            }
	        }

	        else    // updatetime does not have | char
	        {
	        	$datas[] = array(
	        		'id' => $request->id,
	        		'code' => $request->code,
	        		'objectcode' => $request->objectcode,
	        		'name' => $request->name,
	        		'state' => $request->state,
	   				'updatetime' =>$request->updatetime,
	   				'log'=> 0
	        	);

	        }

	        foreach($datas as $data)
	        {
	        	Series::updateInsert($data);
	        }

	        return $this->series_get($request, $limit, $offset);
	     }
	     else
	     {
	        $message = array( 'status' => 0, 'error' => 'No Rights!' );
	        return $this->sendError($message);

	     }
	}
}
