<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Odwd;
use App\Imei;
use Validator;

class OdwdController extends BaseController
{
    //Odwd Get Method
    public function odwds_get(Request $request, $limit= NULL, $offset = NULL)
    {
        $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        $count = Odwd::all()->count();
        $max_log_id = Odwd::get('log')->max();

        if($count == null) $count = 0;
        if($max_log_id == null) $max_log_id = 0;

        $col = '*';

        $odwds = Odwd::getAll($col, $limit, $offset);

        if($odwds)
        {
            array_push($odwds, array('max_log_id' => $max_log_id, 'count' => $count, 'status' => 1,
                'limit'=>$limit, 'offset'=>$offset));
            return $this->sendResponse($odwds, 'Odwd Retrieved Successfully.');   //OK
        }
        else
        {
             return $this->sendResponse(array(array('max_log_id' => $max_log_id, 'count' => $count, 'status'=>0, 'error' => '0 row' )));
        }
    }

    //Odwd PUT Method

    public function odwds_post(Request $request, $limit= NULL, $offset = NULL)
    {
        $odwd = new Odwd();

        $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        if( $request->updatetime != false )
        {
            if (strpos($request->updatetime, '|') !== false)
            {
            	$ids = explode('|',$request->id);
                $codes = explode('|',$request->code);
                $names = explode('|',$request->name);
                $itemgroups = explode('|',$request->itemgroup);
  				$groupss = explode('|',$request->groups);
                $versions = explode('|',$request->version);
                $updatetimes = explode('|',$request->updatetime);


                for($i = 0; $i < count($updatetime); $i++)
                {
                    $datas[] = array(
                        'id' => $ids[$i],
                        'code' => $codes[$i],
                        'name' => $names[$i],
                        'itemgroup' => $itemgroups[$i],
                        'groups' => $groupss[$i],
                        'version' => $versions[$i],
                        'updatetime' => $updatetimes[$i] );

                }
            }

            else    // qty does not have | char
            {
                $datas[] = array(
                    'id' =>$request->id,
                    'code' => $request->code,
                    'name' => $request->name,
                    'itemgroup' => $request->itemgroup,
                    'groups' => $request->groups,
                    'version' => $request->version,
                    'updatetime' =>$request->updatetime
                );

            }

            foreach($datas as $data)
            {
                Odwd::updateInsert($data);
            }

            return $this->odwds_get($request, $limit, $offset);
         }
         else
         {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);

         }
     }

}
