<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Master;
use App\Address;
use App\Imei;
use App\Uploads;

class MasterController extends BaseController
{
    //Master GET Method
    public function masters_get($s, $salt)
    {
    	$message = array( 'status' => 0, 'error' => 'No Rights!' );
        return $this->sendError($message);
    }

    //Master POST Method
    public function masters_post(Request $request)
    {
    	$count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }
        else if($request->action == 'truncate' || $request->action == 'all')
		{
            $message = Master::all();
            if($message)
			{
				$message['msg'] = array(array('status'=>1));
				return $this->sendResponse($message, 'Master Retrieved Successfully.');	//OK
			}
			else
			{
				$message = array('msg' => array('status'=>0));
				return $this->sendError($message);
			}
		}

		else
        {

            $actions = explode(',',$request->action);

            if(count($actions) > 0){

                $message = array();

                foreach($actions as $action)
                {
                    if($action == 'tb_showroom_addr')
                        $message[$action] =Master::getTbByName('tb_address');
                    else
                        $message[$action] = Master::getTbByName($action);
                }

                if($message)
    			{
    				$message['msg'] = array(array('status'=>1));
    				return $this->sendResponse($message, 'Data Retrieved Successfully.');	//OK
    			}
    			else
    				return $this->sendError(array('msg' => array('status'=>0)), REST_Controller::HTTP_OK);
            }
        }
    }

    public function uploads_post(Request $request)
    {
        $isInit = $request->action == 'init' ? true : false;
        $isAll = $request->isAll == '1' ? true : false;

        $rowCount = 0;

        $zipper = new \Madnest\Madzipper\Madzipper;

        $validator = Validator::make($request->all(), ['zip_file' => 'required | mimetypes:application/zip, application/x-zip-compressed, multipart/x-zip, application/x-compressed']);

        if ($validator->fails())
        {
            return $this->sendError('The file you are trying to upload is not a .zip file. Please try again.', $validator->errors());
        }
        else
        {

            $filename = $request->file('zip_file')->getClientOriginalName();

            $name = explode('.', $filename);

            $rowCount = Uploads::getCount('tb_' . $name[0]);

            if($path= $request->file('zip_file')->move(public_path('xml'),$filename))
            {
                $zipper->zip($path)->extractTo(public_path('xml'));
                $zip->close();
                unlink($path);

                $this->fileToDb($name, $isInit, $isAll, $rowCount);
            }
            $message = "Your .zip file was uploaded and unpacked.";
        }

    }

    public function fileToDb($name, $isInit, $isAll, $rowCount)
    {
        if(file_exists(public_path('xml')))
        {
            $reader = new XMLReader();

            if ( !$reader->open(public_path('xml').$name[0].'.xml'))
            {
                $this->response(array('status' => 0), REST_Controller::HTTP_OK);
            }
            else
            {
                $PricelistCount = 0;
                $PricelistArr = array();
                while($reader->read())
                {
                    // check to ensure nodeType is an Element not attribute or #Text
                    if($reader->nodeType == XMLReader::ELEMENT)
                    {
                        if($name[0] == 'Pricelist')
                        {
                            if($reader->localName == 'Pl') {
                                $reader->read(); // move to its textnode / child
                                $arr['Plist'] = $reader->value;
                            }
                            if($reader->localName == 'Cu') {
                                $reader->read();
                                $arr['Currency'] = $reader->value;
                            }
                            if($reader->localName == 'Pr') {
                                $reader->read();
                                $arr['Price'] = $reader->value;
                            }
                            if($reader->localName == 'Ov') {
                                $reader->read();
                                $arr['Overwritten'] = $reader->value;
                            }
                            if($reader->localName == 'Co') {
                                $reader->read();
                                $arr['Code'] = $reader->value;

                                $PricelistCount++;

                                if($isInit == true && $rowCount == 0)
                                {

                                    if($PricelistCount > 100){
                                        Uploads::insert_batch('tb_' . $name[0], $PricelistArr);
                                        $PricelistCount = 0;
                                        $PricelistArr = array();
                                    }
                                    else{
                                        $PricelistArr[] = array('Plist'=>$arr['Plist'], 'Currency'=>$arr['Currency'], 'Price'=>$arr['Price'], 'Overwritten'=>$arr['Overwritten'], 'Code'=>$arr['Code']);
                                    }

                                }
                                else
                                {
                                    $where = array(['Code'=>$arr['Code'], 'Plist'=>$arr['Plist']]);
                                    Uploads::updateInsert('tb_' . $name[0], $arr, $where);
                                }
                            }
                        }



                        else if($name[0] == 'PricelistName'){

                            if($reader->localName == 'ListName') {
                                $reader->read(); // move to its textnode / child
                                $arr['ListName'] = $reader->value;
                            }
                            if($reader->localName == 'BaseNum') {
                                $reader->read();
                                $arr['BaseNum'] = $reader->value;
                            }
                            if($reader->localName == 'Factor') {
                                $reader->read();
                                $arr['Factor'] = $reader->value;
                            }
                            if($reader->localName == 'PrimCurr') {
                                $reader->read();
                                $arr['PrimCurr'] = $reader->value;
                            }
                            if($reader->localName == 'ListNum') {
                                $reader->read();
                                $arr['ListNum'] = $reader->value;

                                $where = array( 'ListNum'=>$arr['ListNum']);

                                if($isInit == true && $rowCount == 0)
                                {
                                    Uploads::insert('tb_' . $name[0], $arr);
                                }
                                else
                                {
                                    // $arr['log'] = 0; //
                                    Uploads::updateInsert('tb_' . $name[0], $arr, $where);
                                }
                            }

                        }

                        else if($name[0] == 'Odwd'){

                            if($reader->localName == 'Name') {
                                $reader->read(); // move to its textnode / child
                                $arr['Name'] = $reader->value;
                            }
                            if($reader->localName == 'Whs') {
                                $reader->read();
                                $arr['Whs'] = $reader->value;
                            }
                            if($reader->localName == 'Groups') {
                                $reader->read();
                                $arr['Groups'] = $reader->value;
                            }
                            if($reader->localName == 'Itemgroup') {
                                $reader->read();
                                $arr['Itemgroup'] = $reader->value;
                            }
                            if($reader->localName == 'Unit') {
                                $reader->read();
                                $arr['Unit'] = $reader->value;
                            }
                            if($reader->localName == 'Project') {
                                $reader->read();
                                $arr['Project'] = $reader->value;
                            }
                            if($reader->localName == 'Code') {
                                $reader->read();
                                $arr['Code'] = $reader->value;

                                $where = array( 'Code'=>$arr['Code']);

                                if($isInit == true && $rowCount == 0)
                                {
                                    Uploads::insert('tb_' . $name[0], $arr);
                                }
                                else
                                {
                                    // $arr['log'] = 0; //
                                    Uploads::updateInsert('tb_' . $name[0], $arr, $where);
                                }
                            }

                        }

                        else if($name[0] == 'Omad'){

                            if($reader->localName == 'Code') {
                                $reader->read(); // move to its textnode / child
                                $arr['Code'] = $reader->value;
                            }
                            if($reader->localName == 'Area') {
                                $reader->read();
                                $arr['Area'] = $reader->value;
                            }
                            if($reader->localName == 'Name') {
                                $reader->read();
                                $arr['Name'] = $reader->value;

                                $where = array( 'Name'=>$arr['Name']);

                                if($isInit == true && $rowCount == 0)
                                {
                                    Uploads::insert('tb_' . $name[0], $arr);
                                }
                                else
                                {
                                    // $arr['log'] = 0; //
                                    Uploads::updateInsert('tb_' . $name[0], $arr, $where);
                                }
                            }

                        }

                        else if($name[0] == 'Account'){

                            if($reader->localName == 'Segment_0') {
                                $reader->read(); // move to its textnode / child
                                $arr['Segment_0'] = $reader->value;
                            }
                            if($reader->localName == 'Segment_1') {
                                $reader->read();
                                $arr['Segment_1'] = $reader->value;
                            }
                            if($reader->localName == 'AcctName') {
                                $reader->read();
                                $arr['AcctName'] = $reader->value;
                            }
                            if($reader->localName == 'AcctCode') {
                                $reader->read();
                                $arr['AcctCode'] = $reader->value;

                                $where = array( 'AcctCode'=>$arr['AcctCode']);

                                if($isInit == true && $rowCount == 0)
                                {
                                    Uploads::insert('tb_' . $name[0], $arr);
                                }
                                else
                                {
                                    // $arr['log'] = 0; //
                                    Uploads::updateInsert('tb_' . $name[0], $arr, $where);
                                }
                            }

                        }

                        else if($name[0] == 'Branch' || $name[0] == 'BranchLoc' || $name[0] == 'Brand' || $name[0] == 'Carrier' ||
                            $name[0] == 'DistChn' || $name[0] == 'Division' || $name[0] == 'SalesPerson' || $name[0] == 'SalesType' || $name[0] == 'ShippingType' || $name[0] == 'Township' || $name[0] == 'Department' || $name[0] == 'Project' ||  $name[0] == 'GroupNum'  ||  $name[0] == 'CustomerGroup'
                        ){
                            if($reader->localName == 'Name') {
                                $reader->read();
                                $arr['Name'] = $reader->value;
                            }
                            if($reader->localName == 'Code') {
                                $reader->read();
                                $arr['Code'] = $reader->value;

                                $where = array( 'Code'=>$arr['Code']);

                                if($isInit == true && $rowCount == 0)
                                {
                                    Uploads::insert('tb_' . $name[0], $arr);
                                }
                                else
                                {
                                    // $arr['log'] = 0; //
                                    Uploads::updateInsert('tb_' . $name[0], $arr, $where);
                                }
                            }

                        }

                        else if($name[0] == 'Address' ){

                            if($reader->localName == 'U_Company') {
                                $reader->read();
                                $arr['Company'] = $reader->value;
                            }
                            if($reader->localName == 'U_Address') {
                                $reader->read();
                                $arr['Address'] = $reader->value;
                            }
                            if($reader->localName == 'U_Faddress') {
                                $reader->read();
                                $arr['Faddress'] = $reader->value;
                            }
                            if($reader->localName == 'U_Tship') {
                                $reader->read();
                                $arr['Tship'] = $reader->value;
                            }
                            if($reader->localName == 'U_Town') {
                                $reader->read();
                                $arr['Town'] = $reader->value;
                            }
                            if($reader->localName == 'U_Phone1') {
                                $reader->read();
                                $arr['Phone1'] = $reader->value;
                            }
                            if($reader->localName == 'U_Phone2') {
                                $reader->read();
                                $arr['Phone2'] = $reader->value;
                            }
                            if($reader->localName == 'U_Fax') {
                                $reader->read();
                                $arr['Fax'] = $reader->value;
                            }
                            if($reader->localName == 'U_Email') {
                                $reader->read();
                                $arr['Email'] = $reader->value;
                            }
                            if($reader->localName == 'Name') {
                                $reader->read();
                                $arr['Name'] = $reader->value;
                            }
                            if($reader->localName == 'Code') {
                                $reader->read();
                                $arr['Code'] = $reader->value;

                                $where = array( 'Code'=>$arr['Code']);

                                if($isInit == true && $rowCount == 0)
                                {
                                    Uploads::insert('tb_' . $name[0], $arr);
                                }
                                else
                                {
                                    // $arr['log'] = 0; //
                                    Uploads::updateInsert('tb_' . $name[0], $arr, $where);
                                }
                            }

                        }

                        else if($name[0] == 'Series' ){

                            if($reader->localName == 'ObjectCode') {
                                $reader->read();
                                $arr['ObjectCode'] = $reader->value;
                            }
                            if($reader->localName == 'Name') {
                                $reader->read();
                                $arr['Name'] = $reader->value;
                            }
                            if($reader->localName == 'BeginStr') {
                                $reader->read();
                                $arr['BeginStr'] = $reader->value;
                            }
                            if($reader->localName == 'Locked') {
                                $reader->read();
                                $arr['Locked'] = $reader->value;
                            }
                            if($reader->localName == 'Indicator') {
                                $reader->read();
                                $arr['Indicator'] = $reader->value;
                            }
                            if($reader->localName == 'Code') {
                                $reader->read();
                                $arr['Code'] = $reader->value;

                                $where = array( 'Code'=>$arr['Code']);

                                if($isInit == true && $rowCount == 0)
                                {
                                    Uploads::insert('tb_' . $name[0], $arr);
                                }
                                else
                                {
                                    // $arr['log'] = 0; //
                                    Uploads::updateInsert('tb_' . $name[0], $arr, $where);
                                }

                                $arr['BeginStr'] = '';
                                $arr['Indicator'] = '';
                            }

                        }

                        else if($name[0] == 'Contact' ){

                            if($reader->localName == 'Code') {
                                $reader->read();
                                $arr['Code'] = $reader->value;
                            }
                            if($reader->localName == 'Name') {
                                $reader->read();
                                $arr['Name'] = $reader->value;
                            }
                            if($reader->localName == 'Phone') {
                                $reader->read();
                                $arr['Phone'] = $reader->value;
                            }
                            if($reader->localName == 'CardCode') {
                                $reader->read();
                                $arr['CardCode'] = $reader->value;

                                $where = array( 'CardCode'=>$arr['CardCode']);

                                if($isInit == true && $rowCount == 0)
                                {
                                    Uploads::insert('tb_' . $name[0], $arr);
                                }
                                else
                                {
                                    // $arr['log'] = 0; //
                                    Uploads::updateInsert('tb_' . $name[0], $arr, $where);
                                }
                            }

                        }

                        else if($name[0] == 'Warehouse' ){

                            if($reader->localName == 'Location') {
                                $reader->read();
                                $arr['Location'] = $reader->value;
                            }
                            if($reader->localName == 'Name') {
                                $reader->read();
                                $arr['Name'] = $reader->value;
                            }
                            if($reader->localName == 'Code') {
                                $reader->read();
                                $arr['Code'] = $reader->value;

                                $where = array( 'Code'=>$arr['Code']);

                                if($isInit == true && $rowCount == 0)
                                {
                                    Uploads::insert('tb_' . $name[0], $arr);
                                }
                                else
                                {
                                    // $arr['log'] = 0; //
                                    Uploads::updateInsert('tb_' . $name[0], $arr, $where);
                                }
                            }

                        }


                        else if($name[0] == 'Item' ){

                            if($reader->localName == 'inactive') {
                                $reader->read();
                                if($reader->value == 'Y')
                                    $arr['state'] = 0;
                                else
                                    $arr['state'] = 1;
                            }

                            if($reader->localName == 'id') {
                                $reader->read();
                                $arr['id'] = $reader->value;
                            }

                            if($reader->localName == '_id') {
                                $reader->read();
                                $arr['_id'] = $reader->value;
                            }

                            if($reader->localName == 'ItmsGrpCod') {
                                $reader->read();
                                //$arr['catcode'] = $reader->value;
                                $arr['catid'] = $reader->value;
                            }
                            if($reader->localName == 'Brand') {
                                $reader->read();
                                $arr['brand'] = $reader->value;
                            }
                            if($reader->localName == 'Name') {
                                $reader->read();
                                $arr['name'] = $reader->value;
                            }
                            if($reader->localName == 'Code') {
                                $reader->read();
                                $arr['code'] = $reader->value;

                                $where = array( 'code'=>$arr['code']);



                                if($isInit == true && $rowCount == 0)
                                {
                                    Uploads::insert('tb_' . $name[0], $arr);
                                }
                                else
                                {
                                    // $arr['log'] = 0; //
                                    Uploads::updateInsert('tb_' . $name[0], $arr, $where);
                                }
                            }

                        }

                        else if($name[0] == 'Cat' ){

                            if($reader->localName == 'Name') {
                                $reader->read();
                                $arr['name'] = $reader->value;
                            }

                            if($reader->localName == 'Code') {
                                $reader->read();
                                $arr['id'] = $reader->value;
                                $arr['_id'] = $reader->value;
                                $arr['code'] = $reader->value;

                                $where = array( 'code'=>$arr['code']);

                                if($isInit == true && $rowCount == 0)
                                {
                                    Uploads::insert('tb_' . $name[0], $arr);
                                }
                                else
                                {
                                    // $arr['log'] = 0; //
                                    Uploads::updateInsert('tb_' . $name[0], $arr, $where);
                                }
                            }

                        }

                        else if($name[0] == 'ShopStock' ){

                            if($reader->localName == 'itemid') {
                                $reader->read();
                                $arr['iid'] =  $reader->value;
                            }
                            if($reader->localName == 'itemcode') {
                                $reader->read();
                                $arr['itemcode'] =  $reader->value;
                            }
                            if($reader->localName == 'whscode') {
                                $reader->read();
                                $arr['whscode'] =  $reader->value;
                            }
                            if($reader->localName == 'onhand') {
                                $reader->read();
                                $arr['onhand'] =  $reader->value;
                            }
                            if($reader->localName == 'qty') {

                                if($arr['itemcode'] != null && $arr['whscode'] != null){

                                    $reader->read();
                                    $arr['qty'] = $reader->value;

                                    if($row = $this->Uploads->getRowsByColumns('tb_warehouse' ,array('code'=>$arr['whscode']))) // check exist
                                    {
                                        $arr['sid'] = $row[0]['id'];
                                    }
                                    else{
                                        $arr['sid'] = 0;
                                    }

                                    $where = array( 'itemcode'=>$arr['itemcode'], 'whscode' =>$arr['whscode']);

                                    if($row['qty'] != $arr['qty'] || $row['onhand'] != $arr['onhand']){

                                        if($isInit == true && $rowCount == 0)
                                        {
                                            Uploads::insert('tb_shopstock', $arr);
                                        }
                                        else
                                        {
                                            //$arr['log'] = 0; // only update log if updated in mysql
                                            Uploads::updateInsert('tb_shopstock', $arr, $where);
                                        }
                                    }
                                }
                            }

                        }

                        else if($name[0] == 'Customer' ){

                            if($reader->localName == 'id') {
                                $reader->read();
                                $arr['id'] = $reader->value;
                            }

                            else if($reader->localName == '_id') {
                                $reader->read();
                                $arr['_id'] = $reader->value;
                            }

                            else if($reader->localName == 'GroupCode') {
                                $reader->read();
                                $arr['groupid'] = $reader->value;

                            }
                            else if($reader->localName == 'Plist') { // price list
                                $reader->read();
                                $arr['price'] = $reader->value;
                            }
                            else if($reader->localName == 'Phone1') {
                                $reader->read();
                                $arr['phone'] = $reader->value;
                            }
                            else if($reader->localName == 'CntctPrsn') {
                                $reader->read();
                                $arr['contact'] = $reader->value;
                            }
                            else if($reader->localName == 'Phone2') {  //Cellular
                                $reader->read();
                                $arr['contactphone'] = $reader->value;
                            }
                            else if($reader->localName == 'CreateDate') {
                                $reader->read();
                                $arr['updatetime'] = $reader->value;
                            }

                            else if($reader->localName == 'Building') {
                                $reader->read();
                                $arr['address'] = $reader->value;
                            }
                            else if($reader->localName == 'Name') {
                                $reader->read();
                                $arr['name'] = $reader->value;
                            }
                            else if($reader->localName == 'GroupNum') {
                                $reader->read();
                                $arr['groupnum'] = $reader->value;
                            }
                            else if($reader->localName == 'Pymntgroup') {
                                $reader->read();
                                $arr['pymntgroup'] = $reader->value;
                            }
                            else if($reader->localName == 'Code') {
                                $reader->read();
                                $arr['code'] = $reader->value;

                                $where = array( 'code'=>$arr['code']);


                                if($isInit == true && $rowCount == 0)
                                {
                                    Uploads::insert('tb_' . $name[0], $arr);
                                }
                                else
                                {
                                    // $arr['log'] = 0; //
                                    Uploads::updateInsert('tb_' . $name[0], $arr, $where);
                                }

                                $arr['contactphone'] = '';
                                $arr['contact'] = '';
                                // var_dump($arr);
                            }
                        }

                    }
                }

                $reader->close();
            }
        }
        else
        {
            return $this->sendError(array('status' => 0));
        }
    }
}
