<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Imei;
use App\Pricelistname;
use Validator;

class PricelistnameController extends BaseController
{
	//Price List Name GET Method
    public function pricelistnames_get(Request $request, $limit= NULL, $offset = NULL)
    {
    	 $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        $count = Pricelistname::all()->count();
        $max_log_id = Pricelistname::get('log')->max();

        if($count == null) $count = 0;
        if($max_log_id == null) $max_log_id = 0;

        $col = '*';
        $pricelistnames = Pricelistname::getAll($col,$limit, $offset);

        if($pricelistnames)
        {
            array_push($pricelistnames, array('max_log_id' => $max_log_id, 'count' => $count, 'status' => 1,
                'limit'=>$limit, 'offset'=>$offset));
            return $this->sendResponse($pricelistnames, 'Pricelistname Retrieved Successfully.');   //OK
        }
        else
        {
             return $this->sendResponse(array(array('max_log_id' => $max_log_id, 'count' => $count, 'status'=>0, 'error' => '0 row' )));
        }
    }

    //Price List Name POST or PUT Method

    public function pricelistnames_post(Request $request, $limit= NULL, $offset = NULL)
    {

        $pricelistnames = new Pricelistname();

        $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        if( $request->ListNum != false )
        {
            if (strpos($request->ListNum, '|') !== false)
            {
            	$ListNums= explode('|',$request->ListNum);
            	$ListNames = explode('|',$request->ListName);
   				$BaseNums = explode('|', $request->BaseNum);
                $PrimCurrs = explode('|',$request->PrimCurr);
                $Factors = explode('|',$request->Factor);
                $updatetimes = explode('|',$request->updatetime);
                $States = explode('|',$request->state);

   				for($i = 0; $i < count($ListNums); $i++)
                {
                    $datas[] = array(
                    'ListNum' => $ListNum[$i],
                    'ListName' => $ListName[$i],
                    'BaseNum' => $BaseNum[$i],
                    'Factor' => $Factor[$i],
                    'PrimCurr' => $PrimCurr[$i],
                    'state' => $State[$i],
                    'updatetime' => $updatetimes[$i],
                    'log' => 0
                    );

                }
            }

            else    // qty does not have | char
            {
                if($request->ListNum != '')
                {
                    $datas[] = array(
                        'ListNum' => $request->ListNum,
                        'ListName' => $request->ListName,
                        'BaseNum' => $request->BaseNum,
                        'Factor' => $request->Factor,
                        'PrimCurr' => $request->PrimCurr,
                        'tate' => $request->State,
                        'updatetime' => $request->updatetime,
                        'log' => 0
                    );
                }

            }

            foreach($datas as $data)
            {
                Pricelistname::updateInsert($data);
            }


            return $this->pricelistnames_get($request, $limit, $offset);
         }
         else
         {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);

         }
     }

}
