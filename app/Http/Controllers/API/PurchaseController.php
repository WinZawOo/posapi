<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Purchase;
use App\PurchaseDetail;
use App\Imei;
use App\Key;
use Validator;

class PurchaseController extends BaseController
{
    //Purchase Get Method
    public function purchases_get(Request $request, $limit= NULL, $offset = NULL)
    {

        $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        $count = Purchase::all()->count();
        $max_log_id = Purchase::get('log')->max();


        $col1 = [
        'id AS ida',
        '_id AS _id',
        'did AS did',
        'stockid AS sid',
        'userid AS uid',
        'qty AS qty',
        'cost AS cos',
        'state AS sta',
        'remark AS rem',
        'updatetime AS upd',
        'createtime AS ctt',
        'autoid AS aid',
        'log AS log',
        'version AS ver' ];

        $col2 = [
        'id AS id',
        '_id AS _i',
        'did AS di',
        'purchase_id',
        'stockid AS si',
        'qty AS qt',
        'cost AS co',
        'remark AS re',
        'updatetime AS up',
        'state AS st',
        'log AS lo',
        'autoid AS au',
        'version AS vers'];

        if($count == null) $count = 0;
        if($max_log_id == null) $max_log_id = 0;

        $purchases = Purchase::getAllDetail($col1, $col2, $limit, $offset);

        if($purchases)
        {
            array_push($purchases, array('max_log_id' => $max_log_id, 'count' => $count, 'status' => 1,
                'limit'=>$limit, 'offset'=>$offset));
            return $this->sendResponse($purchases, 'Purchase Retrieved Successfully.');   //OK
        }
        else
        {
             return $this->sendResponse(array(array('max_log_id' => $max_log_id, 'count' => $count, 'status'=>0, 'error' => '0 row' )));
        }
    }

    //Purchase POST or PUT Method

    public function purchases_post(Request $request, $limit= NULL, $offset = NULL)
    {
        $purchases = new Purchase();

        //$key = Key::where('key',$request->header('POS_API_KEY'))->first()->id;

        $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        //For Purchase

        if( $request->updatetime != false )
        {
            if (strpos($request->updatetime, '|') !== false)
            {
            	$ids = explode('|',$request->id);
                $_ids = explode('|',$request->_id);
                $dids = explode('|',$request->did);
                $stockids = explode('|',$request->sid);
  				$userids = explode('|',$request->uid);
                $qtys = explode('|',$request->qty);
                $costs = explode('|',$request->cos);
                $states = explode('|',$request->sta);
                $remarks = explode('|',$request->rem);
                $updatetimes = explode('|',$request->upd);

                for($i = 0; $i < count($updatetime); $i++)
                {
                        $datas[] = array(
                        'kid'       => $this->key,
                        'did'       => $dids[$i],
                        '_id'       => $_ids[$i],
                        'stockid'   => $shopids[$i],
                        'userid'    => $userids[$i],
                        'qty'       => $qtys[$i],
                        'cost'      => $costs[$i],
                        'state'     => $states[$i],
                        'remark'    => $remarks[$i],
                        'updatetime' => $updatetimes[$i],
                        // 'pcode'     => $pcodes[$i],
                        // 'goodscode'     => $goodscode[$i],
                        // 'costeng'       => $costeng[$i],
                        // 'costmn'        => $costmn[$i],
                        'log'    => 0,
                        'id'   =>  $ids[$i]
                    );

                }
            }

            else    // qty does not have | char
            {
                $datas[] = array(
                    'kid'       => $this->key,
                    'did'       => $request->did,
                    '_id'       => $request->_id,
                    'stockid'   => $request->sid,
                    'userid'    => $request->uid,
                    'qty'       => $request->qty,
                    'cost'      => $request->cos,
                    'state'     => $request->sta,
                    'remark'    => $request->rem,
                    'updatetime' => $request->upd,
                    'log'       => 0,
                    'id'  => $request->id,
                );

            }

            foreach($datas as $data)
            {
                Purchase::updateInsert($data);
            }



            //For PurchaseDetail

            if (strpos($request->updatetime, '|') !== false)
            {
                $ids = explode('|',$request->p_id);
                $_ids = explode('|',$request->_pid);
                $dids = explode('|',$request->p_did);
                $purchaseids = explode('|',$request->purchase_id);
                $stockids = explode('|',$request->p_stockid);
                $itemids = explode('|',$request->p_itemid);
                $qtys = explode('|',$request->p_qty);
                $costs = explode('|',$request->p_cost);
                $states = explode('|',$request->p_state);
                $remarks = explode('|',$request->p_remark);
                $updatetimes = explode('|',$request->p_updatetime);

                for($i = 0; $i < count($updatetime); $i++)
                {
                    $datas2[] = array(
                        'kid'       => $this->key,
                        'did'       => $dids[$i],
                        '_id'       => $_ids[$i],
                        'purchaseid'=> $purchaseids[$i],
                        'stockid'   => $stockids[$i],
                        'itemid'    => $itemids[$i],
                        'cost'      => $costs[$i],
                        'qty'       => $qtys[$i],
                        'remark'    => $remarks[$i],
                        'updatetime' => $updatetimes[$i],
                        'id'        => $ids[$i],
                        'state'     =>  $sts[$i],
                        'log'       => 0
                    );

                }
            }

            else    // qty does not have | char
            {
                $datas2[] = array(
                    'kid'       => $this->key,
                    'did'       => $request->p_did,
                    '_id'       => $request->_pid,
                    'purchaseid'=> $request->purchase_id,
                    'stockid'   => $request->p_stockid,
                    'itemid'    => $request->p_itemid,
                    'cost'      => $request->p_cost,
                    'qty'       => $request->p_qty,
                    'remark'    => $request->p_remark,
                    'updatetime' => $request->p_updatetime,
                    'id'        => $request->p_id,
                    'state'     => $request->p_state,
                    'log'       =>0,
                );

            }

            foreach($datas2 as $data)
            {
                PurchaseDetail::insertUpdate($data);
            }

            return $this->purchases_get($request, $limit, $offset);
         }
         else
         {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);

         }

    }
}
