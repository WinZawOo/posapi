<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Imei;
use App\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SupplierController extends BaseController
{
    //Suppliers GET
    public function suppliers_get(Request $request, $limit = null, $offset = null){

        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $count = Supplier::all()->count();
        $max_log_id = Supplier::max('log');
        if($count == null) $count = 0;
        if($max_log_id == null) $max_log_id = 0;

            $col = array(
                'id',
                '_id',
                'did AS di',
                'name AS na',
                'address AS ad',
                'email AS em',
                'phone AS ph',
                'contact AS ct',
                'cphone AS cp',
                'debt AS de',
                'remark AS re',
                'updatetime AS up',
                'encrypt AS en',
                'log AS log',
                'autoid AS autoid',
                'sync As sy',
                'state As st',
//                'bank As ba',
//                'account As ac',
//                'oper As op',
//                'operdate As opd'
        );
            $message = Supplier::GetData($col, $limit, $offset);
            if($message){
                array_push($message, array('max_log_id'=>$max_log_id, 'count'=>$count, 'status'=>1,'limit'=>$limit, 'offset'=>$offset));
                return $this->sendResponse($message);
            }else{
                return $this->sendError(array('max_log_id' => $max_log_id, 'count' => $count, 'status'=>0, 'error' => '0 row' ));
            }

    }

    //Suppliers POST
    public function suppliers_post(Request $request, $limit = null, $offset = null){

        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $datas = array();

        if( $request->up != null ) {
            if (strpos($request->up, '|') !== false) { // name has | char
                $idall = explode('|', $request->id, true);
                $_ids = explode('|', $request->_id, true);
                $names = explode('|', $request->na, true);
                $addresses = explode('|', $request->ad, true);
                $emails = explode('|', $request->em, true);
                $phones = explode('|', $request->ph, true);
                $contacts = explode('|', $request->ct, true);
                $cphones = explode('|', $request->cp, true);
                $debts = explode('|', $request->de, true);
                $remarks = explode('|', $request->re, true);
                $updatetimes = explode('|', $request->up, true);
                //$encrypts =     explode('|',$request->en, true);
                $dids = explode('|', $request->di, true);
//                $accounts = explode('|', $request->ac, true);
//                $banks = explode('|', $request->ba, true);
//                $opers = explode('|', $request->op, true);
//                $operdates = explode('|', $request->opd, true);
                $states = explode('|', $request->st, true);

                for ($i = 0; $i < count($updatetimes); $i++) {
                    $datas[] = array(
                        'kid' => $this->keyId,
                        'id' => $idall[$i],
                        '_id' => $_ids[$i],
                        'did' => $dids[$i],
                        'name' => $names[$i],
                        'address' => $addresses[$i],
                        'email' => $emails[$i],
                        'phone' => $phones[$i],
                        'contact' => $contacts[$i],
                        'cphone' => $cphones[$i],
                        'debt' => $debts[$i],
                        'remark' => $remarks[$i],
                        'updatetime' => $updatetimes[$i],
                        'encrypt' => $this->input->post('en') == "true" ? 1 : 0,
                        'log' => 0,
//                        'bank' => $banks[$i],
//                        'account' => $accounts[$i],
//                        'oper' => $opers[$i],
//                        'operdate' => $operdates[$i],
                        'state' => $states[$i]
                    );
                }
            } else    // name does not have | char
            {
                $datas[] = array(
                    'kid' => $this->keyId,
                    'id' => $request->id,
                    '_id' => $request->_id,
                    'did' => $request->di,
                    'name' => $request->na,
                    'address' => $request->ad,
                    'email' => $request->em,
                    'phone' => $request->ph,
                    'contact' => $request->ct,
                    'cphone' => $request->cp,
                    'debt' => $request->de,
                    'remark' => $request->re,
                    'updatetime' => $request->up,
                    'encrypt' => $request->en == "true" ? 1 : 0,
                    'log' => 0,
//                    'bank' => $request->ba,
//                    'account' => $request->ac,
//                    'oper' => $request->op,
//                    'operdate' => $request->opd,
                    'contact' => $request->ct,
                    'state' => $request->st
                );
            }
            foreach ($datas as $data){
                Supplier::updateOrCreate(['id'=>$data['id']],$data);
            }
        }
        return $this->suppliers_get($request, $limit, $offset);
    }
}
