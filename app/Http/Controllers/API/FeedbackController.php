<?php

namespace App\Http\Controllers\API;

use App\Feedback;
use App\Http\Controllers\Controller;
use App\Imei;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use App\Http\Resources\Feedback as FeedbackResource;

class FeedbackController extends BaseController
{
    //Feedbacks GET
    public function feedbacks_get($salt){
        if(Imei::where('s',$salt)->get()->count() < 1){
            return $this->sendResponse(FeedbackResource::collection(array('status' => 0, 'error' => 'No Rights!')));
        }

        if($message = Feedback::where('kid','=',$this->keyId)->get()->toArray()){
            array_push($message, array('status' => 1));
            return $this->sendResponse(FeedbackResource::collection($message));
        }else{
            return $this->sendError(array('status' => 0, 'error' => '0 row'));
        }
    }

    //Feedbacks POST
    public function feedbacks_post(Request $request){
        if(Imei::where([['s',$request->s],['id',$request->did]])->get()->count() < 0){
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $datas[] = array(
            //'tb' => 'tb_feedback', // for versions use
            'kid' => $this->keyId,
            'did' => $request->did,
            'username' => $request->username,
            'role' => $request->role,
            //'updatetime' => $request->updatetime,
            'feedback' => $request->feedback
        );

        $count = 0;
        if($request->did > 0)
        {
            foreach ($datas as $data) {
                if (Feedback::firstOrCreate($data))
                    $count++;
            }
        }

        if($count > 0){
            $message = array();
            array_push($message,array('status'=>1));
           return $this->sendResponse(FeedbackResource::collection($message));
        }else{
           return $this->sendError(array('status'=> 0, 'error'=>'No inserted'));
        }
    }
}
