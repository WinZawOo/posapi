<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Http\Request;
use App\Http\Resources\MainStock as MainStockResource;
use App\MainStock;
use App\Imei;
use Validator;

class MainStockController extends BaseController
{
	//Main Stock Get Method
    public function stocks_get(Request $request, $limit= NULL, $offset = NULL)
    {
        $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        $count = MainStock::all()->count();
        $max_log_id = MainStock::get('log')->max();

        if($count == null) $count = 0;
        if($max_log_id == null) $max_log_id = 0;

        $col = [
        'did AS di',
        'sid AS si',
        'iid AS ii',
        'qty AS qt',
        'alt AS al',
        'rmk AS re',
        'udt AS up' ];

        $stocks = MainStock::getAll($col,$limit,$offset);

        if($stocks)
        {
            array_push($stocks, array('max_log_id' => $max_log_id, 'count' => $count, 'status' => 1,
                'limit'=>$limit, 'offset'=>$offset));
            return $this->sendResponse($stocks, 'Main Stock Retrieved Successfully.');   //OK
        }
        else
        {
             return $this->sendResponse(array(array('max_log_id' => $max_log_id, 'count' => $count, 'status'=>0, 'error' => '0 row' )));
        }
    }


    //Main Stock PUT Method

    public function stocks_post(Request $request, $limit= NULL, $offset = NULL)
    {
        $stock = new MainStock();

        $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        if($file   =   $request->file('image')) {

            $img_name      =   time().time().'.'.$file->getClientOriginalExtension();

            $target_path    =   public_path('/uploads/');

            $file->move($target_path, $img_name);
            }

        if( $request->qty != null )
        {
            if (strpos($request->qty, '|') !== false)
            {
            	$ids = explode('|',$request->id);
                $dids = explode('|',$request->did);
                $sids = explode('|',$request->shopid);
                $iids = explode('|',$request->itemid);
                $qtys = explode('|',$request->qty);
                $alerts = explode('|',$request->alert);
                $remarks = explode('|',$request->remark);


                for($i = 0; $i < count($remarks); $i++)
                {
                	$datas[] = array(
                        'id' => $ids[$i],
                        'did' => $dids[$i],
                        'sid' => $sids[$i],
                        'iid' => $iids[$i],
                        'qty' => $qtys[$i],
                        'alt' => $alerts[$i],
                        'rmk' => $remarks[$i] );

                }
            }

            else    // qty does not have | char
            {
                $datas[] = array(
                    'id' => $request->id,
                    'did' => $request->did,
                    'sid' => $request->shopid,
                    'iid' => $request->itemid,
                    'qty' => $request->qty,
                    'alt' => $request->alert,
                    'rmk' =>$request->remark );


            }

            foreach($datas as $data)
            {
                MainStock::updateInsert($data);
            }

            return $this->stocks_get($request, $limit, $offset);
         }
         else
         {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);

         }
     }

}
