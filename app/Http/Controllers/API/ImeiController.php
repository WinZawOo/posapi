<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Imei;
use Illuminate\Http\Request;
use App\Http\Resources\Imei as ImeiResource;
use App\Http\Controllers\API\BaseController;
use Illuminate\Support\Str;

class ImeiController extends BaseController
{
    //Imei GET
    public function imeis_get($s, $simid){
        $where = array(
            'kid' => $this->keyId,
            'simid' => $simid
        );
       $message = Imei::GetData($where, $s)->toArray();
       if($message){
            array_push($message,array('status'=>1));
            return  $this->sendResponse(ImeiResource::collection($message));
       }else{
            return $this->sendError(array( 'status' => 0));
       }
    }

    //Imei POST
    public function imeis_post(Request $request){

        if($request->simid == ""){
            $message = array('status' => 0,'error'=>'No Right!');
            return $this->sendError($message);
        }

        if($request->setlogo == "1") {
           if(Imei::where([['s',$request->s],['id',$request->did]])->get()->count() > 0){
               if(Imei::where(['simid'=>$request->simid])->get()->count() > 0){
                   $request->validate([
                       'image' => 'image|mimes:jpeg,png,jpg,gif|max:1000000|string|min:10|max:40',
                   ]);

                   $filename = time().'.'.$request->image->extension();
                   if($request->image->move(public_path('images'),$filename)){
                       $message  = array('status' => '1');
                       return $this->sendResponse(ImeiResource::collection($message));
                   }else{
                       return $this->sendError(array("status"=> '0', "result"=>"failed"));
                   }
               }
           }
        }

    if(Imei::where('simid',$request->simid)->get()->count() > 0){
        if($request->setting == "1"){
            $data = array(
                'sn' => $request->sn,
                'simid' => $request->simid,
                'encrypt' => $request->encrypt == "true" ? 1 : 0,
                'printlogo' => $request->printlogo == "true" ? 1 : 0,
                'autosync' => $request->autosync == "true" ? 1 : 0,
                'autosyncbeforeadd' => $request->autosyncbefored == "true" ? 1 : 0,
                'slipitemeng' => $request->slipitemeng == "true" ? 1 : 0,
                'taxmin' => $request->taxmin == "true" ? 1 : 0,
                'tax' => $request->tax,
                'spec1' => $request->spec1,
                'spec2' => $request->spec2,
                'spec3' => $request->spec3,
                'sliplang' => $request->sliplang,
                'tlshowsize' => $request->tlshowsize,
                'tlheadersize' => $request->tlheadersize,
                'tlfontsize' => $request->tlfontsize,
                'autouploadsize' => $request->autouploadsize,
                'slipfooterspace' => $request->slipfooterspace,
                'payfontsize' => $request->payfontsize,
                'onfocus' => $request->onfocus,
                'dontsyncdata' => $request->dontsyncdata == "true" ? 1 : 0,
                'custcode' => $request->custcode,
                'opendrawer' => $request->opendrawer == "true" ? 1 : 0,
                'printunit' => $request->pintunit == "true" ? 1 : 0,
                'printinfo' => $request->printinfo == "true" ? 1 : 0,
                'editprice' => $request->editprice == "true" ? 1 : 0,
                'editinvoice' => $request->editinvoice == "true" ? 1 : 0,
                'printed' => $request->printed == "true" ? 1 : 0,
                'relogin' => $request->relogin == "true" ? 1 : 0,
                'autobgsync' => $request->autobgsync == "true" ? 1 : 0,
                'showsap' => $request->showsap == "true" ? 1 : 0,
                'showspec' => $request->showspec == "true" ? 1 : 0,
                'oversales' => $request->oversales == "true" ? 1 : 0,

                'slipwidth' => $request->slipwidth == "true" ? 1 : 0,
                'printernum' => $request->printernum == "true" ? 1 : 0,
                'autobgsynctime' => $request->autobgsynctime == "true" ? 1 : 0,
                'showcost' => $request->showcost == "true" ? 1 : 0,
                'custprefix' => $request->custprefix == "true" ? 1 : 0,
                'custstart' => $request->custstart == "true" ? 1 : 0,
                'invprefix' => $request->invprefix == "true" ? 1 : 0,
                'invstart' => $request->invstart == "true" ? 1 : 0,
                'invend' => $request->invend == "true" ? 1 : 0,
                'luckydraw' => $request->luckydraw == "true" ? 1 : 0,
            );

            if($request->version > Imei::where('simid', $data['simid'])->first()->version){
                if(Imei::UpdateData($data)){
                    $message = Imei::GetRowsByColumn(array('sn'=>$request->sn, 'simid'=>$request->simid));
                    return $this->sendResponse($message);
                }else{
                    return $this->sendError(array('status'=>0, 'error'=>'No Updated'));
                }
            }
        }
        $message = Imei::GetRowsByColumn(array('sn'=>$request->sn, 'simid'=>$request->simid));
        return $this->sendResponse($message);
    }else{
        if($request->updatetime != ""){
            $data = array(
                'kid' => $this->keyId,
                'key' => $this->key,
               // 'uid' => $this->user_id,
                'simid' => $request->simid,
                'imei' => $request->imei,
                'sn' => $request->sn,
                'updatetime' => $request->updatetime,
                'sync' => 1,
                's' => Str::random(16)
            );
            if(Imei::Create($data)){
                $message = Imei::GetRowsByColumn(array('sn'=>$request->sn, 'simid'=>$request->simid));
                return $this->sendResponse($message);
            }else{
                return $this->sendError(array('status'=>0, 'error'=>'No Inserted'));
            }
        }
    }

    }
}
