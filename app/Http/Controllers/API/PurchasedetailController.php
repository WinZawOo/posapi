<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\PurchaseDetail;
use App\Imei;
use App\Key;

class PurchasedetailController extends BaseController
{

	public function purchasedetails_get($salt)
	{
		$count = Imei::where('s',$salt)->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        $message = array('status' => 0, 'error' => '');
        return $this->sendError($message);
	}

    public function purchasedetails_post(Request $request)
    {
    	$count = Imei::where('s', $request->header('s'))->where('simid', $request->header('sim'))->get()->count();
    	 if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        if($request->count == '0')
        {
        	$message = PurchaseDetail::all()->toArray();

        	if(count($message) > 0)
        	{
        		$message = array($message, 'status' => 1);
        		return $this->sendResponse($message, 'PurchaseDetail Reterived Successfully!');
        	}
        	else
        		return $this->sendError(array('status' => 0));
        }
        elseif($request->action == 'all')
        {
        	$data = array(
                'year'=> $request->year,
                'month'=> $request->month,
                'day'=> $request->day);

        	if($message = PurchaseDetail::getPurchaseDetailsByYearMonth($request))
        	{
                $message = array($message, 'status' => 1);
                return $this->sendResponse($message, 'PurchaseDetail Reterived Successfully!');
        	}
            else
            {
                return $this->sendError(array('status' => 0));
            }

        }
        elseif($request->updatetime == '')
        {
            $message = PurchaseDetail::getIdGreaterThan($request);

            if(count($message) > 0)
            {
                $message = array($message, 'status' => 1);
                return $this->sendResponse($message, 'PurchaseDetail Reterived Successfully!');
            }
            else
                return $this->sendError(array('status' => 0));

        }
        else
        {
             if (strpos($request->updatetime, '|') !== false)
            {
                $ids = explode('|',$request->id, true);
                $_ids = explode('|',$request->_id, true);
                $dids = explode('|',$request->did, true);
                $purchaseids = explode('|',$request->purchase_id, true);
                $stockids = explode('|',$request->stockid, true);
                $itemids = explode('|',$request->itemid, true);
                $qtys = explode('|',$request->qty, true);
                $costs = explode('|',$request->cost, true);
                $remarks = explode('|',$request->remark, true);
                $updatetimes = explode('|',$request->updatetime, true);
                $versions = explode('|',$request->version, true);


                for($i = 0; $i < count($updatetime); $i++)
                {
                    $datas[] = array(
                        'tb' => 'tb_purchasedeatail',
                        'id' => $ids[$i],
                        '_id' => $_ids[$i],
                        'did' => $dids[$i],
                        'kid' => $this->key,
                        'purchase_id' => $purchaseids[$i],
                        'stockid' => $stockids[$i],
                        'itemid' => $itemids[$i],
                        'qty' => $qtys[$i],
                        'cost' =>$costs[$i],
                        'version' => $versions[$i],
                        'remark' => $remarks[$i],
                        'updatetime' => $updatetimes[$i],
                    );

                }
            }

            else    // qty does not have | char
            {

                $datas[] = array(
                    'tb' => 'tb_purchasedeatail',
                    'id' => $request->id,
                    '_id' => $request->_id,
                    'did' => $request->did,
                    'kid' => $this->key,
                    'purchase_id' => $request->purchase_id,
                    'stockid' => $request->stockid,
                    'itemid' => $request->itemid,
                    'qty' => $request->qty,
                    'cost' =>$request->cost,
                    'version' => $request->version,
                    'remark' => $request->remark,
                    'updatetime' => $request->updatetime,
                );

            }

            // print_r($datas);
            // die();

            $_ids = array();
            $id = 0;
            $idsall = array();
            $addId = 0; // if _id starts from 1, then check server's _id

            if($datas[0]['_id'] == 1)
            {
                $count = PurchaseDetail::where('did',$datas[0]['did'])->where('_id', $datas[0]['_id'])->
                        get()->count();

                if($count > 0 )
                {
                    $rows = PurchaseDetail::where('did','=' ,$datas[0]['did'])->get();
                    $addId = $rows[0]['_id'];

                }

            }

            foreach($datas as $data)
            {
                if($addId > 0)
                {
                    $data['_id'] += $addId;
                    $_ids[] = $data['_id'];
                }
                if($data['stockid'] != "")
                {
                    if($idsall[] = PurchaseDetail::create($data))
                        $id ++;
                }
            }

            if($id > 0)
            {
                if($addId > 0)
                {
                    $message =  PurchaseDetail::getWhereInIds($data['did'], $_ids);
                }

                else
                {

                    $message =  PurchaseDetail::getWhereInIdsAlls($idsall);

                    if($arrReturn = PurchaseDetail::getWhereBetween( $request->maxid, $idsall[0]))
                    {
                        foreach($arrReturn as $arr)
                            array($message, $arr);
                    }
                }
               return $this->sendResponse($message, array('status' => 1, 'message' => $id . ' Rows Added.'));
            }
            else
            {
                $message = array( 'status' => 0, 'error' => 'Not inserted!',
                    'count' => PurchaseDetail::where('did', $request->did)->get->count());
                return $this->sendError($message);

            }

        }

    }
}
