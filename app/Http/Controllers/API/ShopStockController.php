<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Imei;
use App\ShopStock;
use Illuminate\Http\Request;

class ShopStockController extends BaseController
{
    //ShopStocks GET
    public function shopstocks_get(Request $request, $limit = null, $offset = null){

        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $count = ShopStock::all()->count();
        $max_log_id = ShopStock::max('log');

        if($count == null) $count = 0;
        if($max_log_id == null) $max_log_id = 0;

        $col = array(
            'id as id',
            'did AS di',
            'sid AS si',
            'iid AS ii',
            'tti AS tt',
            'qty AS qt',
            'alt AS al',
            'rmk AS re',
            'udt AS up',
            'onhand AS oh',
            'log AS log'
        );

        $message = ShopStock::GetData($col, $limit, $offset)->toArray();
        if($message){
            array_push($message, array('max_log_id'=>$max_log_id, 'count'=>$count, 'status'=>1,'limit'=>$limit, 'offset'=>$offset));
            return $this->sendResponse($message);
        }else{
            return $this->sendError(array('max_log_id' => $max_log_id, 'count' => $count, 'status'=>0, 'error' => '0 row' ));
        }
    }

    //Shopstocks POST
    public function shopstocks_post(Request $request, $limit = null, $offset = null){

        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        return $this->shopstocks_get($request, $limit, $offset);
    }
}
