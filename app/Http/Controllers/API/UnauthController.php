<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Unauth;

class UnauthController extends BaseController
{
    public function unauths_post(Request $request)
    {
    	$message = Unauth::getCountByColumn(array(['imei', '=', $request->imei]));

    	if(count($message) > 0)
    	{
    		$result = Unauth::getRowsByColumns(array(
    			['imei', '=', $request->imei],
    			['simid', '=', $request->simid],
    		));
    		return $this->sendResponse($result, 'Imei data Retrieve Successfully!');
    	}
    	else
    	{	// not found
			$data = array(
				'kid' => $this->key,
				'key' => $request->header('POS_API_KEY'),
				'uid' => $this->user_id,
				'simid' => $request->simid,
				'imei' => $request->imei,
				'phone' => $request->phone,
				'name' => $request->name,
				'sn' => $request->sn,
				'updatetime' =>$request->updatetime,
				'sync' => 1,
				's'     => $request->header('s'),
			);

			$id = Unauth::insert($data);

			if($id)
			{	
				// insert and return id
				$message[] = array('status' => 1);
				return $this->sendResponse($message, 'Imei Insert Successfully!!'); // CREATED (201) being the HTTP response code
			}
			else{	// not insert
				$message[] = array('status' => 0);
				return $this->sendError($message); 
			}
			
		}
    }
}
