<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Version;
use App\Imei;

class VersionController extends BaseController
{
    //Version GET Method
    public function versions_get($salt = NULL)
    {
    	$count = Imei::where('s',$salt)->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        $message = Version::getAll();

        if($message)
        {           
            return $this->sendResponse($message, 'Version Retrieved Successfully.');   //OK
        }
        else
        {
             return $this->sendError(array('status' =>0, 'msg' => '0 row'));
        }

    }
}
