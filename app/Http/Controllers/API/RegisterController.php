<?php

namespace App\Http\Controllers\API;

use App\Imei;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\BaseController;

class RegisterController extends BaseController
{
    //User register POST
    public function register(Request $request)
    {

        if (Imei::where([['s', $request->s], ['id', $request->did]])->count() < 1) {

            $message = array(
                'status' => 0,
                'error' => 'No Rights',
                's' => $request->s,
                'id' => $request->did,
                'count' => Imei::where('s', $request->s)->count(),
            );

            return $this->sendError($message);
        }

        $message = array();
        $ids = array();
        $datas = array();

        $count = 0;
        $count1 = 0;
        $count2 = 0;

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        if ($request->syncuser == '1') {
            $message = User::GetRowsByColumns();
            array_push($message, array('status' => 1, 'msg' => 'syncuser'));
            return $this->sendResponse($message);
        }


        if ($request->newname)
            $username = $request->newname;
        else
            $username = $request->username;

        $data = array(
            //'tb' => 'tb_user', // for versions use
            'kid' => $this->keyId,
            'id' => $request->id,
            '_id' => $request->_id,
            'did' => $request->did,
            'name' => $request->name,
            'password' => $request->password,
            'role' => $request->role,
            'updatetime' => $request->updatetime,
            'version' => $request->version,
            'sync' => 1,
            'branch' => $request->branch,
            'dept' => $request->dept,
            'rights' => $request->rights,
            'username' => $username,
            'code' => $request->code,
            'shipping' => $request->shipping,
            'dist' => $request->dist,
            'printing' => $request->printing,
            'salestype' => $request->salestype,
            'telephone' => $request->telephone,
            'mobile' => $request->mobile,
            'fax' => $request->fax,
            'email' => $request->email,
            'state' => $request->state,
            'log' => $request->log
        );

        $successToken = array();
        if ($data['_id'] != "") {
            if (User::where('_id', $data['_id'])->count() > 0) {
                $data['id'] = User::where('_id', $data['_id'])->value('id');
                $count = User::updateData($data);
            } else {

                $success = User::insertData($data);
                $count = (int)$success['id'];
                $successToken['token'] = $success['token'];
            }
            if ($count > 0) {
                $message = $data;
            }
        }

        $datas = array();
        if ($request->check != "") {
            if (strpos($request->check, '|') !== false) { // name has | char

                $checks = explode('|', $request->check);

                for ($i = 0; $i < count($checks); $i++) {
                    $tmp = explode('-', $checks[$i]);
                    $datas[] = array(
                        'id' => $tmp[0],
                        'version' => $tmp[1]
                    );
                }
            } else    // name does not have | char
            {
                $tmp = explode('-', $request->check);
                $datas[] = array(
                    'id' => $tmp[0],
                    'version' => $tmp[1]
                );
            }
            foreach ($datas as $data) {
                if ($row = User::GetRowsByColumns(array(['id','=',$data['id']],['version','>', $data['version']]), 1)) {
                    $count1++;
                    $message = $row;
                }
            }

            if($request->count < User::all()->count()){
                foreach ($datas as $data){
                    $ids[] = $data['id'];
                }
                if($rows = User::whereNotIn('id',$ids)->get()){
                    foreach ($rows as $row){
                        $count2 ++;
                        array_push($message, $row);
                    }
                }
            }
        }


        if($count > 0 OR $count1 > 0 OR $count2 > 0){
            array_push($message, $successToken, array('status'=>1, 'c'=>$count, 'c1'=>$count1,'c2'=>$count2) );
            return $this->sendResponse($message);
        }elseif ($request->check == NULL){
            $message = User::where('id',$this->keyId)->get()->toArray();
            array_push($message,array('status'=>1));
            return $this->sendResponse($message);
        }else{
            $message[] = array( 'status' => 0, 'error' => 'Not found!' );
            return $this->sendError($message);
        }

    }

    //User Login POST
    public function login(Request $request, $salt){

        if(Imei::where('s',$salt)->count() < 1){
            $message = array('status'=>0, 'error'=>'No Rights!','count'=>Imei::where('s',$salt)->get());
            return $this->sendResponse($message);
        }

        if(Auth::attempt(['email'=> $request->email, 'password'=>$request->password]) && $message = User::where('kid',$this->keyId)->get()->toArray() ){
            $user = Auth::user();
            $success['token'] = $user->createToken('MyApp')->accessToken;
            array_push($message,$success);
            return $this->sendResponse($message, 'User login successfully.');
        }else{
            return $this->sendError('Unauthorised.',['error'=>'Unauthorised']);
        }
    }
}
