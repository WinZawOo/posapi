<?php

namespace App\Http\Controllers\API;

use App\Imei;
use App\IncomeExpense;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use App\Http\Resources\IncomeExpense as IncomeExpenseResource;

class IncomeExpenseController extends BaseController
{
    //IncomeExpenses GET
    public function incomeexpenses_get(Request $request, $limit = null, $offset = null){

        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $count = IncomeExpense::all()->count();
        $max_log_id = IncomeExpense::max('log');

        if($count == null) $count = 0;
        if($max_log_id == null) $max_log_id = 0;

        $col = array(
            'id',
            '_id',
            'did',
            'shopid',
            'userid',
            'title',
            'inout',
            'amount',
            'remark',
            'updatetime',
            'sync',
            'state',
            'log',
            'autoid'
        );
        $message = IncomeExpense::GetData($col, $limit, $offset)->toArray();
        if ($message) {
            array_push($message, array('max_log_id' => $max_log_id, 'count' => $count, 'status' => 1, 'limit' => $limit, 'offset' => $offset));
            return $this->sendResponse(IncomeExpenseResource::collection($message));
        } else {
            return $this->sendError(array('max_log_id' => $max_log_id, 'count' => $count, 'status' => 0, 'error' => '0 row'));
        }
    }

    //IncomeExpenses POST
    public function incomeexpenses_post(Request $request, $limit = null, $offset = null){

        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $datas = array();
        if( $request->updatetime != null ){
            if (strpos($request->updatetime, '|') !== false) { // name has | char

                $ids =             explode('|',$request->id, true);
                $_ids =             explode('|',$request->_id, true);
                $shopids = 		    explode('|',$request->shopid, true);
                $userids = 		    explode('|',$request->userid, true);
                $titles = 			explode('|',$request->title, true);
                $inouts = 			explode('|',$request->inout, true);
                $amounts = 			explode('|',$request->amount, true);
                $remarks = 			explode('|',$request->remark, true);
                $updatetimes = 		explode('|',$request->updatetime, true);
                $states =            explode('|',$request->state, true);

                for($i = 0; $i < count($updatetimes); $i++)
                {
                    $datas[] = array(
                        //'tb' => 'tb_incomeexpense', // for versions use
                        'kid' => $this->rest->id,
                        'did' => $this->input->post('did', true),
                        'id' => $ids[$i],
                        '_id' => $_ids[$i],
                        'shopid' => $shopids[$i],
                        'userid' => $userids[$i],
                        'title' => $titles[$i],
                        'inout' => $inouts[$i],
                        'amount' => $amounts[$i],
                        'remark' => $remarks[$i],
                        'updatetime' => $updatetimes[$i],
                        'sync' => 1,
                        'state' => $states[$i]
                    );
                }
            }
            else	// name does not have | char
            {
                $datas[] = array(
                    //'tb' => 'tb_incomeexpense', // for versions use
                    'kid' => $this->keyId,
                    'did' => $request->did,
                    'id' => $request->id,
                    '_id' => $request->_id,
                    'shopid' => $request->shopid,
                    'userid' => $request->userid,
                    'title' => $request->title,
                    'inout' => $request->inout,
                    'amount' => $request->amount,
                    'remark' => $request->remark,
                    'updatetime' => $request->updatetime,
                    'sync' => 1,
                    'state' => $request->state
                );
                foreach ($datas as $data) {
                    IncomeExpense::updateOrCreate(['id' => $data['id']], $data);
                }
            }
        }
        return $this->incomeexpenses_get($request, $limit, $offset);
    }
}
