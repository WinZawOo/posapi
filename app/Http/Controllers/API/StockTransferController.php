<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Imei;
use App\StockTransfer;
use Illuminate\Http\Request;

class StockTransferController extends BaseController
{
    //Stocktransfers GET
    public function stocktransfers_get(Request $request, $limit = null, $offset = null)
    {
        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $count = StockTransfer::all()->count();
        $max_log_id = StockTransfer::max('log');
        if($count == null) $count = 0;
        if($max_log_id == null) $max_log_id = 0;

        $col = array(
            'id AS id',
            '_id AS _id',
            'did AS did',
            'fromid AS fid',
            'toid AS tid',
            'itemid AS iid',
            'qty AS qty',
            'remark AS rem',
            'updatetime AS upd',
            'state AS sta',
            'log AS log',
            'autoid AS autoid'
        );

        $message = StockTransfer::GetData($col, $limit, $offset);
        if($message){
            array_push($message, array('max_log_id'=>$max_log_id, 'count'=>$count, 'status'=>1,'limit'=>$limit, 'offset'=>$offset));
            return $this->sendResponse($message);
        }else{
            return $this->sendError(array('max_log_id' => $max_log_id, 'count' => $count, 'status'=>0, 'error' => '0 row' ));
        }

    }

    //Stocktransfers POST
    public function stocktransfers_post(Request $request, $limit = null, $offset = null){

        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $datas = array();

        if( $request->upd != null ){
            if (strpos($request->upd, '|') !== false) { // name has | char
                $_ids = 			explode('|',$request->_id);
                $fromshopids = 		explode('|',$request->fid, true);
                $toshopids = 		explode('|',$request->tid, true);
                $itemids = 			explode('|',$request->iid, true);
                $qtys = 			explode('|',$request->qty, true);
                $remarks = 			explode('|',$request->rem, true);
                $updatetimes = 		explode('|',$request->upd, true);
                $dids = 	   		explode('|',$request->did, true);
                $ids = 	   			explode('|',$request->id, true);

                for($i = 0; $i < count($updatetimes); $i++)
                {
                    $datas[] = array(
                        'kid' 	=> $this->rest->id,
                        'did' 	=> $dids[$i],
                        '_id' 	=> $_ids[$i],
                        'fromid'=> $fromshopids[$i],
                        'toid' 	=> $toshopids[$i],
                        'itemid'=> $itemids[$i],
                        'qty' 	=> $qtys[$i],
                        'remark'=> $remarks[$i],
                        'updatetime' => $updatetimes[$i],
                        'id' 	=> $ids[$i],
                        'log' 	=> 0
                    );
                }
            }
            else	// name does not have | char
            {
                $datas[] = array(
                    'kid' 	=> $this->keyId,
                    'did' 	=> $request->did,
                    '_id' 	=> $request->_id,
                    'fromid'=> $request->fid,
                    'toid' 	=> $request->tid,
                    'itemid'=> $request->iid,
                    'qty' 	=> $request->qty,
                    'remark'=> $request->rem,
                    'updatetime' => $request->upd,
                    'id' 	=> $request->id,
                    'log' 	=> 0
                );
            }
            //var_dump($datas);
            foreach ($datas as $data){
                StockTransfer::insertUpdate($data, ['id'=>$data['id']]);
            }
        }
        return $this->stocktransfers_get($request,$limit,$offset);
    }
}
