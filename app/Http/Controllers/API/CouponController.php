<?php

namespace App\Http\Controllers\API;

use App\Coupon;
use App\Http\Controllers\API\BaseController;
use App\Imei;
use Illuminate\Http\Request;
use App\Http\Resources\Coupon as CouponResource;

class CouponController extends BaseController
{

    //Coupon GET
    public  function coupons_get(Request $request, $limit = null, $offset = null){

        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $count = Coupon::all()->count();
        $max_log_id = Coupon::max('log');
        if($count == null) $count = 0;
        if($max_log_id == null) $max_log_id = 0;

        $col = array(
            'id',
            '_id AS _i',
            'did AS di',
            'event AS ev',
            'code AS co',
            'discount AS di',
            'price AS pr',
            'updatetime AS up',
            'createtime AS cr',
            'used AS us',
            'state AS st',
            'log AS log',
            'autoid AS autoid'
        );
        $message = Coupon::GetData($col, $limit, $offset);
        if ($message) {
            array_push($message, array('max_log_id' => $max_log_id, 'count' => $count, 'status' => 1, 'limit' => $limit, 'offset' => $offset));
            return $this->sendResponse(CouponResource::collection($message));
        } else {
            return $this->sendError(array('max_log_id' => $max_log_id, 'count' => $count, 'status' => 0, 'error' => '0 row'));
        }
    }

    //Coupon POST
    public function  coupons_post(Request $request, $limit = null, $offset = null){

        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $datas = array();

        if($request->up != null){
            if(strpos($request->up, '|') !== false){
                $ids = explode('|', $request->id);
                $_ids = explode('|', $request->_i);
                $dids = explode('|',$request->di);
                $events = explode('|',$request->ev);
                $codes = explode('|',$request->co);
                $discounts = explode('|', $request->dis);
                $prices = explode('|', $request->pr);
                $createtimes = explode('|',$request->cr);
                $updatetimes = explode('|', $request->up);
                $useds = explode('|', $request->us);
                $states = explode('|', $request->st);
                for ($i = 0; $i < count($updatetimes); $i++){
                    $datas[] = array(
                        'id' => $ids[$i],
                        '_id' => $_ids[$i],
                        'did' => $dids[$i],
                        'event' =>$events[$i],
                        'code' => $codes[$i],
                        'discount' => $discounts[$i],
                        'price' => $prices[$i],
                        'createtime' => $createtimes[$i],
                        'updatetime' => $updatetimes[$i],
                        'used' => $useds[$i],
                        'log' => 0,
                        'state' => $states[$i]
                    );
                }
            }else{
                $datas[] = array(
                    'id' => $request->id,
                    '_id' => $request->_i,
                    'did' => $request->di,
                    'event' => $request->ev,
                    'code' => $request->co,
                    'discount' => $request->dis,
                    'price' => $request->pr,
                    'createtime' => $request->cr,
                    'updatetime' => $request->up,
                    'used' => $request->us,
                    'log' => 0,
                    'state' => $request->st
                );
            }
            foreach ($datas as $data) {
                Coupon::updateOrCreate(['id' => $data['id']], $data);
            }
        }
        return $this->coupons_get($request, $limit, $offset);
    }
}
