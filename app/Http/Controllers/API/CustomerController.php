<?php

namespace App\Http\Controllers\API;

use App\Customer;
use App\CustomerGroup;
use App\Http\Controllers\Controller;
use App\Http\Resources\Customer as CustomerResource;
use App\Http\Resources\CustomerGroup as CustomerGroupResource;
use App\Imei;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;

class CustomerController extends BaseController
{
    //Toupdates GET
    public function  toupdates_get(Request $request){
        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $message = Customer::all()->toArray();
        return ($message) ? $this->sendResponse(CustomerResource::collection($message)) : $this->sendError(array('status'=>0, 'msg' => '0 row' ));
    }

    //ConfirmUpdates GET
    public function  confirmupdates_get(Request $request,$confirms){

        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $confirms = urldecode($confirms);
        if(strpos($confirms, '|') !== false) {
            for ($i = 0; $i < count($confirms); $i++) {
                $data['id'] = $confirms[$i];
                $data['toupdate'] = 0;
                $data['log'] = 0;
              $customer = Customer::find($data['id']);
              $customer->fill($data);
            }
        }else{
            $data['toupdate'] = 0;
            $data['log'] = 0;
            $data['id'] = $confirms;
            $customer = Customer::find($data['id']);
            $customer->fill($data);
        }
       return $this->sendResponse(array('status'=>1, 'msg' => 'done' ));
    }

    //Customers GET
    public function  customers_get(Request $request, $limit = null, $offset = null){

        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $count = Customer::all()->count();
        $max_log_id = Customer::max('log');

        if($count == null) $count = 0;
        if($max_log_id == null ) $max_log_id = 0;

        $col = array(
            'id','_id',
            'did AS di',
            'code AS co',
            'name AS na',
            'division AS dv',
            'township AS ts',
            'address AS ad',
            'email AS em',
            'rank AS ra',
            'discount AS dc',
            'phone AS ph',
            'owe AS ow',
            'remark AS re',
            'updatetime AS up',
            'encrypt AS en',
            'balance AS ba',
            'price AS pr',
            'groupnum AS gn',
            'pymntgroup AS pt',
            'groupid AS gi',
            'state AS st',
            'toupdate AS tu',
            'log AS log',
            'autoid AS autoid',
            'contact AS ct',
            'contactphone AS cp',
            'pricelist AS pl' );

        $message = Customer::GetData($col,$limit,$offset)->toArray();
        if ($message) {
            array_push($message, array('max_log_id' => $max_log_id, 'count' => $count, 'status' => 1, 'limit' => $limit, 'offset' => $offset));
            return $this->sendResponse(CustomerResource::collection($message));
        } else {
            return $this->sendError(array('max_log_id' => $max_log_id, 'count' => $count, 'status' => 0, 'error' => '0 row'));
        }
    }

    //Customers POST
    public function customers_post(Request $request, $limit = null, $offset = null){

        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $datas = array();
        if( $request->up != null ) {
            if (strpos($request->up, '|') !== false) { // name has | char
                $idall = explode('|', $request->id);
                $_ids = explode('|', $request->_id);
                $codes = explode('|', $request->co);
                $names = explode('|', $request->na);
                $addresses = explode('|', $request->ad);
                $divisions = explode('|', $request->dv);
                $townships = explode('|', $request->ts);
                $emails = explode('|', $request->em);
                $ranks = explode('|', $request->ra);
                $discounts = explode('|', $request->dc);
                $phones = explode('|', $request->ph);
                $owes = explode('|', $request->ow);
                $remarks = explode('|', $request->re);
                $updatetimes = explode('|', $request->up);
                $dids = explode('|', $request->di);
                //$encrypts =     explode('|',$reencrypt);
                $prices = explode('|', $request->pr);
                $toupdates = explode('|', $request->tu);

                for ($i = 0; $i < count($updatetimes); $i++) {

                    if (count($toupdates) < 1)
                        $toupdates[$i] = 0;

                    $datas[] = array(
                        'kid' => $this->rest->id,
                        'id' => $idall[$i],
                        '_id' => $_ids[$i],
                        'did' => $dids[$i],
                        'code' => $codes[$i],
                        'name' => $names[$i],
                        'address' => $addresses[$i],
                        'division' => $divisions[$i],
                        'township' => $townships[$i],
                        'email' => $emails[$i],
                        'rank' => $ranks[$i],
                        'discount' => $discounts[$i],
                        'phone' => $phones[$i],
                        'owe' => $owes[$i],
                        'remark' => $remarks[$i],
                        'updatetime' => $updatetimes[$i],
                        'encrypt' => $this->input->post('en') == "true" ? 1 : 0,
                        'price' => $prices[$i],
                        'log' => 0,
                        'toupdate' => $toupdates[$i]
                    );
                }
            } else    // name does not have | char
            {
                $datas[] = array(
                    'kid' => $this->keyId,
                    'id' => $request->id,
                    '_id' => $request->_id,
                    'did' => $request->di,
                    'code' => $request->co,
                    'name' => $request->na,
                    'address' => $request->ad,
                    'division' => $request->dv,
                    'township' => $request->ts,
                    'email' => $request->em,
                    'rank' => $request->ra,
                    'discount' => $request->dc,
                    'phone' => $request->ph,
                    'owe' => $request->ow,
                    'remark' => $request->re,
                    'updatetime' => $request->up,
                    'encrypt' => $request->en == "true" ? 1 : 0,
                    'price' => $request->pr,
                    'log' => 0,
                    'toupdate' => $request->tu == null ? 0 : $request->tu,
                );
            }
            foreach ($datas as $data) {
                Customer::updateOrCreate(['id' => $data['id']], $data);
            }
        }
        return $this->customers_get($request, $limit, $offset);
    }

    //Groups GET
    public function groups_get(Request $request, $limit = null, $offset = null){

        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $count = CustomerGroup::all()->count();
        $max_log_id = CustomerGroup::max('log');
        if($count == null) $count = 0;
        if($max_log_id == null) $max_log_id = 0;

        $message = CustomerGroup::GetData($limit, $offset)->toArray();
        if ($message) {
            array_push($message, array('max_log_id' => $max_log_id, 'count' => $count, 'status' => 1, 'limit' => $limit, 'offset' => $offset));
            return $this->sendResponse(CustomerGroupResource::collection($message));
        } else {
            return $this->sendError(array('max_log_id' => $max_log_id, 'count' => $count, 'status' => 0, 'error' => '0 row'));
        }
    }

    //Groups POST
    public function groups_post(Request $request, $limit = null, $offset = null){

        if ( Imei::where([['s', $request->header('s')], ['simid', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $message = array();
        $datas = array();

        if (strpos($request->updatetime, '|') !== false) { // name has | char
            $ids         = 		explode('|',$request->id);
            // $dids     = 		explode('|',$request->did);
            $codes       = 		explode('|',$request->code);
            $names       =        explode('|',$request->name);
            //$groups    =        explode('|',$request->group);
            $updatetimes = 	explode('|',$request->updatetime);
            $version     = 	    explode('|',$request->state);

            for($i = 0; $i < count($updatetimes); $i++)
            {
                $datas[] = array(
                    'id' => $ids[$i],
                    // 'did' => $dids[$i],
                    'code' => $codes[$i],
                    'name' => $names[$i],
                    //'groups' => $groups[$i],
                    'updatetime' => $updatetimes[$i],
                    'state' => $version[$i],
                    'log' => 0
                );
            }
        }
        else	// name does not have | char
        {
            if($request->id != ''){
                $datas[] = array(
                    'id' => $request->id,
                    // 'did' => $request->did,
                    'code' => $request->code,
                    'name' => $request->name,
                    //'groups' => $request->group,
                    'updatetime' => $request->updatetime,
                    'state' => $request->state,
                    'log' => 0
                );
            }
        }
        foreach ($datas as $data) {
            CustomerGroup::updateOrcreate(['id' => $data['id']], $data);
        }
        return $this->groups_get($request,$limit, $offset);
    }

}
