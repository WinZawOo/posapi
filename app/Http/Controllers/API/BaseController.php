<?php


namespace App\Http\Controllers\API;


use App\apiKey;
use App\Imei;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;


class BaseController extends Controller
{

    public $keyId;
    public $key;
    public $user_id;

    public function __construct(Request $request)
    {
       $keys =  apiKey::where('key','=',$request->header('POS_API_KEY'))->get();
       foreach ($keys as $key){
           $this->keyId = $key->id;
           $this->key = $key->key;
           $this->user_id = $key->user_id;
       }
    }


    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($result)
    {
        $response = [
            'success' => true,
            'data'    => $result,
        ];
        return response()->json($response, 200);
    }


    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $error,
        ];

        if (!empty($errorMessages)) {
            $response['data'] = $errorMessages;
        }

        return response()->json($response, $code);
    }

}
