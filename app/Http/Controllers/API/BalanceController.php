<?php

namespace App\Http\Controllers\API;

use App\Imei;
use App\Balance;
use App\Keys;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\BaseController;
use App\Http\Resources\Balance as BalanceResource;

class BalanceController extends BaseController
{
    //Balance GET
    public function balance_get(Request $request, $limit = null, $offset = null) {

        if ( Imei::where([['s', '=', $request->header('s')], ['simid', '=', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

       $count = Balance::all()->count();
       $max_log_id = Balance::max('log');
       if($count == null) $count = 0;
       if($max_log_id == null) $max_log_id = 0;

       $col = array(
        'id',
        '_id',
        'did',
        'sid',
        'uid',
        'tid',
        'fid',
        'io',
        'amt',
        'bal',
        'rmk',
        'udt',
        'syc',
        'ste',
        'log',
        'autoid');

       $message = Balance::GetData($col,$limit,$offset);
       if($message){
            array_push($message,array('max_log_id'=> $max_log_id, 'count'=>$count, 'status' => 1));
         return   $this->sendResponse(BalanceResource::collection($message));
       }else{
         return   $this->sendError(array('max_log_id'=>$max_log_id, 'count'=>$count, 'status' => 0, 'error'=> '0 row'));
       }

    }

    //Balance POST
    public function balance_post(Request $request, $limit = NULL, $offset = NULL){

        if ( Imei::where([['s', '=', $request->header('s')], ['simid', '=', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

       $datas = array();

       if($request->udt != null){
            if(strpos($request->udt, "|") !==false){
                    $ids  =   explode('|',$request->id);
                    $_ids =  explode('|',$request->_id);     // _id
                    $dids =  explode('|',$request->did);     // did
                    $sids =  explode('|',$request->sid);        //  code
                    $uids =  explode('|',$request->uid);        //  code
                    $tids =  explode('|',$request->tid);        //  code
                    $fids =  explode('|',$request->fid);       //  code
                    $ios  =   explode('|',$request->io);       // discount
                    $amts =  explode('|',$request->amt);     // pri
                    $bals =  explode('|',$request->bal);      // cre
                    $rmks =  explode('|',$request->rmk);       // upd
                    $udts =  explode('|',$request->udt);       // upd
                    $stes =  explode('|',$request->ste);


                    for($i = 0; $i < count($udts); $i++)
                    {
                        $datas[] = array(
                            //'tb' => 'tb_package', // for versions use
                            'id'  => $ids[$i],
                            '_id' => $_ids[$i],
                            'did' => $dids[$i],
                            'sid' => $sids[$i],
                            'uid' => $uids[$i],
                            'tid' => $tids[$i],
                            'fid' => $fids[$i],
                            'io'  => $ios[$i],
                            'amt' => $amts[$i],
                            'bal' => $bals[$i],
                            'rmk' => $rmks[$i],
                            'udt' => $udts[$i],
                            'log' => 0,
                            'ste' => $stes[$i],
                        );
                    }

            }else{
                 $datas[] = array(
                        'id'  => $request->id,
                        '_id' => $request->_id,
                        'did' => $request->did,
                        'sid' => $request->sid,
                        'uid' => $request->uid,
                        'tid' => $request->tid,
                        'fid' => $request->fid,
                        'io'  => $request->io,
                        'amt' => $request->amt,
                        'bal' => $request->bal,
                        'rmk' => $request->rmk,
                        'udt' => $request->udt,
                        'log' => 0,
                        'ste' => $request->ste,
                    );
            }
             foreach ($datas as $data) {
                Balance::updateOrCreate(['id',$request->id], $data);
            }
       }
       return $this->balance_get($request, $limit, $offset);
    }



}

