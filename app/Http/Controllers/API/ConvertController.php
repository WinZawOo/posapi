<?php

namespace App\Http\Controllers\API;

use App\ConvertDetail;
use App\Imei;
use App\Convert;
use App\Invoice;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Resources\Convert as ConvertResource;
use MongoDB\Driver\BulkWrite;

class ConvertController extends BaseController
{
    //Convert GET
    public function converts_get(Request $request, $limit = null, $offset = null){

        if ( Imei::where([['s', '=', $request->header('s')], ['simid', '=', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

        $count = Convert::all()->count();
        $max_log_id = Convert::max('log');

        if($count == null) $count = 0;
        if($max_log_id == null) $max_log_id = 0;

              $col1 = array(
            'id         AS id',
            '_id        AS _id',
            'did        AS did',
            'fromid     AS fid',
            'toid       AS tid',
            'itemid     AS iid',
            'qty        AS qty',
            'remark     AS rem',
            'updatetime AS upd',
            'state   AS sta',
            'log        AS  log',
            'autoid     AS autoid'
        );

        $col2  = array(
            'id         AS ddid',
            '_id        AS d_id',
            'did        AS dddid',
            'fromid     AS dfid',
            'toid       AS dtid',
            'itemidto   AS diid',
            'qtyto      AS dqty',
            'remark     AS drem',
            'updatetime AS dupd',
            'state      AS dsta',
            'cid',
            'log        AS dlog',
            'autoid     AS dautoid'
        );


        $message = Convert::GetData($col1, $col2, $limit, $offset)->toArray();

        if ($message) {
        array_push($message, array('max_log_id' => $max_log_id, 'count' => $count, 'status' => 1, 'limit' => $limit, 'offset' => $offset));
            return $this->sendResponse(ConvertResource::collection($message));
        } else {
            return $this->sendError(array('max_log_id' => $max_log_id, 'count' => $count, 'status' => 0, 'error' => '0 row'));
        }

}

    //Convert POST
    public  function  converts_post(Request $request, $limit = null, $offset = null){

        if ( Imei::where([['s', '=', $request->header('s')], ['simid', '=', $request->header('sim')]])->get()->count() < 1) {
            $message = array('status' => 0, 'error' => 'No Rights!');
            return  $this->sendError($message);
        }

            $datas = array();
            if($request->upd  != null){
                if(strpos($request->upd, '|') !== false){
                    $_ids = explode('|', $request->_id, true);
                    $fromshopids = explode('|', $request->fid);
                    $toshopids = explode('|', $request->tid, true);
                    $itemids = explode('|', $request->iid, true);
                    $itemidtos = explode('|', $request->iidt, true);
                    $qtys = explode('|', $request->qty, true);
                    $remarks = explode('|', $request->rem, true);
                    $updatetimes = explode('|', $request->upd, true);
                    $dids = explode('|', $request->did, true);
                    $_ids = explode('|', $request->id, true);
                    for ($i = 0 ; $i < count($updatetimes); $i++){
                        $datas[] = array(
                            'kid' => $this->keyId,
                            'did' => $dids[$i],
                            '_id' => $_ids[$i],
                            'fromid' => $fromshopids[$i],
                            'toid' =>  $toshopids[$i],
                            'itemid' => $itemids[$i],
                            'itemidto' => $itemidtos[$i],
                            'qty' => $qtys[$i],
                            'remark' => $remarks[$i],
                            'updatetime' => $updatetimes[$i],
                            'id' => $_ids[$i],
                            'log' => 0
                        );
                    }
                }else{
                    $datas[] = array(
                        'kid' => $this->keyId,
                        'did' => $request->did,
                        '_id' => $request->_id,
                        'fromid' => $request->fid,
                        'toid' => $request->tid,
                        'itemid' => $request->iid,
                        'itemidto' => $request->iidt,
                        'qty' => $request->qty,
                        'remark' => $request->rem,
                        'updatetime' => $request->upd,
                        'id' => $request->id,
                        'log' => 0
                    );
                }
                foreach ($datas as $data) {
                    Convert::insertUpdate($data,['id'=>$data['id']]);
                }
            }

        $datas2 = array();

            if($request->dupd != null){
                if(strpos($request->dupd, '|') !==false){
                    $_ids = explode('|',$request->d_id);
                    $fromshopids = explode('|',$request->dfid, true);
                    $toshopids = explode('|', $request->dtid, true);
                    $itemidtos = explode('|', $request->diid, true);
                    $qtytos = explode('|', $request->dqty, true);
                    $remarks = explode('|', $request->drem, true);
                    $updatetimes = explode('|', $request->dupd, true);
                    $dids = explode('|', $request->dddid,true);
                    $ids = explode('|', $request->ddid, true);
                    $convert_id = explode('|', $request->id);

                    for($i = 0; $i < count($updatetimes); $i++){
                        $datas2[] = array(
                            'kid' => $this->keyId,
                            'did' => $dids[$i],
                            '_id' => $_ids[$i],
                            'fromid' => $fromshopids[$i],
                            'toid' => $toshopids[$i],
                            'remark' => $remarks[$i],
                            'updatetime' => $updatetimes[$i],
                            'id' => $ids[$i],
                            'convert_id' => $convert_id[$i],
                            'itemidto' => $itemidtos[$i],
                            'qtyto' => $qtytos[$i],
                            'log' => 0,
                        );
                    }
                }else{
                    $datas2[] = array(
                        'kid' => $this->keyId,
                        'did' => $request->dddid,
                        '_id' => $request->d_id,
                        'fromid' => $request->dfid,
                        'toid' => $request->dtid,
                        'remark' => $request->drem,
                        'updatetime' => $request->dupd,
                        'id' => $request->ddid,
                        'itemidto' => $request->diid,
                        'qtyto' => $request->dqty,
                        'convert_id' => $request->id,
                        'log' =>0,
                    );
                }
                foreach ($datas2 as $data){
                    ConvertDetail::insertUpdate($data, ['id'=>$data['id']]);
                }
            }
          return  $this->converts_get($request,$limit,$offset);
    }
}
