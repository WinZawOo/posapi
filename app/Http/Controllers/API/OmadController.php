<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Omad;
use App\Imei;
use Validator;

class OmadController extends BaseController
{
    //Omad Get Method
    public function omads_get(Request $request, $limit= NULL, $offset = NULL)
    {
        $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        $count = Omad::all()->count();
        $max_log_id = Omad::get('log')->max();

        if($count == null) $count = 0;
        if($max_log_id == null) $max_log_id = 0;

        $col = '*';

        $omads = Omad::getAll($col, $limit, $offset);


        if($omads)
        {
            array_push($omads, array('max_log_id' => $max_log_id, 'count' => $count, 'status' => 1,
                'limit'=>$limit, 'offset'=>$offset));
            return $this->sendResponse($omads, 'Omad Retrieved Successfully.');   //OK
        }
        else
        {
             return $this->sendResponse(array(array('max_log_id' => $max_log_id, 'count' => $count, 'status'=>0, 'error' => '0 row' )));
        }
    }

    //omad PUT Method

    public function omads_post(Request $request, $limit= NULL, $offset = NULL)
    {

        $omads = new Omad();

        $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();

        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        if( $request->updatetime != false )
        {
            if (strpos($request->updatetime, '|') !== false)
            {
            	$ids = explode('|',$request->id);
                $codes = explode('|',$request->code);
                $names = explode('|',$request->name);
                $areas = explode('|',$request->area);
                $updatetimes = explode('|',$request->updatetime);
                $states = explode('|',$request->state);


                for($i = 0; $i < count($updatetime); $i++)
                {
                	$datas[] = array(
                    'id' => $ids[$i],
                    'code' => $codes[$i],
                    'name' => $names[$i],
                    'area' => $areas[$i],
                    'updatetime' => $updatetimes[$i],
                    'state' => $states[$i],
                    'log' => 0
                );

                }
            }

            else    // qty does not have | char
            {
                $datas[] = array(
                    'id' => $request->id,
                    'code' => $request->code,
                    'name' => $request->name,
                    'area' => $request->area,
                    'state' => $request->state,
                    'log' => 0,
                    'updatetime' =>$request->updatetime,
                );
            }

            foreach($datas as $data)
            {
                Omad::updateInsert($data);
            }

            return $this->omads_get($request, $limit, $offset);
         }
         else
         {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);

         }
     }

}
