<?php

namespace App\Http\Controllers\API;

use App\Item;
use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Http\Request;
use App\Imei;
use App\Key;
use App\ShopStock;
use Intervention\Image\Facades\Image;
use Validator;

class ItemController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function items_get(Request $request, $limit= NULL, $offset = NULL)
    {
        $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        $count = Item::all()->count();
        $max_log_id = Item::get('log')->max();

        if($count == null) $count = 0;
        if($max_log_id == null) $max_log_id = 0;

        $col = [
        'id AS id',
        '_id AS _i',
        'did AS di',
        'catid AS ci',
        'catcode AS cc',
        'supplierid AS su',
        'name AS n1',
        'name2 AS n2',
        'unit AS u1',
        'unit2 AS u2',
        'image AS im',
        'code AS co',
        'barcode AS ba',
        'model AS mo',
        'spec1 AS s1',
        'spec2 AS s2',
        'spec3 AS s3',
        'cost AS c1',
        'cost2 AS c2',
        'price AS p1',
        'price2 AS p2',
        'price3 AS p3',
        'remark AS re',
        'updatetime AS up',
        'version AS ve',
        'encrypt AS en',
        'state AS st',
        'lockprice AS lo',
        'qty AS qt',
        'brand AS br',
        'uom AS uo',
        'old_code AS ol',
        'nonstock AS ns',
        'log AS log',
        'autoid AS autoid'];

        $items = Item::getAll($col,$limit,$offset);


        if($items)
        {
            array_push($items, array('max_log_id' => $max_log_id, 'count' => $count, 'status' => 1,
                'limit'=>$limit, 'offset'=>$offset));
            return $this->sendResponse($items, 'Item Retrieved Successfully.');   //OK
        }
        else
        {
             return $this->sendResponse(array(array('max_log_id' => $max_log_id, 'count' => $count, 'status'=>0, 'error' => '0 row' )));
        }
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function items_post(Request $request, $limit= NULL, $offset = NULL)
    {
        $item = new Item();


        $img_name = "";
        $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }


        if($file   =   $request->file('image')) {

            $img_name      =   time().time().'.'.$file->getClientOriginalExtension();

            $target_path    =   public_path('/uploads/');

            $file->move($target_path, $img_name);
            }



        if( $request->name != null )
        {
            if (strpos($request->name, '|') !== false)
            {
                $idall =        explode('|',$request->id);
                $_ids =         explode('|',$request->_id);
                $dids =         explode('|',$request->did);
                $cids =         explode('|',$request->catid);
                $catcodes =     explode('|',$request->catcode);
                $supplierids =  explode('|',$request->supplierid);
                $names =        explode('|',$request->name);
                $names2 =       explode('|',$request->names2);
                $units =        explode('|',$request->unit);
                $units2 =       explode('|',$request->unit2);
                $images =       explode('|',$img_name);
                $codes =        explode('|',$request->code);
                $models =       explode('|',$request->model);
                $spec1s =       explode('|',$request->spec1);
                $spec2s =       explode('|',$request->spec2);
                $spec3s =       explode('|',$request->spec3);
                $costs =        explode('|',$request->cost);
                $costs2 =       explode('|',$request->cost2);
                $prices =       explode('|',$request->price);
                $prices2 =      explode('|',$request->price2);
                $prices3 =      explode('|',$request->price3);
                $remarks =      explode('|',$request->remark);
                $encrypts =     explode('|',$request->encrypt);
                $states =       explode('|',$request->state);
                $brands =       explode('|',$request->brand);
                $nonstocks =    explode('|',$request->nonstock);
                $old_code = explode('|',$request->old_code);
                $qty = explode('|',$request->qty);
                $updatetimes =    explode('|',$request->updatetime);



                for($i = 0; $i < count($updatetimes); $i++)
                {
                    $datas[] = array(
                        'kid' => $this->key,
                        'id' => $idall[$i],
                        '_id' => $_ids[$i],
                        'did' => $dids[$i],
                        'catid' => $cids[$i],
                        'catcode' => $catcodes[$i],
                        'supplierid' => $supplierids[$i],
                        'name' => $names[$i],
                        'name2' => $names2[$i],
                        'unit' => $units[$i],
                        'unit2' => $units2[$i],
                        'image' => $images[$i],
                        'code' => $codes[$i],
                        'model' => $models[$i],
                        'spec1' => $spec1s[$i],
                        'spec2' => $spec2s[$i],
                        'spec3' => $spec3s[$i],
                        'cost' => $costs[$i],
                        'cost2' => $costs2[$i],
                        'price' => $prices[$i],
                        'price2' => $prices2[$i],
                        'price3' => $prices3[$i],
                        'remark' => $remarks[$i],
                        'updatetime' => $updatetimes[$i],
                        'encrypt' => $encrypts[$i],
                        'state' => $states[$i],
                        'log' => 0,
                        'brand' => $brands[$i],
                        'nonstock' => $nonstocks[$i],
                        'old_code' => $old_code[$i],
                        'qty' => $qty[$i]
                    );

                }
            }

            else    // name does not have | char
            {
                $datas[] = array(
                    'kid' => $this->key,
                    'id' => $request->id,
                    '_id' => $request->_id,
                    'did' => $request->did,
                    'catid' => $request->catid,
                    'catcode' => $request->catcode,
                    'supplierid' => $request->supplierid,
                    'name' => $request->name,
                    'name2' => $request->name2,
                    'unit' => $request->unit,
                    'unit2' => $request->unit2,
                    'image' => $img_name,
                    'code' => $request->code,
                    'model' => $request->model,
                    'spec1' => $request->spec1,
                    'spec2' => $request->spec2,
                    'spec3' => $request->spec3,
                    'cost' => $request->cost,
                    'cost2' => $request->cost2,
                    'price' => $request->price,
                    'price2' => $request->price2,
                    'price3' => $request->price3,
                    'remark' => $request->remark,
                    'updatetime' => $request->updatetime,
                    'version' => $request->version,
                    'encrypt' => $request->encrypt,
                    'state' => $request->state,
                    'log' => 0,
                    'brand' => $request->brand,
                    'nonstock' => $request->nonstock,
                    'old_code' => $request->old_code,
                    'qty' => $request->qty );
            }

            foreach($datas as $data)
            {
               Item::updateInsert($data);
            }

            return $this->items_get($request, $limit, $offset);
        }
        else
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);

        }
    }
}

