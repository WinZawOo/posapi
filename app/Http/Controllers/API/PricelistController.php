<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Imei;
use App\Pricelist;
use Validator;

class PricelistController extends BaseController
{
    //Price List GET Method
    public function pricelists_get(Request $request, $limit= NULL, $offset = NULL)
    {
    	 $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        $count = Pricelist::all()->count();
        $max_log_id = Pricelist::get('log')->max();

        if($count == null) $count = 0;
        if($max_log_id == null) $max_log_id = 0;

        $col = [
        'id',
        'code AS co',
        'plist AS pl',
        'currency AS cu',
        'price AS pr',
        'overwritten AS ov',
        'state AS st',
        'log AS log'];

        $pricelists = Pricelist::getAll($col, $limit, $offset);

        if($pricelists)
        {
            array_push($pricelists, array('max_log_id' => $max_log_id, 'count' => $count, 'status' => 1,
                'limit'=>$limit, 'offset'=>$offset));
            return $this->sendResponse($pricelists, 'Pricelist Retrieved Successfully.');   //OK
        }
        else
        {
             return $this->sendResponse(array(array('max_log_id' => $max_log_id, 'count' => $count, 'status'=>0, 'error' => '0 row' )));
        }
    }

    //Price List POST or PUT Method

    public function pricelists_post(Request $request, $limit= NULL, $offset = NULL)
    {
        $pricelists = new Pricelist();

        $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        if( $request->updatetime != false )
        {
            if (strpos($request->updatetime, '|') !== false)
            {
            	$ids = explode('|',$request->id, true);
            	$codes = explode('|',$request->code, true);
                $updatetimes = explode('|',$request->updatetime, true);
                $states = explode('|',$request->state, true);

   				for($i = 0; $i < count($updatetime); $i++)
                {
                    $datas[] = array(
                    'id' => $ids[$i],
                    'code' => $codes[$i],
                    'updatetime' => $updatetimes[$i],
                    'state' => $states[$i],
                    'log' => 0
                );

                }
            }

            else    // qty does not have | char
            {
                if($request->id != NULL)
                {
                    $datas[] = array(
                        'id' => $request->id,
                        'code' => $request->code,
                        'updatetime' => $request->updatetime,
                        'state' => $request->state,
                        'log' => 0
                    );
                }

            }

            foreach($datas as $data)
            {
                Pricelist::updateInsert($data);
            }

            return $this->pricelists_get($request, $limit, $offset);
         }
         else
         {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);

         }
     }

}
