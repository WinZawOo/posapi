<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Warehouse;
use App\Imei;

class WarehouseController extends BaseController
{
    public function warehouses_get(Request $request, $limit = NULL, $offset = NULL)
    {
    	$count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        $count = Warehouse::all()->count();
        $max_log_id = Warehouse::get('log')->max();

        if($count == null) $count = 0;
        if($max_log_id == null) $max_log_id = 0;

        $col = [
        'id AS id',
        'code AS code',
        'name AS name',
        'groups AS group',
        'updatetime AS updatetime',
        'sync AS sync',
        'state AS state',
        'log AS log'];


		$message = Warehouse::getAll($col, $limit, $offset);

		if($message)
        {
            array_push($message, array('max_log_id' => $max_log_id, 'count' => $count, 'status' => 1,
                'limit'=>$limit, 'offset'=>$offset));
            return $this->sendResponse($message, 'Warehouse Data Retrieved Successfully.');   //OK
        }
        else
        {
             return $this->sendResponse(array(array('max_log_id' => $max_log_id, 'count' => $count, 'status'=>0, 'error' => '0 row' )));
        }
    }

    public function warehouses_post(Request $request, $limit = NULL, $offset = NULL)
    {
    	$count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        $message = array();
        $datas = array();


		if (strpos($request->updatetime, '|') !== false)
		{ 	// name has | char
			$ids = 		explode('|',$request->id);
			$codes = 		explode('|',$request->code);
            $names =        explode('|',$request->name);
            $groups =        explode('|',$request->group);
			$updatetimes = 	explode('|',$request->updatetime);
            $states = 	    explode('|',$request->state);

			for($i = 0; $i < count($updatetimes); $i++)
			{
				$datas[] = array(
					'id' => $ids[$i],
					'code' => $codes[$i],
                    'name' => $names[$i],
                    'groups' => $groups[$i],
					'updatetime' => $updatetimes[$i],
                    'state' => $states[$i],
                    'log' => 0
				);
			}
		}

		else	// name does not have | char
		{
            if($request->id != '')
            {
                $datas[] = array(
                    'id' => $request->id,
                    'code' => $request->code,
                    'name' => $request->name,
                    'groups' => $request->group,
                    'updatetime' => $request->updatetime,
                    'state' => $request->state,
                    'log' => 0
                );
            }
		}

		foreach($datas as $data)
		{
			Warehouse::updateInsert($data);
		}

		return $this->warehouses_get($request, $limit, $offset);
    }
}
