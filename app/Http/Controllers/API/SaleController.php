<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\BaseContoller as BaseContoller;
use App\Sale;
use App\SalesDetail;
use App\SaleUpdate;
use App\Imei;
use App\ShopStock;
use App\Warehouse;

class SaleController extends BaseController
{
    //Sale GET Method
    public function salesorders_get(Request $request)
    {
    	$count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        $col1 = [
        'id AS id',
        '_id AS _id',
        'did AS Did',
        'sid AS Shopid',
        'cod AS Code',
        'cid AS Custid',
        'ccd AS Custcode',
        'ccn AS CustCardname',
        'cnm AS Custname',
        'uid AS Userid',
        'amt AS Amount',
        'dis AS Discount',
        'dks AS Discountks',
        'tax AS Tax',
        'ttl AS Total',
        'pid AS Paid',
        'chg AS Change',
        'rmn AS Remain',
        'qty AS qty',
        'sta AS State',
        'rmk AS Remark',
        'udt AS Updatetime',
        'version AS ver',
        'prt AS prt',
        'eve AS eve',
        'cpn AS cpn',
        'dlr AS dlr',
        'drm AS drm',

        'prj AS Project',
        'brh AS Branch',
        'dtr AS Dist',
        'slt AS Salestype',
        'dpt AS Dept',
        'shp AS Shipping',
        'adr AS Address',
        'dld AS Deldate',
        'slp AS Salesperson',

        'srs AS Series',
        'srn AS Seriesname',
        'det AS Docentry',
        'dnb AS Docnum',
        'cod AS dcc',
        'caa AS Carrier',
        'gnb AS Groupnum',
        'ucd AS Usercode'];

        $sales = Sale::getAllWhere($col1);

        if($sales)
        {
            return $this->sendResponse($sales, 'Sale Order Retrieved Successfully.');   //OK
        }
        else
        {
             return $this->sendError(array('status' =>0, 'msg' => '0 row'));
        }

    }

    //Sale Order Detail GET Method

    public function salesorderdetails_get(Request $request)
    {
        $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        // $col = [
        //     'id           AS Id',
        //     '_id          AS _i',
        //     'did          AS Did',
        //     'sale_id      AS Salesid',
        //     'iid          AS Itemid',
        //     'sid          AS Shopid',
        //     'qty          AS Qty',
        //     'prc          AS Price',
        //     'rmk          AS Remark',
        //     'udt          AS Updatetime',
        //     'version      AS Version',
        //     'foc          AS Foc',
        //     'srl          AS Serial',
        //     'bth          AS Batch',
        //     'dlr          AS Todelive',
        //     'duid         AS du',
        //     'dtm         AS Delivetime',
        //     'drv         AS Driver',
        //     'vhl         AS Vehacle',
        //     'brd         AS Brand',
        //     'car         AS Carrier',
        //     'cod         AS Code',
        //     'prj         AS Project',
        //     'dis         AS Discountks',
        //     'but         AS Dist',
        //     'grp         AS Branch',
        //     'csf         AS Dept',
        //     'lin         AS Line'];

        $col2 = '*';

        $salesdetail = SalesDetail::getAllWhere($col2);

        if($salesdetail)
        {
            return $this->sendResponse($salesdetail, 'Sale Order Detail Retrieved Successfully.');   //OK
        }
        else
        {
             return $this->sendError(array('status' =>0, 'msg' => '0 row'));
        }

    }

    //Sale Order Open GET Method
    public function salesorderopens_get(Request $request)
    {
       $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        $sales = Sale::where('sta', '=', 1)->whereNotNull('dnb')->get()->toArray();

        if($sales)
        {
            return $this->sendResponse($sales, 'Sale Order  Retrieved Successfully.');   //OK
        }
        else
        {
             return $this->sendError(array('status' =>0, 'msg' => '0 row'));
        }

    }

    //Get Max ID Method
    public function maxids_get(Request $request ,$did)
    {
        $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }
        $maxid = Sale::getMax_id($did);

        if($maxid)
        {
            return $this->sendResponse($maxid, 'Maximum ID Retrieved Successfully.');   //OK
        }
        else
        {
             return $this->sendError(array('status' =>0, 'msg' => '0 row'));
        }

    }

    //DOC close GET Method
    public function doccloses_get(Request $request)
    {
        $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        $col2 = [
            'id',
            'iid' ,
            'did',
            'act' ,
            'sta' ,
            'cdt',
            'udt' ];

        $data = SaleUpdate::getAllWhere($col2);

        if($data)
        {
            return $this->sendResponse($data, 'Data Retrieved Successfully.');   //OK
        }
        else
        {
             return $this->sendError(array('status' =>0, 'msg' => '0 row'));
        }

    }

    //Sale Order Update GET Method
    public function salesorderupdates_get(Request $request)
    {
        $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        $col2 = [
        'id' ,
        'iid' ,
        'did',
        'act' ,
        'sta' ,
        'cdt',
        'udt'];

        $updateData = SaleUpdate::getAllwhere($col2);

        if($updateData)
        {
            return $this->sendResponse($updateData, 'Sale Order Update Data Retrieved Successfully.');   //OK
        }
        else
        {
             return $this->sendError(array('status' =>0, 'msg' => '0 row'));
        }

    }

    //Last Sale GET Method
    public function lastsales_get(Request $request, $did)
    {
        $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        $count = Sale::all()->count();
        $max_log_id = Sale::get('log')->max();

        if($count == null) $count = 0;
        if($max_log_id == null) $max_log_id = 0;

        $col1 = [
        'id          AS id',
        '_id         AS _id',
        'did         AS did',
        'sid         AS sid',
        'cod         AS cod',
        'cid         AS cid',
        'ccd         AS ccd',
        'uid         AS uid',
        'amt         AS amt',
        'dis         AS dis',
        'dks         AS dks',
        'tax         AS tax' ,
        'ttl         AS ttl',
        'pid         AS pid',
        'chg         AS chg',
        'rmn         AS rmn',
        'qty         AS qty',
        'sta         AS sta',
        'rmk         AS rmk',
        'udt         AS udt',
        'version     AS ver',
        'prt         AS prt',
        'eve         AS eve',
        'cpn         AS cpn',
        'dlr         AS dlr',
        'dle         AS dle',
        'drm         AS drm',
        'prj         AS prj',
        'brh         AS brh',
        'dtr         AS dtr',
        'slt         AS slt',
        'dpt         AS dpt',
        'shp         AS shp',
        'srs         AS srs',
        'srn         AS srn',
        'det         AS det',
        'dnb         AS dnb',
        'cod         AS dcc',
        'ccn         AS ccn',
        'caa         AS caa',
        'gnb         AS gnb',
        'slp         AS slp',
        'cnm         AS cnm',
        'ucd         AS ucd',
        'adr         AS adr',
        'dld         AS dld',
        'upd         AS upd',
        'autoid      AS aid',
        'cdt         AS cdt',
        'pty         AS pty',
        'log         AS log',
        'autoid      AS autoid'];

        $col2 = [
        'id          AS id',
        '_id         AS _i',
        'did         AS di',
        'sal',
        'iid         AS ii',
        'sid         AS si',
        'qty         AS qt',
        'prc         AS pr',
        'rmk         AS rm',
        'udt         AS ud',
        'version     AS ve',
        'foc         AS fo',
        'srl         AS sr',
        'bth         AS bt',
        'dlr         AS dl',
        'duid        AS du',
        'dtm         AS dt',
        'drv         AS dr',
        'vhl         AS vh',
        'brd         AS br',
        'dis         AS ds',
        'car         AS ca',
        'prj         AS pj',
        'upd         AS up',
        'sta         AS st',
        'log         AS lo',
        'autoid      AS au'];

        $message = Sale::getLastAllDetail($col1, $col2, $did);

        if($message)
        {
            array_push($message, array('max_log_id' => $max_log_id, 'count' => $count, 'status' => 1, 'limit'=>1, 'offset'=>0,));
            return $this->sendResponse($message, 'Purchase Retrieved Successfully.');   //OK
        }
        else
        {
             return $this->sendResponse(array(array('max_log_id' => $max_log_id, 'count' => $count, 'status'=>0, 'error' => '0 row' )));
        }
    }

    //Sale GET Method
    public function sales_get(Request $request, $limit = NULL, $offset = NULL, $oversale = NULL)
    {
        $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        $count = Sale::all()->count();
        $max_log_id = Sale::get('log')->max();

        if($count == null) $count = 0;
        if($max_log_id == null) $max_log_id = 0;

        $col1 = [
        'id  AS id',
        '_id AS _id',
        'did AS did',
        'sid AS sid',
        'cod AS cod',
        'cid AS cid',
        'ccd AS ccd',
        'uid AS uid',
        'amt AS amt',
        'dis AS dis',
        'dks AS dks',
        'tax AS tax',
        'ttl AS ttl',
        'pid AS pid',
        'chg AS chg',
        'rmn AS rmn',
        'qty AS qty',
        'sta AS sta',
        'rmk AS rmk',
        'udt AS udt',
        'version AS ver',
        'prt AS prt',
        'eve AS eve',
        'cpn AS cpn',
        'dlr AS dlr',
        'dle AS dle',
        'drm AS drm',
        'prj AS prj',
        'brh AS brh',
        'dtr AS dtr',
        'slt AS slt',
        'dpt AS dpt',
        'shp AS shp',
        'srs AS srs',
        'srn AS srn',
        'det AS det',
        'dnb AS dnb',
        'cod AS dcc',
        'ccn AS ccn',
        'caa AS caa',
        'gnb AS gnb',
        'slp AS slp',
        'cnm AS cnm',
        'ucd AS ucd',
        'adr AS adr',
        'dld AS dld',
        'upd AS upd',
        'autoid AS aid',
        'cdt AS cdt',
        'pty AS pty',
        'log AS log',
        'autoid AS autoid',
        'err AS err'];

        $col2 = [
        'id AS id',
        '_id AS _i',
        'did AS di',
        'sal',
        'iid AS ii',
        'sid AS si',
        'qty AS qt',
        'prc AS pr',
        'rmk AS rm',
        'udt AS ud',
        'version AS ve',
        'foc AS fo',
        'srl AS sr',
        'bth AS bt',
        'dlr AS dl',
        'duid AS du',
        'dtm AS dt',
        'drv AS dr',
        'vhl AS vh',
        'brd AS br',
        'dis AS ds',
        'car AS ca',
        'prj AS pj',
        'upd AS up',
        'sta AS st',
        'log AS lo',
        'autoid AS au',
        'but AS bu',
        'grp AS gp',
        'csf AS cf'];

        $sales = Sale::getAllDetail($col1, $col2, $limit, $offset);

        if($sales)
        {
            array_push($sales, array('max_log_id' => $max_log_id, 'count' => $count, 'status' => 1,
                'limit'=>$limit, 'offset'=>$offset, 'oversale'=>$oversale));
            return $this->sendResponse($sales, 'Sale Retrieved Successfully.');   //OK
        }
        else
        {
             return $this->sendResponse(array(array('max_log_id' => $max_log_id, 'count' => $count, 'status'=>0, 'error' => '0 row' )));
        }
    }

    //Sale Order POST Method
    public function salesorders_post(Request $request)
    {
        $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        if($request->code != NULL)
        {
             $orders = explode('$',$request->code);


             if (strpos($orders[0], '|') !== false)
            {
                for($i = 0; $i < count($codes); $i++)
                {
                    $arr = explode(',', $codes[$i]);
                    $datas[] = array(
                    'id'        => $arr[0],
                    'docentry' => $arr[1],      //docentry 60009,94055,180103376,1694
                    'docnum' => $arr[2],
                    'series' => $arr[3],
                     );
                }



            }
            else    // name does not have | char
            {

                $arr = explode(',', $orders[0]);
                if($arr[0]!="")
                {
                    $datas[] = array(
                        'id'    => $arr[0],
                        'docentry' => $arr[1],      //docentry
                        'docnum' => $arr[2],
                        'series' => $arr[3],
                    );
                }

            }

            foreach($datas as $data)
            {
                if($data['id'] > 0){
                    Sale::updateCode($data);
                    $updated = true;
                }
            }

            //----------------------Sale Detail----------------//

             if (strpos($orders[1], '|') !== false)
            {

                $detailcode = explode('|',$orders[1]);

                for($i = 0; $i < count($codes); $i++)
                {
                    $res = explode(',', $detailcode[$i]);
                    $details[] = array(
                    'id'        => $res[0],
                    'linenum' => $res[1],      //docentry 60009,94055,180103376,1694
                     );
                }



            }
            else    // name does not have | char
            {

                $res = explode(',', $orders[1]);
                if($res[0]!="")
                {
                    $details[] = array(
                        'id'    => $res[0],
                        'linenum' => $res[1],      //docentry

                    );
                }

            }

            foreach($details as $detail)
            {
                if($detail['id'] > 0){
                    SalesDetail::updateCode($detail);
                    $updated = true;
                }
            }

            //-------------------Sale Close---------------//

            if (strpos($orders[2], '|') !== false)
            {
                // name has | char
                $closes = explode('|',$orders[2]);
                foreach($closes as $close)
                {
                    $close_arr[] = array( 'id' => $close );
                }
            }
            else    // name does not have | char
            {
                $close_arr[] = array(
                    'id' => $orders[2]    //docentry
                );
            }

            foreach($close_arr as $close)
            {
                if($close['id'] > 0){
                    Sale::updateClosed($close);
                    $updated = true;
                }
            }

            //-----------------Sale Cancel---------------//

            if (strpos($orders[3], '|') !== false)
            { // name has | char
                $cancels = explode('|',$orders[3]);
                foreach($cancels as $cancel)
                {
                    $datasCancel[] = array( 'id' => $cancel );
                }
            }
            else    // name does not have | char
            {
                $datasCancel[] = array(
                    'id' => $orders[3]    //docentry
                );
            }
            foreach($datasCancel as $cancel)
            {
                if($cancel['id'] > 0){
                    Sale::updateCanceled($cancel);
                    $updated = true;
                }
            }

            //---------------Sale Error---------------//

            if (strpos($orders[4], '|') !== false)
            { // name has | char
                $error = explode('|',$orders[4]);
                foreach($error as $data)
                {
                    $tmp = explode('@@',$data);
                    $datasError[] = array('id'=>$tmp[0], 'err'=>$tmp[1]);
                }
            }
            else    // name does not have | char
            {
                $tmp = explode('@@',$orders[4]);

                $datasError[] = array('id'=>$tmp[0], 'err'=>$tmp[1]);
            }

            foreach($datasError as $errors)
            {
                if($errors['id'] > 0){
                    Sale::updateError($errors);
                    $updated = true;
                }
            }
        }

        if($updated)
        {
            $message = array('status' => 1);
            return $this->sendResponse($message, 'Updated!');
        }
        else
         {
            $message = array( 'status' => 0, 'message' => 'No Updated!' );
            return $this->sendError($message);

         }
    }

    public function sales_post(Request $request, $limit = NULL, $offset = NULL)
    {
        $count = Imei::where('s',$request->header('s'))->where('simid', $request->header('sim'))->get()->count();
        if($count< 1)
        {
            $message = array( 'status' => 0, 'error' => 'No Rights!' );
            return $this->sendError($message);
        }

        if( $request->ovr != NULL ) //oversale
        {
            if($request->ovr == 0)
            {
                if (strpos($request->udt, '|') !== false)  //updatetime has| char
                {
                    $shopids = explode('|',$request->sid);
                    //$salesids = explode('|',$request->sal, true);
                    $itemsid = explode('|', $request->iid);
                    $ids = explode('|',$request->id);
                    $prices = explode('|',$request->prc);
                    $qtys = explode('|',$request->qty);

                    for($i = 0; $i < count($shopids); $i++)
                    {
                        $tochecks[] = array(
                        'sid'       => $shopids[$i],
                        'sal'      => $salesids[$i],
                        'iid'       => $itemids[$i],
                        'prc'       => $prices[$i],
                        'qty'       => $qtys[$i],
                        'id'      => $ids[$i],

                        );
                    }
                }
                else
                {
                   $tochecks[] = array(
                        'sid'   => $request->sid,
                        'sal' => $request->sal,
                        'iid'   => $request->iid,
                        'prc'   => $request->prc,
                        'qty'       => $request->qty,
                        'id' => $request->id,

                    );
                }

                foreach($tochecks as $data)
                {
                    if($data['id'] > 0){
                        $row =ShopStock::getRowsByColumns($request, $limit, $offset);

                        if(count($row) > 0){
                            if($data['qty']>$row[0]['qty']){
                                $oversales[] = array(
                                    'sid'       => $data['sid'],
                                    'sal'      => $data['sal'],
                                    'iid'       => $data['iid'],
                                    'prc'       => $data['prc'],
                                    'qty'       => $data['qty'],
                                    'id'      => $data['id'],
                                    'sqty'      => $row[0]['qty']
                                );
                             }
                        }
                    }
                }
            }
        }

        // print_r($oversales);

        // die();

        $datas = array();
        $datas2 = array();

        if($request->udt != NULL)
        {
            if(strpos($request->udt, '|') !== false)
            {
                $_ids =         explode('|',$request->_id);
                $shopids =      explode('|',$request->sid);
                $dids =         explode('|',$request->did);
                $codes =        explode('|',$request->cod);
                $customerids =    explode('|',$request->cid);
                $customercodes =    explode('|',$request->ccd);
                $customercodenames =    explode('|',$request->ccn);
                $userids =      explode('|',$request->uid);
                $amounts =      explode('|',$request->amt);
                $discounts =    explode('|',$request->dis);
                $discountkss =  explode('|',$request->dks);
                $taxs =         explode('|',$request->tax);
                $totals =       explode('|',$request->ttl);
                $paids =        explode('|',$request->pid);
                $changes =      explode('|',$request->chg);
                $remains =      explode('|',$request->rmn);
                $qtys =         explode('|',$request->qty);
                $states =       explode('|',$request->sta);
                $remarks =      explode('|',$request->rmk);
                $updatetimes =  explode('|',$request->udt);
                $versions =     explode('|',$request->ver);
                $printeds =     explode('|',$request->prt);
                $events =       explode('|',$request->eve);
                $coupons =      explode('|',$request->cpn);
                $delivers =         explode('|',$request->dlr);
                $dremarks =         explode('|',$request->drm);
                $idas =         explode('|',$request->ida);

                $prjs =         explode('|',$request->prj);
                $brhs =         explode('|',$request->brh);
                $dtrs =         explode('|',$request->dtr);
                $slts =         explode('|',$request->slt);
                $dpts =         explode('|',$request->dpt);
                $shps =         explode('|',$request->shp);
                $caas =         explode('|',$request->caa);
                $srns =         explode('|',$request->srn);
                $srss =         explode('|',$request->srs);
                $gnbs =         explode('|',$request->gnb);
                $slps =         explode('|',$request->slp);
                $dets =         explode('|',$request->det);
                $dnbs =         explode('|',$request->dnb);


                $cnms =         explode('|',$request->cnm);
                $ucds =         explode('|',$request->ucd);   // user code

                $adrs =         explode('|',$request->adr);   // printing address
                $dlds =         explode('|',$request->dld);   // printing address

                $upds =         explode('|',$request->upd);   // to update sap, 1 = yes
                $ptys =         explode('|',$request->pty);   // to update sap, 1 = yes

                for($i = 0; $i < count($updatetimes); $i++)
                {
                    $upds[$i] = $upds[$i] == '' ? 1 : $upds[$i];
                    $ptys[$i] = $ptys[$i] == '' ? 1 : $ptys[$i];
                }

                for($i = 0; $i < count($updatetimes); $i++)
                {
                    $datas[] = array(
                        'kid'       => $this->key,
                        'did'       => $dids[$i],
                        '_id'       => $_ids[$i],
                        'cod'       => $codes[$i],
                        'sid'   => $shopids[$i],
                        'cid'  => $customerids[$i],
                        'ccd'  => $customercodes[$i],
                        'ccn'  => $customercodenames[$i],
                        'uid'     => $userids[$i],
                        'amt'   => $amounts[$i],
                        'dis'   => $discounts[$i],
                        'dks'  => $discountkss[$i],
                        'tax'       => $taxs[$i],
                        'ttl'   => $totals[$i],
                        'pid'       => $paids[$i],
                        'chg'   => $changes[$i],
                        'rmn'   => $remains[$i],
                        'qty'       => $qtys[$i],
                        'sta'   => $states[$i],
                        'rmk'   => $remarks[$i],
                        'udt'  => $updatetimes[$i],
                        'syc'  => 1,
                        'log'   => 0,
                        'prt'   => $printeds[$i],
                        'eve'   => $events[$i],
                        'cpn'   => $coupons[$i],
                        'dlr'   => $delivers[$i],
                        'drm'   => $dremarks[$i],
                        'id'    => $idas[$i],

                        'prj'    => $prjs[$i],
                        'brh'    => $brhs[$i],
                        'dtr'    => $dtrs[$i],
                        'slt'    => $slts[$i],
                        'dpt'    => $dpts[$i],
                        'shp'    => $shps[$i],
                        'det'    => $dets[$i],
                        'dnb'    => $dnbs[$i],
                        'caa'    => $caas[$i],
                        'srn'    => $srns[$i],
                        'srs'    => $srss[$i],
                        'gnb'    => $gnbs[$i],
                        'slp'    => $slps[$i],
                        'cnm'    => $cnms[$i],
                        'ucd'    => $ucds[$i],
                        'adr'    => $adrs[$i],
                        'dld'    => $dlds[$i],
                        'upd'   => $upds[$i],
                        'pty'   => $ptys[$i]
                    );
                }

            }
            else
            {
                $datas[] = array(
                    'kid'       => $this->key,

                    'did'       => $request->did,
                    '_id'       => $request->_id,
                    'cod'       => $request->cod,
                    'sid'      => $request->sid,
                    'cid'     => $request->cid,
                    'ccd'     => $request->ccd,
                    'ccn'     => $request->ccn,
                    'uid'     => $request->uid,
                    'amt'      =>$request->amt,
                    'dis'      => $request->dis,
                    'dks'     => $request->dks,
                    'tax'       => $request->tax,
                    'ttl'      => $request->ttl,
                    'pid'       => $request->pid,
                    'chg'      => $request->chg,
                    'rmn'      => $request->rmn,
                    'qty'       => $request->qty,
                    'sta'      => $request->sta,
                    'rmk'      => $request->rmk,
                    'udt'     => $request->udt,
                    'syc'     => 1,
                    'log'   => 0,
                    'prt'   => $request->prt,
                    'eve'   => $request->eve,
                    'cpn'   => $request->cpn,
                    'dlr'   => $request->dlr,
                    'drm'   => $request->drm,
                    'id'    => $request->ida,

                    'prj'   => $request->prj,
                    'brh'   => $request->brh,
                    'dtr'   => $request->dtr,
                    'slt'   => $request->slt,
                    'dpt'   => $request->dpt,
                    'shp'   => $request->shp,
                    'slp'   => $request->slp,
                    'det'   => $request->det,
                    'caa'   => $request->caa,
                    'srn'   => $request->srn,
                    'srs'   => $request->srs,
                    'gnb'   => $request->gnb,
                    'slp'   => $request->slp,
                    'dnb'   => $request->dnb,
                    'cnm'   => $request->cnm,
                    'ucd'   => $request->ucd,
                    'adr'   => $request->adr,
                    'dld'   => $request->dld,
                    'upd'   => $request->upd == '' ? 1 : $request->upd,
                    'pty'   => $request->pty == '' ? 1 : $request->pty,

                );
            }


                foreach($datas as $data)
                {
                    if($data['id'] > 0)
                    {

                        $found = false;
                        foreach ($oversales as $oversale)
                        {
                            if($oversale['sal'] == $data['id']){
                                $found = true;
                                continue;
                            }
                        }
                        if(!$found)
                            Sale::updateOrCreate(['id' => $data['id']], $data);
                    }
                }
            //Sale Detail
            if(strpos($request->ud, '|') !== false )
            {
                $_ids        =         explode('|',$request->_i);
                $dids        =         explode('|', $request->di);
                $shopids     =      explode('|',$request->si);
                $salesids    =     explode('|',$request->sa);
                $itemids     =      explode('|',$request->ii);
                $ids         =          explode('|',$request->idd);
                $prices      =       explode('|',$request->pr);
                $qtys        =         explode('|',$request->qt);
                $remarks     =      explode('|',$request->rm);
                $updatetimes =         explode('|',$request->ud);
                $focs        =         explode('|',$request->fo);
                $bths        =         explode('|',$request->bt);
                $srls        =         explode('|',$request->sr);
                $dlrs        =         explode('|',$request->dl);
                $duids       =         explode('|', $request->du);
                $dtms        =         explode('|', $request->dt);
                $drvs        =         explode('|', $request->dr);
                $vhls        =         explode('|', $request->vh);

                $brds        =         explode('|',$request->br);
                $diss        =         explode('|',$request->ds);
                $cars        =         explode('|',$request->ca);
                $prjs        =         explode('|',$request->pj);
                $stas        =         explode('|',$request->st);
                $upds        =         explode('|',$request->up);

                $buts        =         explode('|',$request->bu);    //business unit
                $grps        =         explode('|',$request->gp);    // groups
                $csfs        =         explode('|',$request->cf);    // cash fund


                for($i = 0; $i < count($upds); $i++)
                {
                    $upds[$i] = $upds[$i] == '' ? 1 : $upds[$i];
                }

                for($i = 0; $i < count($updatetimes); $i++)
                {
                    $datas2[] = array(
                        'kid'       => $this->key,
                        'did'       => $dids[$i],
                        '_id'       => $_ids[$i],
                        'sid'       => $shopids[$i],
                        'sale_id'      => $salesids[$i],
                        'iid'       => $itemids[$i],
                        'prc'       => $prices[$i],
                        'qty'       => $qtys[$i],
                        'rmk'       => $remarks[$i],
                        'udt'      => $updatetimes[$i],
                        'syc'      => 1,
                        'id'      => $ids[$i],
                        'foc'       => $focs[$i],
                        'bth'       => $bths[$i],
                        'srl'       => $srls[$i],
                        'dlr'       => $dlrs[$i],
                        'duid'     => $duids[$i],
                        'dtm'      => $dtms[$i] . ' 00:00:00',
                        'drv'      => $drvs[$i],
                        'vhl'      => $vhls[$i],

                        'dis'    => $diss[$i],
                        'brd'    => $brds[$i],
                        'car'    => $cars[$i],
                        'prj'    => $prjs[$i],
                        'upd'   => $upds[$i],
                        'sta'    => $stas[$i],

                        'but'    => $buts[$i],
                        'grp'   => $grps[$i],
                        'csf'    => $csfs[$i]

                    );
                }
            }
            else
            {
                $datas2[] = array(
                    'kid'       => $this->key,
                    'did'       => $request->di,
                    '_id'       => $request->_i,
                    'sid'   => $request->si,
                    'sale_id' => $request->sa,
                    'iid'   => $request->ii,
                    'prc'   => $request->pr,
                    'qty'       => $request->qt,
                    'rmk'   => $request->rm,
                    'udt' => $request->ud,
                    'syc'       => 1,
                    'id' => $request->idd,
                    'foc' => $request->fo,
                    'bth' => $request->bt,
                    'srl' => $request->sr,

                    'dlr' => $request->dl,

                    'duid' => $request->du,
                    'dtm' => $request->dt . ' 00:00:00',
                    'drv' => $request->dr,
                    'vhl' => $request->vh,

                    'prj'    => $request->pj,
                    'upd'   => $request->up == '' ? 1 : $request->up,
                    'sta'    => $request->st,

                    'but'    => $request->bu,
                    'grp'    => $request->gp,
                    'csf'    => $request->cf,
                );
            }

            foreach($datas2 as $data)
            {
                if($data['id'] > 0)
                {

                    $found = false;
                    foreach ($oversales as $oversale)
                    {
                        if($oversale['sal'] == $data['id']){
                            $found = true;
                            continue;
                        }
                    }
                    if(!$found)
                    {
                        SalesDetail::InsertUpdate($data);

                    }
                }
            }

        }

        if($request->del_id != null )
        {
            $data = array(
                    'id'        => $request->del_id,
                    'rmk'       => $request->rmk,
                    'sta'       => 0,
                    'upd'   => 1
                );

            Sale::del($data);

        }

        if( $request->remain_id != null ){

            $data = [
                'id'        =>$request->remain_id,
                'rmn'       =>$request->rmn,
                'rmk'       =>$request->rmk,
                'pid'       =>$request->pid,
                'log'       => 0
            ];

            Sale::where('id', '=', $data['id'])->update($data);

        }

        return $this->sales_get($request, $limit, $offset, $oversales);
    }

    public function openqtys_post(Request $request)
    {
        $zipper = new \Madnest\Madzipper\Madzipper;

        $validator = Validator::make($request->all(), ['zip_file' => 'required | mimetypes:application/zip, application/x-zip-compressed, multipart/x-zip, application/x-compressed']);

        if ($validator->fails())
        {
            return $this->sendError('The file you are trying to upload is not a .zip file. Please try again.', $validator->errors());
        }
        else
        {
            $filename = $request->file('zip_file')->getClientOriginalName();

            $name = explode('.', $filename);

            if($path= $request->file('zip_file')->move(public_path('xml'),$filename))
            {
                $zipper->zip($path)->extractTo(public_path('xml'));
                $zip->close();
                unlink($path);

                if(file_exists(public_path('xml')))
                {
                    $reader = new XMLReader();

                    if ( !$reader->open(public_path('xml').$name[0].'.xml'))
                    {
                        $this->response(array('status' => 0), REST_Controller::HTTP_OK);
                    }
                    else
                    {
                        $strReturn = '';
                        while($reader->read())
                        {
                            // check to ensure nodeType is an Element not attribute or #Text
                            if($reader->nodeType == XMLReader::ELEMENT)
                            {

                                if($reader->localName == 'docentry')
                                {
                                    $reader->read(); // move to its textnode / child
                                    $arr['docentry'] = $reader->value;
                                    $strReturn .= $reader->value . ',';
                                }
                                if($reader->localName == 'openqty')
                                {
                                    $reader->read(); // move to its textnode / child
                                    $arr['openqty'] = $reader->value;
                                    $strReturn .= $reader->value . ',';
                                }
                                if($reader->localName == 'freetxt')
                                {
                                    $reader->read();
                                    $arr['freetxt'] = $reader->value;
                                    $strReturn .= $reader->value . ' - ';

                                    Sale::updateSalesOpenQty($arr);

                                }
                            }
                        }

                        $reader->close();

                        //$this->Uploads->insertToLog('tb_sales');

                        return $this->sendResponse(array('status' => 1, 'msg' => 'Success!', 'updated' => $strReturn), 'Update Sale Open Qty Successfully');

                    }
                }
                else
                {
                    $this->sendError(array('status' => 0, 'msg' => 'Error Uploading'));
                }
            return $this->sendResponse(array('status' => 1, 'msg' => 'Success!'), 'Your .zip file was uploaded and unpacked.');
            }
            else
            {
                $message = "There was a problem with the upload. Please try again.";

                $this->sendError($message);
            }

        }

    }
}
