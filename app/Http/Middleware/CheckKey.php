<?php

namespace App\Http\Middleware;

use App\apiKey;
use Closure;

class CheckKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $key = apiKey::select('key')->where('key','=',$request->header('POS_API_KEY'))->first();
        if($request->header('POS_API_KEY') == NULL){
            $response = [
                'success' => 'false',
                'message' => 'API KEY required'
            ];
            return response()->json($response);
        }elseif(empty($key)){
            $response = [
                'success' => 'false',
                'message' => 'invalid api key'
            ];
            return response()->json($response);
        }

        return $next($request);
    }
}
