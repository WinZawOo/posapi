<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Imei extends Model
{

    protected $table = 'tb_imei';
    //protected $fillable = ['kid','key','simid','imei','sn'];
    protected $guarded = [];
    protected $attributes = [
        'phone' => '0',
        'custprefix' => '0',
    ];
    protected $casts = [
        'updatetime' => 'datetime:Y-m-d'
    ];

    public $timestamps = false;

    const UPDATED_AT = 'updatetime';
    const CREATED_AT =  null;

    public function scopeGetData($query, $where, $s){
        if($s == 'version'){
            return  $query->select('apkversioncode','apkname','apkmessage','version')->where($where)->get();
        }elseif ($s == 'all'){
            $kid = Arr::only($where,['kid']);
            return $query->where('kid',$kid)->get();
        }
    }

    public function scopeUpdateData($query, $data){
        if($query->where('simid',$data['simid'])->update($data)){
             $query->where('simid',$data['simid'])->increment('version');
             return true;
         }else
         return false;
    }

    public function scopeGetRowsByColumn($query, $where){
        $data = $query->where($where)->orderBy('id','desc')->first()->toArray();
        $data['tb_cat'] = $this->get_id("Category", $data);
        $data['tb_convert'] = $this->get_id("Convert",$data);
        $data['tb_ConvertDetail'] = $this->get_id("ConvertDetail",$data);
        $data['tb_coupon'] = $this->get_id("Coupon",$data);
        $data['tb_customer'] = $this->get_id("Customer",$data);
        $data['tb_incomeexpense'] = $this->get_id("IncomeExpense",$data);
        $data['tb_invoice'] = $this->get_id("Invoice",$data);
        $data['tb_invoicedetail'] = $this->get_id("InvoiceDetail",$data);
        $data['tb_item'] = $this->get_id("Item",$data);
        $data['tb_package'] =$this->get_id("Package",$data);
        $data['tb_purchase'] = $this->get_id("Purchase",$data);
        $data['tb_purchasedetail'] =$this->get_id("PurchaseDetail", $data);
        $data['tb_stockadjust'] = $this->get_id("StockAdjust", $data);
        $data['tb_stocktransfer'] = $this->get_id("StockTransfer", $data);
        $data['tb_supplier'] = $this->get_id("Supplier", $data);
      return $data;
    }

    public function get_id($model, $data){
        $namespace = "\\App\\";
        $model = $namespace.$model;
        $_id =  $model::where('did',Arr::only($data,['id']))->max('_id');
        if($_id == null){
            return 0;
        }else{
            return (int) $_id;
        }
    }



}
