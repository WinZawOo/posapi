<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Item;
use App\ShopStock;
use App\Warehouse;
use DB;

class SalesDetail extends Model
{
    protected $fillable = ['id','kid','did','sale_id','iid','itc','sid','qty','pry','rmk','udt','syc','version','foc','srl','bth','dlr','duid','dtm','drv','vhl','brd','dis','car','cod','who','prj','lin','sta','upd','autoid','log','but','grp','csf'];

    protected $table = 'tb_salesdetail';
    const UPDATED_AT = 'udt';

    protected $attributes = [
        'itc' => 'null',
    ];

    public function sale()
    {
    	return $this->belongsTo('App\Sale');
    }

    public function item()
    {
    	return $this->belongsTo('App\Item', 'iid');
    }

    public function warehouse()
    {
    	return $this->belongsTo('App\Warehouse', 'sid', 'id');
    }

    public function scopeGetAllWhere($query, $col)
    {

        $res = $this->with(['warehouse' => function($query) use ($col){ $query->select($col); }])->with(['item' => function($query) use ($col){ $query->select($col); }])->get();

    	// $res = $query->find(10001)->item->where('id', '=', 27558 )->get('name');

        return $res;
    }

    public function scopeUpdateCode($query, $detail)
    {
        SalesDetail::where('id', '=', $detail['id'])->update(['upd'=>0, 'lin'=>$detail['linenum']]);
    }

    public function scopeInsertUpdate($query, $data)
    {
        $query->updateOrCreate(['id' => $data['id']], $data);

        $qty = 'qty-';

        if($nonstocks = Item::where('id', '=', $data['iid'])->get())
        {
            if($nonstocks[0]['nonstock'] == 1)
                return true;
        }

        $data['itemid'] = $data['iid']; 
        $data['stockid'] = $data['sid'];
        $data['updatetime'] = $data['udt'];

        $stocks = ShopStock::where('iid', '=', $data['itemid'])->where('sid', '=', $data['stockid'])->get();

        if(count($stocks) < 1)
        {
            //----------- add to stock if not exist ---------------
            $stock['id']    = null;
            $stock['kid']   = $data['kid'];
            $stock['did']   = $data['did'];
            $stock['sid']   = $data['stockid'];
            $stock['iid']   = $data['itemid'];
            $stock['qty']   = $data['qty'];
            $stock['alt']   = 0;
            $stock['rmk']   = "";
            $stock['udt']   = $data['updatetime'];
            $stock['syc']   = 1;
            $stock['version']   = 1;
            //$stock['log']     = 0;
            
            $rows = Warehouse::where('id', '=', $data['stockid'])->get();

            if(count($rows) > 0)
            {
                $stock['whscode'] = $rows[0]['code'];
            }

            $rows =Item::where('id', '=', $data['itemid'])->get();

            if(count($rows) > 0){
                $stock['itemcode'] = $rows[0]['code'];
            }

            
            $stock['tti'] = $data['qty'];

            ShopStock::create($stock);                           
        
        }
        else
        {
        //     print_r($data['qty']);

        //     die();
            $update_data = [
                'qty' => DB::raw('qty - '. (int)$data['qty']), 
                'tti' => DB::raw('tti + '. (int)$data['qty']), 
                'version' => DB::raw('version + 1'), 
                'udt' => $data['updatetime']
            ];

            // print_r($update_data);
            // die();
            ShopStock::where('iid', '=', $data['itemid'])->where('sid', '=', $data['stockid'])->update($update_data);
            
        }
    }
}
