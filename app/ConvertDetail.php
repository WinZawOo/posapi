<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ConvertDetail extends Model
{
    //
    protected  $table = 'tb_convertdetail';
    protected $fillable = ['id','kid','did','_id','convert_id','fromid','toid','itemidto','qtyto','remark','log','autoid','state','version'];

    const  UPDATED_AT = 'updatetime';
    const CREATED_AT = null;
    public  function  convert(){
        return $this->belongsTo('App\Convert');
    }

    public static function insertUpdate($data, $where = array()){

            self::updateOrCreate($where, $data);
            $s = ShopStock::where([['iid',$data['itemidto']],['sid',$data['toid']]])->get()->count();
            if($s > 0){
                $shopStock = array(
                    'qty' => DB::raw('qty + '.(int)$data['qtyto']),
                    'udt' => $data['updatetime']
                );
                ShopStock::where([['sid',$data['toid'],['iid',$data['itemidto']]]])->update($shopStock);
            }else{
                $stock = array();
                $stock['id']    = null;
                $stock['kid']   = $data['kid'];
                $stock['did']   = $data['did'];
                $stock['sid']   = $data['toid'];
                $stock['iid']   = $data['itemidto'];
                $stock['qty']   = $data['qtyto'];
                $stock['alt']   = 0;
                $stock['rmk']   = "";
                $stock['udt']   = $data['updatetime'];
                $stock['syc']   = 1;
                $stock['version']   = 1;
                $stock['tti'] = $data['qtyto'];

                if($row = Warehouse::where('id',$data['fromid'])->get()->toArray()){
                    $stock['whscode'] = $row[0]['code'];
                }

                if($row = Item::where('id',$data['itemidto'])->get()->toArray()){
                    $stock['itemcode'] = $row[0]['code'];
                }
                ShopStock::create($stock);
            }
        return true;
    }

}
