<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    //
    protected  $table = 'tb_supplier';
    protected $fillable = ['id','kid','did','_id','name','address','email','phone','contact','cphone','debt','remark'];

    const CREATED_AT = 'createtime';
    const UPDATED_AT = null;

    public function scopeGetData($query, $col, $limit, $offset){
        if($limit == 0 && $offset == 0){
            $data = $query->select($col)->orderby('log','desc')->get()->toArray();
        }elseif ($limit == 0 && $offset != null ){
            $data = $query->select($col)->where('log','>',$offset)->orderby('log','desc')->get()->toArray();
        }else{
            $data = $query->select($col)->where('log','>',$offset)->limit($limit)->orderby('log','desc')->get()->toArray();
        }
        return $data;
    }
}
