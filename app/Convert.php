<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use function foo\func;

class Convert extends Model
{
    protected $table = 'tb_convert';
    const CREATED_AT = 'createtime';
    const UPDATED_AT = 'updatetime';
    protected $fillable = ['kid','did','_id','fromid','toid','itemid','itemidto','qty','remark','updatetime','id','log','master'];
    protected $attributes = [
        'master' => '1'
    ];

    public  function  convertDetail(){
      return  $this->hasMany('App\ConvertDetail','cid','id');
    }

    /**
     * Scope a query to only include users of a given type.
     *
     * @param Builder $query
     * @param $col1
     * @param $col2
     * @param null $limit
     * @param null $offset
     * @return Builder
     */
    public function scopeGetData($query, $col1, $col2, $limit  = null, $offset = null)
    {
        if($limit == 0 && $offset == 0){
            $convert = $this->with(['convertDetail'=>function($query)use($col2){
                $query->select($col2)->orderby('log','desc');
            }])->orderBy('log','desc')->get($col1);
        }elseif($limit == 0 && $offset !== null){
            $convert = $this->with(['convertDetail'=>function($query) use($col2,$offset){
                $query->select($col2)->where('log','>',$offset)->orderby('log','desc');
            }])->where('log','>',$offset)->orderBy('log','desc')->get($col1);
        }else{
            $convert = $this->with(['convertDetail'=>function($query) use($col2,$limit,$offset){
                $query->select($col2)->where('log','>',$offset)->take($limit)->orderby('log','desc');
            }])->where('log','>',$offset)->take($limit)->orderBy('log','desc')->get($col1);
        }

        return $convert;
    }

    public static function insertUpdate($data, $where = array()){
            Self::updateOrCreate($where, $data);
            $s = ShopStock::where([['iid', $data['itemid']],['sid', $data['fromid']]])->get()->count();
            if ($s > 0) {
                $shopStock = array(
                    'qty' => DB::raw('qty - ' . (int)$data['qty']),
                    'udt' => $data['updatetime'],
                );
                ShopStock::where([['iid', $data['itemid']], ['sid', $data['fromid']]])->update($shopStock);
            } else {
                $stock = array();
                $stock['id'] = null;
                $stock['kid'] = $data['kid'];
                $stock['did'] = $data['did'];
                $stock['sid'] = $data['fromid'];
                $stock['iid'] = $data['itemid'];
                $stock['qty'] = $data['qty'];
                $stock['alt'] = 0;
                $stock['rmk'] = "";
                $stock['udt'] = $data['updatetime'];
                $stock['syc'] = 1;
                $stock['version'] = 1;
                $stock['tti'] = $data['qty'];

                if ($row = Warehouse::where('id', $data['fromid'])->get()->toArray()) {
                    $stock['whscode'] = $row[0]['code'];
                }

                if ($row = Item::where('id', $data['itemid'])->get()->toArray()) {
                    $stock['itemcode'] = $row[0]['code'];
                }

                ShopStock::create($stock);
            }
         return true;
    }
}
