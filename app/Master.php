<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Master extends Model
{
   protected $guarded = [];

   protected $table = 'tb_master';

   public function scopeGetTbByName($query, $table)
   {
   		if($table == 'tb_address')
			return DB::select('select * from tb_address');
		else
			return DB::select('select * from '. $table);
   	}
}
