<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = ['id','kid','did','_id','catid','catcode','supplierid','name','name2','unit','unit2','image','code','barcode','model', 'spec1', 'spec2','spec3','cost','cost2','price','price2','price3',
    'remark','updatetime', 'createtime', 'sync', 'version', 'encrypt', 'state','lockprice','currency','fcost','parent','qty','brand','uom','old_code','log','autoid','hassn','nonstock'];

    protected $table = 'tb_item';

    //protected $incrementable = 'id';


    const UPDATED_AT = 'updatetime';

   	public function setUpdatedAt($value)
   	{     
    	return $this;
   	}
   	public function setCreatedAt($value)
   	{
    	return $this;
   	}

    public function scopeGetAll($query, $col, $limit, $offset)
    {
      if($offset == 0 && $limit == 0)
      {
          return $query->select($col)->orderBy('log','ASC')->get()->toArray();
      }
      elseif($limit == 0 && $offset != 0)
      {
        return $query->select($col)->where('log', '>', $offset)->orderBy('log','ASC')->get()->toArray();
      }
      else
      {
          return $query->select($col)->where('log', '>', $offset)->orderBy('log','ASC')->take($limit)->get()->toArray();
      }
    }

    public function scopeUpdateInsert($query, $data)
    {
      $item = $query->updateOrCreate(['id' => $data['id']], $data);

      if ($item->wasRecentlyCreated) 
      {
             
      }
      else
      {
        $code = $data['code'];
        ShopStock::where('itemcode','!=',$code)->where('iid','=',$data['id'])->update([
       'itemcode' => $code]);
      }

    }

}
