<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table = 'tb_feedback';
    protected $fillable = ['kid','did','username','feedback','role'];

    const CREATED_AT = 'createtime';
    const UPDATED_AT =  'updatetime';
}
