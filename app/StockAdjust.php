<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StockAdjust extends Model
{

    protected  $table = 'tb_stockadjust';
    protected $guarded = [];

    const CREATED_AT =  'createtime';
    const UPDATED_AT =  'updatetime';

    public function scopeGetData($query, $col, $limit, $offset){
        if($limit == 0 && $offset == 0){
            $data = $query->select($col)->orderby('log','desc')->get();
        }elseif ($limit == 0 && $offset != null ){
            $data = $query->select($col)->where('log','>',$offset)->orderby('log','desc')->get();
        }else{
            $data = $query->select($col)->where('log','>',$offset)->limit($limit)->orderby('log','desc')->get();
        }
        return $data;
    }

    public static function insertUpdate($data, $where = array()){

        self::updateOrCreate($where, $data);

        $qty = '';
        $data['qty'] = $data['qtynew'];

        if(!ShopStock::where([['iid', $data['itemid']],['sid', $data['stockid']]])->get()->toArray()){
            $stock = array();
            $stock['id']    = null;
            $stock['kid']   = $data['kid'];
            $stock['did']   = $data['did'];
            $stock['sid']  	= $data['stockid'];
            $stock['iid']  	= $data['itemid'];
            $stock['qty']   = $data['qty'];
            $stock['alt']   = 0;
            $stock['rmk']   = "";
            $stock['udt']   = $data['updatetime'];
            $stock['syc']   = 1;
            $stock['version']  	= 1;
            $stock['tti'] = $data['qty'];

            if ($row = Warehouse::where('id', $data['stockid'])->get()->toArray()) {
                $stock['whscode'] = $row[0]['code'];
            }

            if ($row = Item::where('id', $data['itemid'])->get()->toArray()) {
                $stock['itemcode'] = $row[0]['code'];
            }
            ShopStock::create($stock);
        }else{
            $updateData = array(
                'qty' => $qty.$data['qty'],
                'tti' => DB::raw('tti + '.(int)$data['qty']),
                'version' => DB::raw('version + 1'),
                'udt' => $data['updatetime']
            );

            ShopStock::where([['iid', $data['itemid']],['sid', $data['stockid']]])->update($updateData);
        }

    }
}
