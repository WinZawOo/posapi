<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use PhpParser\Builder;
use function foo\func;

class InvoiceDetail extends Model
{
    //
    protected  $table = 'tb_invoicedetail';
    protected $guarded = [];

    const CREATED_AT =  null;
    const UPDATED_AT = null;

    public function items(){
        return $this->belongsTo('App\Item','iid','id');
    }

    public  function warehouses(){
        return $this->belongsTo('App\Warehouse','sid','id');
    }

    public function scopeSaleOrderDetail($query){
        $col = array(
            'id AS Id',
            '_id AS _i',
            'did AS Did',
            'inv AS Invoiceid',
            'iid',
            'sid',
            'qty AS Qty',
            'prc AS Price',
            'rmk AS Remark',
            'udt AS Updatetime',
            'version AS Version',
            'foc AS Foc',
            'srl AS Serial',
            'bth AS Batch',
            'dlr AS Todeliver',
            'duid AS du',
            'dtm AS Delivetime',
            'drv AS Driver',
            'vhl AS Vehacle',
            'brd AS Brand',
            'car AS Carrier',
            'cod AS Code',
//            'code AS Itemcode',
//            'code AS Whcode',
            'prj AS Project',
            'dis AS Discountks',
            'lin AS Line'
        );
        return $this->with('items:id,code','warehouses:id,code')->select($col)->where('upd',"=",'1')->get()->toArray();
    }


}
