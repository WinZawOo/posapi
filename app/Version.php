<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Version extends Model
{
    public function scopegetAll($query)
    {
    	return $query->get();
    }
}
