<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerGroup extends Model
{
    protected $table = 'tb_customergroup';
    protected $fillable = ['id','code','name','state'];

    const UPDATED_AT = 'updatetime';
    const CREATED_AT =  null;

    public function scopeGetData($query, $limit, $offset){
        if($limit == 0 && $offset == 0){
            return $query->orderby('log','desc')->get();
        }elseif ($limit == 0 && $offset != null){
            return $query->where('log','>',$offset)->orderby('log','desc')->get();
        }else{
            return $query->where('log','>',$offset)->limit($limit)->orderby('log','desc')->get();
        }
    }
}
