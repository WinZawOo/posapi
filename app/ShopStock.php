<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopStock extends Model
{
    protected $table = 'tb_shopstock';
    protected $guarded = [];
    public $timestamps = false;

    public function scopeGetData($query, $col, $limit, $offset){
        if($limit == 0 && $offset == 0){
            $data = $query->select($col)->orderby('log','desc')->get();
        }elseif ($limit == 0 && $offset != null ){
            $data = $query->select($col)->where('log','>',$offset)->orderby('log','desc')->get();
        }else{
            $data = $query->select($col)->where('log','>',$offset)->limit($limit)->orderby('log','desc')->get();
        }
        return $data;
    }
}
