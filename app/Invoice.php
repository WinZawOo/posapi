<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Self_;
use function foo\func;

class Invoice extends Model
{
    public $timestamps = false;
    protected  $table = 'tb_invoice';
    protected $fillable = ['id','_id','cod','sid','cid','ccd','ccn','uid','rmk','syc','eve',
                            'cpn','drm','ida','prj','brh','dtr','slt','dpt','shp','caa','srn',
                            'srs','gnb','slp','cnm','ucd','adr','dld','upd','pty','det'];

    protected $attributes = [
        'det'=> '1',
        'dnb' => '1',
    ];


    public function invoiceDetails(){
        return $this->hasMany('App\InvoiceDetail','inv','id');
    }

    public function scopeGetLastAllDetail($query, $col1, $col2, $did){
        $data = $this->with(['invoiceDetails'=>function($query)use($col2){
             $query->select($col2);
        }])->select($col1)->where('did','=',$did)->orderBy('autoid','desc')->get()->toArray();
        return $data;
    }

    public function scopeGetAllDetail($query, $col1, $col2, $limit, $offset){
        if($limit == 0 && $offset == 0){
            $data = $this->with(['invoiceDetails'=>function($query)use($col2){
                $query->select($col2);
            }])->select($col1)->get()->toArray();
        }elseif ($limit == 0 && $offset != null){
            $data = $this->with(['invoiceDetails'=>function($query)use($col2,$offset){
                $query->select($col2)->where('log','>',$offset)->get();
            }])->select($col1)->where('log','>',$offset)->get()->toArray();
        }else{
            $data = $this->with(['invoiceDetails'=>function($query)use($col2,$offset,$limit){
                $query->select($col2)->where('log','>',$offset)->limit($limit)->get();
            }])->select($col1)->where('log','>',$offset)->limit($limit)->get()->toArray();
        }
        return $data;
    }

    public static function updateCode($model,$datas){
        if($model == "Invoice") {
            $invoiceCode = [
                'upd' => 0,
                'cod' => 'srn-'.$datas['docnum'],
                'det' => $datas['docentry'],
                'dnb' => $datas['docnum'],
                'srs' => $datas['series']
            ];
            $invoiceDetailCode = [
                'cod' => $datas['docnum'],
                'upd' => 0,
                'sta' => 0
            ];

            self::where(['id' => $datas['id']])->update($invoiceCode);
            InvoiceDetail::where([['inv','=',$datas['id']],['qty','=','0']])->update($invoiceDetailCode);
        }

        if ($model == "InvoiceDetail")
           InvoiceDetail::where(['inv'=>$datas['id']])->update(['lin'=>$datas['linenum'],'upd'=>0]);

        return true;
    }

    public static function updateClosed($datasc){
        $code = [
            'upd'=>0,
            'sta'=>2
        ];
        self::where(['id'=>$datasc['id']])->update($code);
        InvoiceDetail::where(['inv'=>$datasc['id']])->update($code);
    }

    public static function updateCanceled($dataCancel){
        $codes = [
            'upd' => 0,
            'sta' => 0,
            'rmk' => "SAP CANCELLED",
        ];
        InvoiceDetail::where(['inv'=>$dataCancel['id']])->update($codes);
    }

    public static function insertUpdate($model, $data, $where){
        if($model == "Invoice"){
            Self::updateOrCreate($where, $data);
        }

        if($model == "InvoiceDetail"){
            InvoiceDetail::updateOrCreate($where, $data);

            if($nonstocks  = Item::where(['id'=>$data['id']])->get()->toArray()){
                if($nonstocks[0]['nonstock'] == 1)
                    return true;
            }

            $data['itemid'] = $data['iid'];
            $data['stockid'] = $data['sid'];
            $data['updatetime'] = $data['udt'];

            if(!$stocks = ShopStock::where([['iid',$data['itemid']],['sid',$data['stockid']]])->get()->toArray()){
                $stock  = array();
                $stock['id'] = null;
                $stock['kid'] = $data['kid'];
                $stock['did'] = $data['did'];
                $stock['sid'] = $data['stockid'];
                $stock['iid'] = $data['itemid'];
                $stock['qty'] = $data['qty'];
                $stock['alt'] = 0;
                $stock['rmk'] = "";
                $stock['udt'] = $data['updatetime'];
                $stock['syc'] = 1;
                $stock['version'] = 1;
                $stock['tti'] = $data['qty'];

                if($row = Warehouse::where('id',$data['stockid'])->get()->toArray()){
                    $stock['whscode'] = $row[0]['code'];
                }

                if($row = Item::where('id',$data['itemid'])->get()->toArray()){
                    $stock['itemcode'] = $row[0]['code'];
                }
                ShopStock::create($stock);
            }else{
                $updateData = [
                    'qty'     => DB::raw('qty - '. (int)$data['qty']),
                    'tti'     => DB::raw('tti + '. (int)$data['qty']),
                    'version' => DB::raw('version + 1'),
                    'udt'     => $data['updatetime'],
                ];
                ShopStock::where([['sid',$data['stockid']],['iid',$data['itemid']]])->update($updateData);
            }
        }
    }

    public static function del($data){
        if($rows = self::where('id',$data['id'])->get()->toArray()){
           $invoiceData = array(
               'upd' => 1,
               'sta' => 0,
               'rmk' => 'rmk,'.$data['rmk'],
           );
            if(self::where('id',$data['id'])->update($invoiceData)){
                $invoiceUpdate = array();
                $invoiceUpdate['iid'] = $data['id'];
                $invoiceUpdate['did'] = 0;
                $invoiceUpdate['lin'] = -1;
                $invoiceUpdate['act'] = 0;

                InvoiceUpdate::create($invoiceUpdate);

                InvoiceDetail::where('inv',$data['id'])->update(['sta'=>0]);
            }

           if($details = InvoiceDetail::where('inv',$data['id'])->get()){
               foreach ($details as $detail)
                ShopStock::where([['sid',$detail->sid],['iid',$detail->iid]])->update(['qty'=>DB::raw('qty + '.(int)$detail->qty)]);
           }
        }else{
            return true;
        }
    }

    public static function updateInvoiceChangeStock($data)
    {
        InvoiceDetail::where('id', $data['det_id'])->update(['sid' => $data['to'], 'udt' => now()]);

        $fromStock = array(
            'qty'     => DB::raw('qty - '.(int)$data['qty']),
            'version' => DB::raw('version + 1'),
            'udt'     => now()
        );
        ShopStock::where([['sid', $data['from']], ['iid', $data['iid']]])->update($fromStock);

        $toStock = array(
            'qty'     => DB::raw('qty + '. (int)$data['qty']),
            'version' => DB::raw('version + 1'),
            'udt'     => now()
        );
        ShopStock::where([['sid', $data['to']], ['iid', $data['iid']]])->update($toStock);

        Self::where('id',$data['inv_id'])->update(['udt'=>now()]);
    }

    public static function updateOpenQty($arr){
        if( Self::where('id',$arr['inv'])->get()->count() > 0){
            $openqty = 0;
            $invD = InvoiceDetail::where('inv',$arr['inv'])->get()->toArray();
            foreach ($invD as $invd){
                if($arr['u_pos'] == $invd['id']){
                    if($arr['openqty'] == 0)
                        InvoiceDetail::where('id',$arr['u_pos'])->update(['dlr'=>$arr['openqty'],'sta'=>2]);
                    else
                        InvoiceDetail::where('id',$arr['u_pos'])->update(['dlr'=>$arr['openqty']]);
                    $openqty += $arr['openqty'];
                }else{
                    $openqty += $invd['dlr'];
                }
            }

            Self::where('id',$arr['inv'])->update(['dlr'=>$openqty,'dle'=>DB::raw('qty - '.(int)$openqty)]);
        }else{
            return 0;
        }
    }
}
